*Paired project carried out in Autumn 2019*

# Regularized QDA implementation

We implemented the **regularized QDA (Quadratic Discriminant Analysis) algorithm** in order to compare its performance with the classic QDA algorithm (benchmark between our regularized implementation and the classic implementation of the Sklearn library).
The implementation is present in the *main_notebook.ipynb* notebook.
More details on the implementation as well as the results are present in the pdf *"implementation_results_details.pdf"*.