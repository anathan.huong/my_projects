Here is a code that I created to generate fictitious data to be used in Tableau to create a dashboard to be published on Tableau Public. 

The reason I generated this data is to allow for flexibility in the columns to use and also because I did not easily find such data on the internet.

The code allows for simulating a set of fictitious data that is quite close to real life by integrating random factors through mathematical laws. 

Here is a summary of the different steps in the code:

1. Initialization: creation of lists of salespeople, managers, stores, regions, and other necessary variables.
2. Assignment: assigning salespeople to managers, stores, and regions.
3. Account association: assigning accounts to salespeople.
4. Determination and adjustment of the number of opportunities: using a Poisson distribution and adjustment to reach approximately 1000 opportunities.
5. Opportunity generation: creating opportunities for each account based on predefined weights.
6. Calculation of the actual price: applying a variation factor and the probability of success.
7. Creation and verification of a DataFrame: integrating the generated data and ensuring coherence.
8. Growth factors and seasonality: definition and application of annual growth factors and seasonality.
9. Monthly goals: generating monthly goals for each salesperson.
10. Creation of a DataFrame for goals: integration of monthly goals.
11. Export and download of DataFrames: writing the "opportunities" and "goals" DataFrames to an Excel file and retrieving the file.
