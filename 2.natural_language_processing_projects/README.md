This file contains personal projects carried out on the NLP for the purpose of learning and training (in my spare time).
In addition to following tutorials, I experiment and add comments according to my understanding. These files can be used by others in the context of private lessons or conferences for example.

# tweets_classification_1
- source: https://www.analyticsvidhya.com/blog/2018/04/a-comprehensive-guide-to-understand-and-implement-text-classification-in-python/
- Objective: enables me to be better at classification using nlp techniques (TF-IDF, words embedding) with Machine Learning methods (NN, RNN, LSTM, Naive Bayes, XGBoost etc.).


