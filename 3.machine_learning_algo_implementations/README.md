*Project carried out alone (and with the professor help for some part of the code) in spring 2019, in exchange in Seoul (SeoulTech), Data Mining course*

# Implementation and comparisons of different Machine Learning algorithms

For this project, it is easier to compile the code using the spyder platform (more visual interactions, better UI / UX to code).

**From the "MAGIC Gamma Telescope" dataset, we perform classification.**
We start by using logistic regression in order to obtain the classification precision for different "cutoff".
We then implement different machine learning methods by hand and study their accuracy:
- Bernouilli Naïve Bayes: binarization of class columns and calculation of class probabilities with different "cutoff".
- Nearest Neighbord: with uniform or proportional weights depending on the distance of the points with the test point; with "Euclidean" and "Manhattan" metrics; with normalized variables or not; for different values ​​of k (hyperparameter of the method).
- K mean clustering: separation into two clusters.