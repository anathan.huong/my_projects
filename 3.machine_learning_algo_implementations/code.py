# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics.pairwise import euclidean_distances, manhattan_distances
import matplotlib.pyplot as plt

# Magic Gamma telescope dataset 
data=pd.read_csv('https://drive.google.com/uc?export=download&id=1AoCh22pmLHhdQtYdYUAJJqOCwF9obgVO', sep='\t')
data['class']=(data['class']=='g')*1

# Define the feature matrix X and the target y
X=data.drop('class',axis=1).values
y=data['class'].values

# Separation into train/test datasets
trainX,testX,trainY,testY=train_test_split(X,y,stratify=y,test_size=0.2,random_state=11)

# Logistic regression: predict output based on probability of class g (y=1)
clf1 = LogisticRegression()
clf1.fit(trainX, trainY)
y_prob1= clf1.predict_proba(testX)

# cutoff value 
cutOff = np.linspace(0.1, 0.95, 18)

accuracyLogisticRegression2 = []

#For each cutoff value we compute the accuracy to append it to the list accuracyLogisticRegression2
for cutOffValue in cutOff:
    y_pred2 = np.where(y_prob1[:,1] > cutOffValue, 1, 0)
    accuracyLogisticRegression2.append(clf1.score(testX, y_pred2))

#Draw the line 
# =============================================================================
# plt.plot(cutOff, accuracyLogisticRegression2)
# plt.xlabel('CutOff')
# plt.ylabel('Accuracy')
# =============================================================================

# Bernoulli naïve Bayes: Estimate parameters of Bernoulli naïve Bayes

# Write user-defined function to estimate parameters of Bernoulli naïve Bayes
    
# Thanks to this function we can get via a dictionnary the unique classes and the number of repetition of each class   
def getNumberClassBNB(YClass):
    
    # Initialisation 
    classVotes = {}
    
    # We loop on all the instances of YClass 
    for x in range(len(YClass)):
        
        response = YClass[x]
        
        # Already in our dictionnary so we just increment the number associated
        if response in classVotes:
            classVotes[response] += 1
            
        # We initialize at 1 this instance 
        else:
            classVotes[response] = 1 
    
    # Then we realise a sort 
    classVotes = sorted(classVotes.items(), key=lambda x: x[1], reverse = True)
    
    # Return the dictionnary 
    return classVotes

def BNB(X,y):
    ######## BERNOULLI NAIVE BAYES ########
    # INPUT 
    # X: n by p array (n=# of observations, p=# of input variables)
    # y: output (len(y)=n, categorical variable)
    # OUTPUT
    # pmatrix: 2-D array(list) of size c by p with the probability p_ij where c is number of unique classes in y
    
    #We get the unique classes in y and the occurence of these classes 
    YclassDic = getNumberClassBNB(y)
    
    # Initialization of the pmatrix with the right dimensions
    pmatrix = np.zeros((len(YclassDic), len(X[0])))
    
    indexColP = 0
    
    # We loop on each uniaue instance of the classes in YclassDic 
    for key, value in YclassDic : 
        
        # For each feature 
        for j in range(len(X[0])):
            
            numerator = 0
            
            # If the condition of the formula is respected then we increment the numerator of 1
            for i in range(len(X)):
                if (X[i][j] == 1) and (y[i] == key):
                    numerator += 1
        
            pmatrix[key][j] = numerator / value
            
        indexColP += 1
        
    return pmatrix


def BNB2(X,y):  
    
    YclassDic = getNumberClassBNB(y)
    
    # Size number of classes * number of features
    pmatrix = BNB(binarizeTrainX, y)
    
    # Number of samples * number of classes
    pmatrixTwo = np.ones((len(X), len(YclassDic)))
    
    for key, value in YclassDic:
    
        # Loop on samples  
        for i in range(len(X)):
        
            # Loop on the features 
            for j in range(len(X[0])):
                
                # We apply the formula we have seen during class 
                pmatrixTwo[i][key] *= ((pmatrix[key][j])**(X[i][j])) * ((1-pmatrix[key][j])**(1-X[i][j]))
             
            # To respect the formula we then have to multiply with this term 
            pmatrixTwo[i][key] *= (value/len(X))
    
    # Return our matrix
    return pmatrixTwo


# Calculate p values of several Bernoulli distributions

# Binarization of trainX following the procedure of the course 
trainXMean = np.mean(trainX, axis=0)
binarizeTrainX = (trainX>trainXMean)*1

# Computation of p values
pValueMatrix = BNB(binarizeTrainX, y)

# Predict output based on probability of class g (y=1)

# Accuracy is defined as number of true labels predicted / number of total labels
def Accuracy(trueLabels, predictedLabels):
    
    #Initialization 
    truePredictions = 0
    totalLabels = len(trueLabels)
    
    #For each sample with have to compare what we have predicted with the real value 
    for i in range(len(trueLabels)):
        if trueLabels[i] == predictedLabels[i]:
            truePredictions += 1
    
    #We return the accuracy 
    return (truePredictions / totalLabels)
    
#We apply that the same way than in the first part with the cutoff values 
accuracyBNB = []

probaValueMatrix = BNB2(binarizeTrainX, y)

for cutOffValueBNB in cutOff:
    y_pred_BNB = np.where(probaValueMatrix[:,1] > cutOffValue, 1, 0)
    accuracyBNB.append(Accuracy(trainY, y_pred_BNB))
    
#plt.plot(cutOff, accuracyBNB)


# Nearest neighbor 
   
# k-NN with uniform weights
# Write user-deinfed function of k-NN 
# Use imported distance functions implemented by sklearn

def euclidean_dist(a,b):
    ######## EUCLIDEAN DISTANCE ########
    # INPUT
    # a: 1-D array 
    # b: 1-D array 
    # a and b have the same length
    # OUTPUT
    # d: Euclidean distance between a and b
    
    # Euclidean distance
    d = 0
    a = np.array(a)
    b = np.array(b)
    d = np.sqrt(np.sum((a-b)**2))
    return d

def manhattan_dist(a,b):
    ######## EUCLIDEAN DISTANCE ########
    # INPUT
    # a: 1-D array 
    # b: 1-D array 
    # a and b have the same length
    # OUTPUT
    # d: Manhattan distance between a and b
    
    # Manhattan distance
    d = 0
    a = np.array(a)
    b = np.array(b)
    d = np.sum(abs(a-b))
    return d

# Same kind of function as the one below but this one is adaptated to return the most common class 
def getMajorityClass(neighbors):
    
    # We initialize 
    classVotes = {}
    
    # Loop on all the neighbors we gave as argument 
    for x in range(len(neighbors)):
        response = neighbors[x]
        
        # The goal here is to count them 
        if response in classVotes:
            classVotes[response] += 1
        else:
            classVotes[response] = 1
    
    # Then we sort the dictionnary 
    classVotes = sorted(classVotes.items(), key=lambda x: x[1], reverse = True)
    
    max_value = 0
    
    # Via this algorithm we select the key with the maximum occurence 
    # Loop on instance of the dictionary 
    for key, value in classVotes : 
        
        # If we find a stronger occurence, we then memorize the key associated 
        if value > max_value:
            max_value = value
            max_key = key
            
        # If it is draw we select via the key 
        elif value == max_value:
            if max_key > key:
                max_value = value
                max_key = key
                
    # Return the key with the max occurence 
    return max_key


def knn(trainX,trainY,testX,k,dist='euclidean'):
    ######## K-NN Classification ########
    # INPUT 
    # trainX: training input dataset, n by p size 2-D array
    # trainY: training output target, 1-D array with length of n
    # testX: test input dataset, m by p size 2-D array
    # k: the number of the nearest neighbors
    # dist: distance measure function
    # OUTPUT
    # y_pred: predicted output target of testX, 1-D array with length of m
    #         When tie occurs, the final class is select in alpabetical order
    #         EX) if "A" ties "B", select "A" and if "2" ties "4", select 2
    

    #Initialization of y_pred
    y_pred = []
    
    #On boucle pour chaque sample dans testX (m itérations)
    for i in range (len(testX)):
        
        distances = []
        targets = []
    
        #Calculating the Euclidean distance
        if dist == 'euclidean':
        
            #For each sample of trainX (n iterations)
            for j in range(len(trainX)):
                
                #Calculating the distance between testX and trainX samples
                distance = np.sqrt(np.sum(np.square(testX[i,:] - trainX[j,:])))
                
                #Each calculated distance is added to the distances variable
                distances.append([distance, j])
        
        #Manhattan Distance Calculation
        if dist == 'manhattan_dist':
            
            #Calculating the distance between testX and trainX samples
            for j in range(len(trainX)):
                
                 #Calculating the distance between testX and trainX samples
                distance = np.sum(abs(testX[i,:] - trainX[j,:]))
                #Each calculated distance is added to the distances variable
                distances.append([distance, j])
            
        #Arrived at this point, distances is a vector containing ...
        #... the distances between a sample of testX and all the samples of trainX
        #We perform a sort
        distances = sorted(distances)
    
        #We only recover the first k distances among the distances that have been sorted
        for l in range(k):
            index = distances[l][1]
            
            #We recover classes corresponding to nearest neighbors
            targets.append(trainY[index])
        
        #We recover the majority class
        majorityClass = getMajorityClass(targets)
        
        #We add it to the variable y_pred
        y_pred.append(majorityClass)
    
    # TODO: k-NN classification
    
    return y_pred  

# Calculate accuracy of test set 
#       with varying the number neareset neighbors (k) and distance metrics
#       using k-NN
    
# weighted k-NN
# Write user-deinfed function of weighted k-NN
# Use imported distance functions implemented by sklearn

# This algorithm run with the same logic as above but here we also take the distance into account  
def getMajorityWeightClass(neighbors):
    
    # Initialization 
    classVotes = {}
    
    # Loop on the neighbors 
    for x in range(len(neighbors)):
        
        # Here is the class with index 0, the index 1 corresponds to the distance 
        response = neighbors[x][0]
        
        # Works with the inverse of distances
        if response in classVotes:
            
            # We add 0.01 to avoid the case where the distance is zero
            classVotes[response] += 1/(neighbors[x][1] + 0.01)
        
        else:
            
            # We add 0.01 to avoid the case where the distance is zero
            classVotes[response] = 1/(neighbors[x][1] + 0.01)
    
    # Then we sort 
    classVotes = sorted(classVotes.items(), key=lambda x: x[1], reverse = True)
    
    # Initilization
    max_value = 0
    
    # We take the key with the maximum occurence 
    for key, value in classVotes : 
        if value > max_value:
            max_value = value
            max_key = key
            
    # If this case there is a draw so we have to sort regarding the key, it means the class 
        elif value == max_value:
            if max_key > key:
                max_value = value
                max_key = key
               
    # Then we return the majority class 
    return max_key


def wknn(trainX, trainY, testX, k, dist='euclidean_dist'):
    ######## Weighted K-NN Classification ########
    # INPUT 
    # trainX: training input dataset, n by p size 2-D array
    # trainY: training output target, 1-D array with length of n
    # testX: test input dataset, m by p size 2-D array
    # k: the number of the nearest neighbors
    # dist: distance measure function
    # OUTPUT
    # y_pred: predicted output target of testX, 1-D array with length of m
    #         When tie occurs, the final class is select in alpabetical order
    #         EX) if "A" ties "B", select "A" and if "2" ties "4", select 2
    
    # weighted k-NN classification
    y_pred = []
    
    # We loop for each sample in testX (m iterations)
    for i in range(len(testX)):
        distances = []
        targets = []
    
        # Calcul de la distance Euclidienne 
        if dist == 'euclidean_dist':

            # For each sample of trainX (n iterations)
            for j in range(len(trainX)):
                
                # Calculating the distance between testX and trainX samples
                distance = np.sqrt(np.sum(np.square(testX[i,:] - trainX[j,:])))
                
                # Each calculated distance is added to the distances variable
                distances.append([distance, j])
        
        # Manhattan Distance Calculation
        if dist == 'manhattan_dist':
            
            # Loop on all the samples of trainX
            for j in range(len(trainX)):
                
                 # Calculating the distance between testX and trainX samples
                distance = np.sum(abs(testX[i,:] - trainX[j,:]))    
                
                # Each calculated distance is added to the distances variable
                distances.append([distance, j])
            
        # Arrived at this point, distances is a vector containing ...
        # ... the distances between a sample of testX and all the samples of trainX
        # We sort  
        distances = sorted(distances)
      
    
        # We only recover the first k distances among the distances that have been sorted
        for l in range(k):
            
            index = distances[l][1]
            distBis = distances[l][0]
            
            # Classes are retrieved the distance corresponding to nearest neighbors
            targets.append([trainY[index], distBis])
        
        # We recover the majority class
        majorityClass = getMajorityWeightClass(targets)
        
        # We add it to the variable y_pred
        y_pred.append(majorityClass)
    
    return y_pred   


# Calculate accuracy of test set 
# with varying the number neareset neighbors (k) and distance metrics using weighted k-NN
    
# I worked with the data in this way because the execution took time. I prefered working by running one by one
# print("With Euclidean distances")
    
# =============================================================================
# =============================================================================
# yPredEucThree = knn(trainX, trainY, testX, 3)
# AccuracyPredEucThree = Accuracy(testY, yPredEucThree)
# print('AccuracyPredEucThree')
# print(AccuracyPredEucThree)
# =============================================================================
 
# =============================================================================
# yPredEucFive = knn(trainX, trainY, testX, 5)
# AccuracyPredEucFive = Accuracy(testY, yPredEucFive)
# print('AccuracyPredEucFive')
# print(AccuracyPredEucFive)
# =============================================================================

# =============================================================================
# yPredEucSeven = knn(trainX, trainY, testX, 7)
# AccuracyPredEucSeven = Accuracy(testY, yPredEucSeven)
# print('AccuracyPredEucSeven')
# print(AccuracyPredEucSeven)
# =============================================================================
# =============================================================================

#print("With Manhatthan distances")

# =============================================================================
# =============================================================================
# yPredEucThreeM = knn(trainX, trainY, testX, 3, 'manhattan_dist')
# AccuracyPredEucThreeM = Accuracy(testY, yPredEucThreeM)
# print('AccuracyPredEucThreeM')
# print(AccuracyPredEucThreeM)
# =============================================================================

# =============================================================================
# yPredEucFiveM = knn(trainX, trainY, testX, 5, 'manhattan_dist')
# AccuracyPredEucFiveM = Accuracy(testY, yPredEucFiveM)
# print('AccuracyPredEucFiveM')
# print(AccuracyPredEucFiveM)
# =============================================================================

# =============================================================================
# yPredEucSevenM = knn(trainX, trainY, testX, 7, 'manhattan_dist')
# AccuracyPredEucSevenM = Accuracy(testY, yPredEucSevenM)
# print('AccuracyPredEucSevenM')
# print(AccuracyPredEucSevenM)
# =============================================================================
# =============================================================================

#print("With Euclidean distances Weight algorithm")

# =============================================================================
# yPredEucThreeWeight = wknn(trainX, trainY, testX, 3)
# AccuracyPredEucThreeWeight = Accuracy(testY, yPredEucThreeWeight)
# print('AccuracyPredEucThreeWeight')
# print(AccuracyPredEucThreeWeight) 
# =============================================================================
    
# =============================================================================
# yPredEucFiveWeight = wknn(trainX, trainY, testX, 5)
# AccuracyPredEucFiveWeight = Accuracy(testY, yPredEucFiveWeight)
# print('AccuracyPredEucFiveWeight')
# print(AccuracyPredEucFiveWeight)
# =============================================================================

# =============================================================================
# yPredEucSevenWeight = wknn(trainX, trainY, testX, 7)
# AccuracyPredEucSevenWeight = Accuracy(testY, yPredEucSevenWeight)
# print('AccuracyPredEucSevenWeight')
# print(AccuracyPredEucSevenWeight)
# =============================================================================

#print("With Manhatthan distances Weight algorithm")

# =============================================================================
# yPredEucThreeMWeight = wknn(trainX, trainY, testX, 3, 'manhattan_dist')
# AccuracyPredEucThreeMWeight = Accuracy(testY, yPredEucThreeMWeight)
# print('AccuracyPredEucThreeMWeight')
# print(AccuracyPredEucThreeMWeight)
# =============================================================================

# =============================================================================
# yPredEucFiveMWeight = wknn(trainX, trainY, testX, 5, 'manhattan_dist')
# AccuracyPredEucFiveMWeight = Accuracy(testY, yPredEucFiveMWeight)
# print('AccuracyPredEucFiveMWeight')
# print(AccuracyPredEucFiveMWeight)
# =============================================================================

# =============================================================================
# yPredEucSevenMWeight = wknn(trainX, trainY, testX, 7, 'manhattan_dist')
# AccuracyPredEucSevenMWeight = Accuracy(testY, yPredEucSevenMWeight)
# print('AccuracyPredEucSevenMWeight')
# print(AccuracyPredEucSevenMWeight)
# =============================================================================


# NORMALISATION if the datas here 
trainXNorm = (trainX - np.mean(trainX)) / np.std(trainX) 
testXNorm = (testX - np.mean(trainX)) / np.std(trainX) 

# =============================================================================
# print('With Euclidean distances')
#     
# # =============================================================================
# yPredEucThreeNorm = knn(trainXNorm, trainY, testXNorm, 3)
# AccuracyPredEucThreeNorm = Accuracy(testY, yPredEucThreeNorm)
# print('AccuracyPredEucThreeNorm')
# print(AccuracyPredEucThreeNorm)
#  
# yPredEucFiveNorm = knn(trainXNorm, trainY, testXNorm, 5)
# AccuracyPredEucFiveNorm = Accuracy(testY, yPredEucFiveNorm)
# print('AccuracyPredEucFiveNorm')
# print(AccuracyPredEucFiveNorm)
# 
# yPredEucSevenNorm = knn(trainXNorm, trainY, testXNorm, 7)
# AccuracyPredEucSevenNorm = Accuracy(testY, yPredEucSevenNorm)
# print('AccuracyPredEucSevenNorm')
# print(AccuracyPredEucSevenNorm)
# # =============================================================================
# 
# print('With Manhatthan distances')
# 
# yPredThreeMNorm = knn(trainXNorm, trainY, testXNorm, 3, 'manhattan_dist')
# AccuracyPredThreeMNorm = Accuracy(testY, yPredThreeMNorm)
# print('AccuracyPredThreeMNorm')
# print(AccuracyPredThreeMNorm)
# 
# yPredFiveMNorm = knn(trainXNorm, trainY, testXNorm, 5, 'manhattan_dist')
# AccuracyPredFiveMNorm = Accuracy(testY, yPredFiveMNorm)
# print('AccuracyPredFiveMNorm')
# print(AccuracyPredFiveMNorm)
# 
# yPredSevenMNorm = knn(trainXNorm, trainY, testXNorm, 7, 'manhattan_dist')
# AccuracyPredSevenMNorm = Accuracy(testY, yPredSevenMNorm)
# print('AccuracyPredSevenMNorm')
# print(AccuracyPredSevenMNorm)
# 
# print('With Euclidean distances Weight algorithm')
# 
# yPredEucThreeWeightNorm = wknn(trainXNorm, trainY, testXNorm, 3)
# AccuracyPredEucThreeWeightNorm = Accuracy(testY, yPredEucThreeWeightNorm)
# print('AccuracyPredEucThreeWeightNorm')
# print(AccuracyPredEucThreeWeightNorm) 
#     
# yPredEucFiveWeightNorm = wknn(trainXNorm, trainY, testXNorm, 5)
# AccuracyPredEucFiveWeightNorm = Accuracy(testY, yPredEucFiveWeightNorm)
# print('AccuracyPredEucFiveWeightNorm')
# print(AccuracyPredEucFiveWeightNorm)
# 
# yPredEucSevenWeightNorm = wknn(trainXNorm, trainY, testXNorm, 7)
# AccuracyPredEucSevenWeightNorm = Accuracy(testY, yPredEucSevenWeightNorm)
# print('AccuracyPredEucSevenWeightNorm')
# print(AccuracyPredEucSevenWeightNorm)
# 
# print('With Manhatthan distances Weight algorithm')
# 
# yPredThreeMWeightNorm = wknn(trainXNorm, trainY, testXNorm, 3, 'manhattan_dist')
# AccuracyPredThreeMWeightNorm = Accuracy(testY, yPredThreeMWeightNorm)
# print('AccuracyPredThreeMWeightNorm')
# print(AccuracyPredThreeMWeightNorm)
# 
# yPredFiveMWeightNorm = wknn(trainXNorm, trainY, testXNorm, 5, 'manhattan_dist')
# AccuracyPredFiveMWeightNorm = Accuracy(testY, yPredFiveMWeightNorm)
# print('AccuracyPredFiveMWeightNorm')
# print(AccuracyPredFiveMWeightNorm)
# 
# yPredSevenMWeightNorm = wknn(trainXNorm, trainY, testXNorm, 7, 'manhattan_dist')
# AccuracyPredSevenMWeightNorm = Accuracy(testY, yPredSevenMWeightNorm)
# print('AccuracyPredSevenMWeightNorm')
# print(AccuracyPredSevenMWeightNorm)
# =============================================================================
 
# For each feature, we find the minimum and maximum bounds of the set of values
def FindColMinMax(data): 
    
    # Number of features, we only have to take the length of the first sample in the data set
    numberFeatures = len(data[0]); 
    
    # We define a considerable number for minimum et a very small number for maximum so that they can change at least once
    minimum = [999999999999999 for i in range(numberFeatures)]; 
    maximum = [-999999999999999 for i in range(numberFeatures)]; 
      
    # We loop on all the data present in the data set
    for sample in data: 
        
        # For each feature, we look at whether we can replace the current minimum and maximum
        for j in range(len(sample)): 
            if (sample[j] < minimum[j]): 
                minimum[j] = sample[j]; 
              
            if (sample[j] > maximum[j]): 
                maximum[j] = sample[j]; 
  
    # We return minimum and maximum, which are tables of lengths n (number of features)
    return minimum, maximum; 

import random

def InitializeKCentroids(data, k):
    
    # Number of features, we only have to take the length of the first sample in the data set
    numberFeatures = len(data[0]); 
    
    # We get in two tables the minimum and maximum of each feature
    minimum, maximum = FindColMinMax(data)

    # We initialize a two dimensions matrix 
    centroidsData = np.zeros((k, numberFeatures))
    
    # We loop on different centroids
    for centroid in centroidsData: 
        
        # For each centroid, we loop on the different features
        for j in range(numberFeatures): 
  
            # We randomly assign, with the correct terminals, coordinates for the centroid
            centroid[j] = random.randint(int(minimum[j]), int(maximum[j]+1)); 
  
    # We return the table containing the centroids
    return centroidsData; 
    
    
def kmeans(X,k,max_iter=300):
    ############ K-MEANS CLUSTERING ##########
    # INPUT
    # X: n by p array (n=# of observations, p=# of input variables)
    # k: the number of clusters
    # max_iter: the maximum number of iteration
    # OUTPUT
    # label: cluster label (len(label)=n)
    # centers: cluster centers (k by p)
    ##########################################
    # If average distance between old centers and new centers is less than 0.000001, stop
    
    # k-means clustering
    
    # Initilization 
    label = []
    iteration = 0 
    centers = InitializeKCentroids(X, k)
    
    # Condition with the maximum number of iterations 
    while (iteration < max_iter) :
        
        # We save the actual centers 
        oldCenters = np.copy(centers)
        
        # We'll find new labels 
        label = []
        
        # Incrementation to follow the index 
        iteration += 1 
        
        # Loop for each sample in X
        for i in range(len(X)):
            
            # min and max with extreme numbers 
            minimumDist = 999999999;
            indexDist = 0;
            
            # Loop for each cluster 
            for j in range(len(centers)):
                
                # Calculation of distance between the samples and the centroids and then storage in a table
                distance = euclidean_dist(X[i], centers[j])
                
                # We are looking for the minimal distance between the point and all the centers so if we find a nearest centers we save this one 
                if distance < minimumDist:
                    minimumDist = distance 
                    indexDist = j
            
            # We append the nearest cluster label for the current point we are actually looping on 
            label.append(indexDist)
        
        # Update the centers
        for l in range(k):
         
            # Other possibility 
            # centers[l] = np.mean(X[label == l])
            
            # We compute the average regarding all the points that are belonging to the current cluster we are looping on 
            n = 0
            somme = 0
            for i in range(len(label)):
                if (label[i] == l):
                    somme += X[i]
                    n += 1
            # We affect the new values         
            centers[l] = somme/n
        
        # In this part we check if the centroids have changed or no regarding the sensitivity 
        equ = True 
        for i in range(len(centers)):
            for j in range(len(centers[i])):
                if (oldCenters[i][j] -  centers[i][j]) > 0.000001:
                    equ = False
        
            # If we enter in this if, it means the centroids did not change 
            if equ == True:
                return(label, centers)
                
    # If we exit here, it means we have reached the max iterations            
    return (label, centers)


# Calculate centroids of two clusters

kmeansQuestionFourBLabel, kmeansQuestionFourBCenters  = kmeans(X,2)
# print('kmeansQuestionFourBCenters')
# print(kmeansQuestionFourBCenters)
    
#For the log function: 
import math
# homogeneity and completeness 

# Thanks to this function we can get, via a dictionnary, the unique classes and their occurence 
def getNumberClassClustering(YClass):
    classVotes = {}
    for x in range(len(YClass)):
        response = YClass[x]
        if response in classVotes:
            classVotes[response] += 1
        else:
            classVotes[response] = 1 
    
    classVotes = sorted(classVotes.items(), key=lambda x: x[1], reverse = True)
    
    return classVotes

# Computation of the homogeneity regarding the course's formula 
def homogeneity(X, y, labels, k) :
    homogeneity = 0
    entropy = 0
    condEntropy = 0
    n = len(X)
    
    classInY = getNumberClassClustering(y)
    
    for key, nc in classInY : 
        entropy -= (nc/n)*math.log(nc/n)
    
    # Boucle sur les classes
    for key, nc in classInY :
        # boucle sur les clusters
        for i in range(k):
            nck = 0
            nk = 0
            for j in range(len(y)):
                if(y[j] == key and labels[j] == i):
                    nck += 1
                if(labels[j] == i):
                    nk += 1
            
            condEntropy -= nck/n*math.log(nck/nk)
            
    homogeneity = 1 - condEntropy/entropy
    
    return homogeneity
    
# Computation of the completeness regarding the course's formula 
def completeness(X, y, labels, k) :
    completeness = 0
    entropy = 0
    condEntropy = 0
    n = len(X)
    
    classInY = getNumberClassClustering(y)
    clustersInLabels = getNumberClassClustering(labels)
    
    for key, nk in clustersInLabels : 
        entropy -= (nk/n) * math.log(nk/n)
    
    
    for i in range(k):
        for key, nc in classInY :
            
            nck = 0
            nc = 0
            
            for j in range(len(y)):
                
                if(y[j] == key and labels[j] == i):
                    nck += 1
		
                if(y[j] == key):
                    nc += 1
            
            condEntropy -= (nck/n) * math.log(nck/nc)
            
    completeness = 1 - condEntropy/entropy
    
    return completeness
 
    
# =============================================================================
# homogeneity = homogeneity(X, y, kmeansQuestionFourBLabel, 2)    
# print('homogeneity')
# print(homogeneity)
# 
# completeness = completeness(X, y, kmeansQuestionFourBLabel, 2)    
# print('completeness')
# print(completeness)
# =============================================================================
    
    
    
    
    

