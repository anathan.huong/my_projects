*Projects carried out in pairs during the double degree Master AOS, subject AOS1 (Machine Learning), Fall 2019*

# Notebook 1

Compression of an image (reduction to the 3 main components) using the **PCA method**.

# Notebook 2

Design a regression dataset in which we want to select variables and where the **ElasticNet**
gives better results than **Lasso** in terms of stability of the set of selected variables.

# Notebook 3

**Modification of the method SVM: apply vector transforms and rotations to support vectors in order to obtain better precision than with the classic SVM method.**
This method is the application of an algorithm described in the research paper below:
- https://papers.nips.cc/paper/1253-improving-the-accuracy-and-speed-of-support-vector-machines.pdf

# Notebook 4

**Time series prediction** (estimation of cumulated debit card usage in Iceland between January and April 2013) with the classic prediction method: ARIMA.

* No data file was attached in this folder because I considered that the main thing was the logic of the code (reusable with any other dataset). *