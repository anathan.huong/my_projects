*Project carried out alone in autumn 2019 (UV SY19)*

# Objective of the regressions

This folders aims to illustrate, in the **R language**, different regression methods.
In both cases, use of simple methods via cross-validation, ranging from linear regression to more complex methods using neural networks. The goal is we want to find the method allowing to obtain the best results (the interpretability of the method is not taken into account here).
A pdf version report is given in each of the folders (includes explanations and portions of codes to understand the logic applied to the subjects).

- **"unknown_var_pred" folder**: prediction of a variable whose function we do not know (we only have numerical data and no explanatory text) with many other variables (we don't know the functions neither).

- **"yield_pred" folder**: prediction of the yield of a corn field according to many variables (temperature, location etc.).