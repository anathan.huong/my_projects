*Paired project carried out in Autumn 2017*

# Chosen topic

The topic we have chosen is titled, **"Which Pet Is Right For You?"** (our original idea !).
Far too many animals are abandoned each year. In France, every hour more than 11 domestic animals are abandoned, or around 100,000 abandons per year, including 60,000 in summer. The SPA collects part of it (40,000) in 63 shelters across France.
*Source: https://www.planetoscope.com/Animaux/1258-abandons-d-animaux-domestiques-en-france.html*

Everything suggests that the “owners” were misguided in their choice of animals.
But it is better to be safe than sorry. We therefore wish to act, upstream of the abandonment problem, that is to say before the acquisition of the animal.
Our expert system is for people who want to acquire a pet. It can be used as part of prevention campaigns or in animal shops as part of awareness.
Our system is designed to guide people who want an animal to offer them the one that suits them best.

# Expert System 

The objective of this project is the programming of an expert system (based on rules), in front chaining and back chaining. The technology used is exclusively LISP.

**Front chaining**: from a set of characteristics of a person (salary, living space, time spent at home, etc.), we determine which species of pets may be suitable for them.

**Back chaining**: from an animal and the characteristics of the person, we can tell whether or not the animal can be purchased by that person.

The implementation and more details are given in the pdf report (*Report IA - Expert System.pdf*) in this folder.

