; ****************************DEBUT MANIPULATION BASE DE FAITS************************************

(defun creationBF()
  
  (initBF) ; Réinitialisation
  
  (format T "Quel est votre type d'habitat ? Appartement 1 ou Maison 2  ~%")
 	(let ((choice (read-line)))
 	(ajoutBF (list 'Habitat '= (parse-integer choice)))
 		
    )
  
  (format T "Quelle est la surface de votre habitat ?  ~%")
 	(let ((choice (read-line)))
        (ajoutBF (list 'SurfaceHabitat '= (parse-integer choice)))
 		
    )
  
  (format T "Quel est votre budget materiel immediat a ne pas depasser en euros ?  ~%")
 	(let ((choice (read-line)))
    (ajoutBF (list 'PrixMateriel '= (parse-integer choice)))
    
 		
    )
  
  (format T "Quel est votre budget animal a ne pas depasser en euros ?  ~%")
 	(let ((choice (read-line)))
        (ajoutBF (list 'PrixAnimal '= (parse-integer choice)))
 		
    )
  
  (format T "Quel est votre budget annuel veterinaire a ne pas depasser en euros ?  ~%")
 	(let ((choice (read-line)))
        (ajoutBF (list 'PrixVeterinaireAnnuel '= (parse-integer choice)))
 		
    )
  
   (format T "Quel est votre budget mensuel nourriture a ne pas depasser en euros ?  ~%")
 	(let ((choice (read-line)))
        (ajoutBF (list 'PrixNourritureMensuel '= (parse-integer choice)))
 		
    )
  
  
  (format T "Souhaitez-vous un animal dote d une grande capacite d apprentissage ? 1, 2 ou 3  ~%")
 	(let ((choice (read-line)))
        (ajoutBF (list 'Apprend '= (parse-integer choice)))
 		
    )
  
  (format T "Souhaitez-vous un animal dote d une grande capacite de comprehension ? 1, 2 ou 3  ~%")
 	(let ((choice (read-line)))
        (ajoutBF (list 'Comprend '= (parse-integer choice)))
 		
    )
  
  (format T "Souhaitez-vous un animal dote d une interactivite ? 1, 2 ou 3  ~%")
 	(let ((choice (read-line)))
        (ajoutBF (list 'Interactivite '= (parse-integer choice)))
 		
    )
  
 
  
  (format T "Souhaitez-vous un animal dependant de vous ? de 1 a 3 ?  ~%")
 	(let ((choice (read-line)))
        (ajoutBF (list 'Dependance '= (parse-integer choice)))
 		
    )
  
   (format T "Votre travail hebdomadaire en dehors de chez vous : : moins de 8h ? entre 8 et 10h ? plus de 10h ? 1, 2 ou 3  ~%")
 	(let ((choice (read-line)))
        (ajoutBF (list 'TravailHebdomadaire '= (parse-integer choice)))
 		
    )
  
  
  (format T "Voyagez-vous rarement ? de temps en temps ? souvent ? 1, 2 ou 3  ~%")
 	(let ((choice (read-line)))
        (ajoutBF (list 'FrequenceVoyage '= (parse-integer choice)))
 		
    )
  
  
   (format T "Votre projet est il serieux et murement reflechi ? de 1 a 3 ?  ~%")
 	(let ((choice (read-line)))
        (ajoutBF (list 'Projet '= (parse-integer choice)))
 		
    )
  
  (print BF)
 	
  )



(defun ajoutBF (element)
	(if (listp element)
		(if (eq (length element) 3)
        (not (null (push element BF))) ; On renvoie vrai si la modif a eu lieu
			
			(progn
				(format T "Erreur ~%")
				(print element)
			)
		)
		(progn
			(format T "Erreur, l'élèment n'est pas une liste ~%")
			(print element)
		)
	)
  )


(defun initBF ()
  (setq BF ())
  )
  
; ****************************FIN MANIPULATION BASE DE FAITS*************************************

; **********************DEBUT CHAINAGE AVANT PROFONDEUR*****************************

(defun ChainageAvantProf ()
  (let (EC (BR BR) regleCourante (ok nil))
    
    (loop ; on boucle
      (if (assoc 'Espece BF) 
          (progn 
            (setq ok T)
            (print (caddr (assoc 'Espece BF)))
            (return-from ChainageAvantProf ok)
            )
            
   
        (dolist (r BR) 
          (when (declenchable? r BF)
            (push r EC) 
            (setq BR (remove r BR)) 
          )
        )
      )
      (if EC 
        (progn
          (setq regleCourante (pop EC))
          (pushnew (conclusions regleCourante) BF)
          )
        (if (not (assoc 'Espece BF))
            (progn
              (return-from ChainageAvantProf ok) 
              (print "Le but n est pas verifie")
              )
          )
      )
    )
    )
  )
  

(defun vrai? (p)
  (let ((valeurBF (caddr (assoc (car p) BF))))
    (if valeurBF
        (if (numberp valeurBF)
            (funcall (cadr p) valeurBF (caddr p))
                    
          (progn
            (if (eq valeurBF (caddr p)) ; on verifie une egalite de string valeurBF vaut par exemple (type = lapin)
                (return-from vrai? T)
              )
            )
        
      )
    )
    )
  )


(defun declenchable? (r faits) ;On a une règle r et la base de faits
  (let ((OK t))
    (dolist (p (cadr r) OK) ; on a un prémisse p et on regarde s'il est compatible avec les faits
      (if (not (vrai? p))
          (setq OK nil)
      )
    )
   )
  )


(defun conclusions (r)  
  (return-from conclusions (car (caddr r))) ; ne fonctionne que si une seule conclu par regle 
  ) 



(defun moteur_avant ()

		(write-line "*****UTILISATION DU MOTEUR AVANT*****")
		
		
	(if 
         (ChainageAvantProf)
         (print "Cette espece correspond à votre profil")
         (print "Aucune espece ne correspond pas à votre profil")
       )
   		
	
)

; *************************FIN CHAINAGE AVANT PROFONDEUR**********************************


;******************************DEBUT MOTEUR ARRIERE**********************************************

(defun verifier (but) 
  (let ((ok NIL)) ; ok initialisé à faux  
    (if (vraiArr? but) 
          (setq ok T) ; notre condition d'arrêt 

      (let ((RC (regles_candidates but))) ; le else 
        (dolist (r RC)
          (setq ok (verifier_et r))
          (if (equal ok T) ; On peut sortir de la boucle 
              (return) 
            ) 
          ) 
        ;(if (not ok) 
            ;(setq ok (question but)) 
          ;) 
        ) 
      )
    (return-from verifier ok) 
    ) 
  ) 


(defun verifier_et (r)
  (let ((ok T) 
        (prem (premissesArr r)))
    ;(print prem)
    (dolist (p prem) 
      (setq ok (verifier p))
      (if (equal ok NIL)
          (return)
        )
      )
    (return-from verifier_et ok)
    )
  )


(defun vraiArr? (p)
  (let ((valeurBF (caddr (assoc (car p) BF))))
    (if valeurBF
        (if (numberp valeurBF)
            (funcall (cadr p) valeurBF (caddr p))
                    
          (progn
            (if (eq valeurBF (caddr p)) ; on verifie une egalite de string valeurBF vaut par exemple lapin
                (return-from vraiArr? T)
              )
            )
        
      )
    )
    )
  )

  
  
(defun regles_candidates (but) ; Moteur ordre 0+ : a chaque but on doit avoir une égalité
 (let ((rc ())) ; rc représente les règles candidates correspondant au but 
   (dolist (regle BR) ; On parcourt la liste de règles
     
     (if (member but (caddr regle) :test #'equal) ; Si le but est présent en tant que conclusion 
   (pushnew (car regle) rc) ; On ajoute à notre pile le nom de la règle 
     )
     
    
     (if (eq (car but) (car (car (caddr regle))))
     (if (and (numberp (caddr (car (caddr regle)))) (numberp (caddr but)))
         
         (progn 
           (if (funcall (cadr but) (caddr (car (caddr regle))) (caddr but))
             (pushnew (car regle) rc)
                  )
           )
       

                    
          (progn ; On peut enlever ??
            (if (eq (caddr (car (caddr regle))) (caddr but)) 
              (pushnew (car regle) rc)
              )
            )
        
       )
       )
  ) 
  (return-from regles_candidates rc) ; Retourne la pile de règles applicables 
 ) 
  ) 


(defun dans_BF (but) 
 (let ((bool NIL))  
  (if (member but BF :test #'equal) 
  (setq bool T) 
  ) 
  (return-from dans_BF bool) ; Retourne T si but est dans BF, sinon F 
 ) 
  ) 


(defun premissesArr (r) 
  (dolist (regle BR) ; On parcourt les règles 
  (if (equal (car regle) r) 
  (return-from premissesArr (cadr regle)) ; On retourne les prémisses (deuxième élément) 
  ) 
 )  
  )


;(defun conclusions (r) 
; (dolist (regle BR) ; On parcourt les règles 
;  (if (equal (car regle) r)  
;  (return-from conclusions (caddr regle)) ; On retourne le dernier élément de la liste 
;  ) 
;   )) 


(defun moteur_arriere ()
	(let ((saisie NIL))
		(write-line "*****UTILISATION DU MOTEUR ARRIERE*****")
		(write-line "Vous avez un doute sur une espece qui vous plait ? Vous utilisez le bon moteur...")
		(write-line "Liste des especes possibles")
		(dolist (x liste_especes)
			(print x)
		)
		
		(loop 
			(format T "Veuillez saisir le numéro correspondant à votre espèce ~%")
			(setq saisie (read-line))
			(cond 
				(t
					(if 
         (verifier (list 'Espece = (nth (parse-integer saisie) liste_especes )))
         (print "Cette espece correspond à votre profil")
         (print "Cette espece ne correspond pas à votre profil")
       )
     (write-line "==APPUYEZ SUR UNE TOUCHE POUR CONTINUER==")
					(read-line)
					(return-from NIL)
				)
				(T (write-line "erreur"))
			)
		)
	)
)

; ***************************FIN MOTEUR ARRIERE*****************************************


;******************************INTERFACE*********************************************

(defun interface ()
	(let ( (selection nil))
		(loop
			(write-line "SYSTEME EXPERT : QUEL ANIMAL EST FAIT POUR VOUS ?")
                        (write-line "Pour afficher la base de faits, tapez A")
                        (write-line "Pour créer/recréer la base de faits, tapez F")
			(write-line "Pour utiliser le moteur en chaînage avant, tapez Av")
			(write-line "Pour utiliser le moteur en chaînage arrière, tapez Arr")
			(write-line "Pour quitter tapez Q")
			(write-line "=============================================================")
			(setq selection (read-line))
    (cond 
                                ((OR (equal selection "A") (equal selection "a")) (print BF))
				((OR (equal selection "F") (equal selection "f")) (creationBF))
				((OR (equal selection "Av") (equal selection "av")) (moteur_avant))
				((OR (equal selection "Arr") (equal selection "arr")) (moteur_arriere))
				((OR (equal selection "Q") (equal selection "q")) (return-from interface nil))
				;(T (print "Saisie incorrecte, veuillez recommencer") (interface))
			)
		)
	)
  )

;*********************************DEBUT INITIALISATION*******************************

(setq BF ())

(setq BR '( 

(R1 ((Habitat = 1) (SurfaceHabitat < 30)) ((PlaceDispo = 1)))
(R2 ((Habitat = 1) (SurfaceHabitat > 30)) ((PlaceDispo = 2)))
(R3 ((Habitat = 2) (SurfaceHabitat < 30)) ((PlaceDispo = 2)))
(R4 ((Habitat = 2) (SurfaceHabitat > 30)) ((PlaceDispo = 3)))

(R5 ((PrixMateriel < 100 ) (PrixAnimal < 100)) ((BudgetImmediat = 1)))
(R6 ((PrixMateriel < 100) (PrixAnimal > 100) (PrixAnimal < 500)) ((BudgetImmediat = 2)))
(R8 ((PrixMateriel < 500) (PrixMateriel > 100) (PrixAnimal > 100) (PrixAnimal < 500)) ((BudgetImmediat = 2)))
(R9 ((PrixMateriel < 500) (PrixMateriel > 100) (PrixAnimal < 100)) ((BudgetImmediat = 2)))
(R10 ((PrixMateriel < 500) (PrixMateriel > 0) (PrixAnimal > 500)) ((BudgetImmediat = 2)))
(R11 ((PrixMateriel > 500) (PrixAnimal > 500)) ((BudgetImmediat = 3)))
(R12 ((PrixMateriel > 500) (PrixAnimal > 0)(PrixAnimal < 500)) ((BudgetImmediat = 2)))


(R13 ((PrixVeterinaireAnnuel < 100) (PrixNourritureMensuel < 30)) ((BudgetLongTerme = 1)))
(R14 ((PrixVeterinaireAnnuel < 100) (PrixNourritureMensuel > 30) (PrixNourritureMensuel < 80)) ((BudgetLongTerme = 2)))
(R15 ((PrixVeterinaireAnnuel > 100) (PrixVeterinaireAnnuel < 300) (PrixNourritureMensuel < 30)) ((BudgetLongTerme = 2)))
(R16 ((PrixVeterinaireAnnuel > 100) (PrixVeterinaireAnnuel < 300)  (PrixNourritureMensuel > 30) (PrixNourritureMensuel < 80)) ((BudgetLongTerme = 2)))
(R17 ((PrixVeterinaireAnnuel > 300) (PrixNourritureMensuel > 0) (PrixNourritureMensuel < 80)) ((BudgetLongTerme = 2)))
(R18 ((PrixVeterinaireAnnuel > 300) (PrixNourritureMensuel > 80)) ((BudgetLongTerme = 3)))
(R19 ((PrixVeterinaireAnnuel > 0) (PrixVeterinaireAnnuel < 300) (PrixNourritureMensuel > 80)) ((BudgetLongTerme = 2)))


(R20 ((BudgetImmediat = 1) (BudgetLongTerme = 1)) ((BudgetTotal = 1)))
(R21 ((BudgetImmediat >= 2) (BudgetImmediat <= 3) (BudgetLongTerme >= 1) (BudgetLongTerme <= 2)) ((BudgetTotal = 2)))
(R22 ((BudgetImmediat >= 1) (BudgetImmediat <= 2) (BudgetLongTerme >= 2) (BudgetLongTerme <= 3)) ((BudgetTotal = 2)))
(R23 ((BudgetImmediat = 3) (BudgetLongTerme = 3)) ((BudgetTotal = 3)))

(R24 ((Apprend = 1) (Comprend = 1)) ((Intelligence = 1)))
(R25 ((Apprend >= 2) (Apprend <= 3) (Comprend >= 1) (Comprend <= 2)) ((Intelligence = 2)))
(R26 ((Apprend >= 1) (Apprend <= 2) (Comprend >= 2) (Comprend <= 3)) ((Intelligence = 2)))
(R27 ((Apprend = 3) (Comprend = 3)) ((Intelligence = 3)))

(R28 ((Interactivite = 1) (Dependance = 1)) ((TempsAconsacrer = 1)))
(R29 ((Interactivite >= 2) (Interactivite <= 3) (Dependance >= 1) (Dependance <= 2)) ((TempsAconsacrer = 2)))
(R30 ((Interactivite >= 1) (Interactivite <= 2) (Dependance >= 2) (Dependance <= 3)) ((TempsAconsacrer = 2)))
(R31 ((Interactivite = 3) (Dependance = 3)) ((TempsAconsacrer = 3)))

(R32 ((TravailHebdomadaire = 3) (FrequenceVoyage = 3)) ((TempsDispo = 1)))
(R33 ((TravailHebdomadaire >= 2) (TravailHebdomadaire <= 3) (FrequenceVoyage >= 1) (FrequenceVoyage <= 2)) ((TempsDispo = 2)))
(R34 ((TravailHebdomadaire >= 1) (TravailHebdomadaire <= 2) (FrequenceVoyage >= 2) (FrequenceVoyage <= 3)) ((TempsDispo = 2)))
(R35 ((TravailHebdomadaire = 1) (FrequenceVoyage = 1)) ((TempsDispo = 3)))

(R36 ((TempsAconsacrer = 3) (TempsDispo = 1)) ((CompatibilitéTemporelle = 1)))
(R37 ((TempsAconsacrer = 3) (TempsDispo = 2)) ((CompatibilitéTemporelle = 2)))
(R38 ((TempsAconsacrer = 2) (TempsDispo = 1)) ((CompatibilitéTemporelle = 2)))
(R39 ((TempsAconsacrer = 3) (TempsDispo = 3)) ((CompatibilitéTemporelle = 3)))
(R40 ((TempsAconsacrer = 2) (TempsDispo = 2)) ((CompatibilitéTemporelle = 3)))
(R41 ((TempsAconsacrer = 1) (TempsDispo = 1)) ((CompatibilitéTemporelle = 3)))
(R42 ((TempsAconsacrer = 1) (TempsDispo = 3)) ((CompatibilitéTemporelle = 3)))
(R43 ((TempsAconsacrer = 2) (TempsDispo = 3)) ((CompatibilitéTemporelle = 3)))
(R44 ((TempsAconsacrer = 1) (TempsDispo = 2)) ((CompatibilitéTemporelle = 3)))

(R45 ((Intelligence = 1) (PlaceDispo >= 1) (BudgetTotal >= 1) (CompatibilitéTemporelle >= 2) (TempsAconsacrer >= 1) (Projet >= 1)) ((Espece = Poisson)
))

(R46 ((Intelligence = 1) (PlaceDispo >= 1) (BudgetTotal >= 2) (CompatibilitéTemporelle >= 2) (TempsAconsacrer >= 2) (Projet >= 2)) ((Espece = Tortue)
))

(R47 ((Intelligence = 2) (PlaceDispo >= 1) (BudgetTotal >= 2) (CompatibilitéTemporelle >= 2) (TempsAconsacrer >= 1) (Projet >= 2)) ((Espece = Hamster)
))

(R48 ((Intelligence = 2) (PlaceDispo >= 2) (BudgetTotal >= 2) (CompatibilitéTemporelle >= 2) (TempsAconsacrer >= 2) (Projet >= 2)) ((Espece = Lapin)
))

(R49 ((Intelligence = 3) (PlaceDispo = 3) (BudgetTotal = 2) (CompatibilitéTemporelle >= 2) (Projet = 3 ) (TempsAconsacrer >= 2)) ((Espece = Chat)
))

(R50 ((Intelligence = 3) (PlaceDispo = 3) (BudgetTotal = 3) (CompatibilitéTemporelle >= 2) (Projet = 3) (TempsAconsacrer = 3)) ((Espece = Chien)
))
       )
      )



(setq liste_especes '(Poisson Tortue Hamster Lapin Chat Chien))


;*********************************FIn INITIALISATION*******************************


; ***************************DEBUT JEU BASE DE FAITS********************************


;PROFIL ETUDIANT 
;(setq BF '((PROJET = 2) (FREQUENCEVOYAGE = 1) (TRAVAILHEBDOMADAIRE = 2)
; (DEPENDANCE = 1) (INTERACTIVITE = 1) (COMPREND = 1) (APPREND = 1)
; (PRIXNOURRITUREMENSUEL = 20) (PRIXVETERINAIREANNUEL = 20) (PRIXANIMAL = 50)
;           (PRIXMATERIEL = 50) (SURFACEHABITAT = 20) (HABITAT = 1))
;      )

; PROFIL HOMME OU FEMME RICHE CELIBATAIRE 
;(setq BF '((PROJET = 3) (FREQUENCEVOYAGE = 1) (TRAVAILHEBDOMADAIRE = 1)
 ;(DEPENDANCE = 3) (INTERACTIVITE = 3) (COMPREND = 3) (APPREND = 3)
 ;(PRIXNOURRITUREMENSUEL = 2000) (PRIXVETERINAIREANNUEL = 2500) (PRIXANIMAL = 5000)
  ;         (PRIXMATERIEL = 5000) (SURFACEHABITAT = 300) (HABITAT = 2))
   ;   )

; PROFIL FAMILLE DEUX ENFANTS
;(setq BF '((PROJET = 2) (FREQUENCEVOYAGE = 2) (TRAVAILHEBDOMADAIRE = 2)
 ;(DEPENDANCE = 2) (INTERACTIVITE = 2) (COMPREND = 2) (APPREND = 3)
 ;(PRIXNOURRITUREMENSUEL = 100) (PRIXVETERINAIREANNUEL = 90) (PRIXANIMAL = 150)
 ;          (PRIXMATERIEL = 305) (SURFACEHABITAT = 150) (HABITAT = 2))
  ;    )

;***************************FIN JEU BASE DE FAITS************************************


(interface)

