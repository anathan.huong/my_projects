#include "autocell1D.h"


AutoCell1D::AutoCell1D (QWidget *parent) : QWidget(parent) {

    setWindowTitle("Automate 1 dimension");

    // Définir la taille
    largeurL = new QLabel("Largeur : ", this);
    largeur = new QSpinBox(this);
    largeur->setRange(5,100);
    largeur->setValue(50);
    hauteurL = new QLabel("Hauteur : ", this);
    hauteur = new QSpinBox(this);
    hauteur->setRange(5,100);
    hauteur->setValue(50);
    validDimensions = new QPushButton("Changer dimensions", this);
    connect(validDimensions, SIGNAL(clicked()), this, SLOT(setSizeTable()));

    dimensions = new QHBoxLayout;
    dimensions->addWidget(largeurL);
    dimensions->addWidget(largeur);
    dimensions->addWidget(hauteurL);
    dimensions->addWidget(hauteur);
    dimensions->addWidget(validDimensions);

    // _____________________________________________________________________

    num = new QSpinBox(this); // On créé une barre de nombres défilables
    num->setRange(0, 255); // Modification des limites
    num->setValue(30);

    numl = new QLabel("Numéro", this); // Texte au dessus de la barre de nombres défilables

    numc = new QVBoxLayout; // Alignement de num et numl verticalement
    numc->addWidget(numl);
    numc->addWidget(num);

    numeroc = new QHBoxLayout; // Alignement vertical
    numeroc->addLayout(numc);

    zeroOneValidator = new QIntValidator(this); //
    zeroOneValidator->setRange(0,1);

    numeroBitl[0] = new QLabel("111");
    numeroBitl[1] = new QLabel("110");
    numeroBitl[2] = new QLabel("101");
    numeroBitl[3] = new QLabel("100");
    numeroBitl[4] = new QLabel("011");
    numeroBitl[5] = new QLabel("010");
    numeroBitl[6] = new QLabel("001");
    numeroBitl[7] = new QLabel("000");

    for(unsigned int counter = 0; counter < 8; ++counter) {
        numeroBit[counter] = new QLineEdit(this);
        numeroBit[counter]->setMaxLength(1); //caractères
        numeroBit[counter]->setText("0");
        numeroBit[counter]->setValidator(zeroOneValidator);
        bitc[counter] = new QVBoxLayout;
        bitc[counter]->addWidget(numeroBitl[counter]);
        bitc[counter]->addWidget(numeroBit[counter]);
        numeroc->addLayout(bitc[counter]);
        connect(numeroBit[counter], SIGNAL(textChanged(QString)), this, SLOT(synchronizeNumBitToNum(QString)));
    }
    connect(num, SIGNAL(valueChanged(int)), this, SLOT(synchronizeNumToNumBit(int)));

    // Qestion 2
    larg = largeur->value(); haut = hauteur->value();
    depart = new QTableWidget(1, larg, this); //1 ligne, 25 colonnes
    depart->setFixedSize(larg*taille, 50); // largeur = nombre_cellules*taille_cellule, hauteur = taille_cellule
    depart->horizontalHeader()->setVisible(false); // masque le header horizontal
    depart->verticalHeader()->setVisible(false); // masque le header vertical
    depart->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff); // désactive la scroll barre
    depart->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn); // scroll bar largeur necessaire

    // creation des items du QTableWidget, initialisés à "0" avec un fond blanc
    for(unsigned int counter = 0; counter < larg ; ++counter) {
        depart->setColumnWidth(counter, taille);
        depart->setItem(0, counter, new QTableWidgetItem("0"));
        depart->item(0, counter)->setBackgroundColor("white");
        depart->item(0, counter)->setTextColor("white");
    }

    connect(depart, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(cellActivation(QModelIndex)));
    // Question 3
    etats1D = new QTableWidget(haut, larg, this);
    etats1D->horizontalHeader()->setVisible(false);
    etats1D->verticalHeader()->setVisible(false);
    etats1D->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    etats1D->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    etats1D->setEditTriggers(QAbstractItemView::NoEditTriggers); // désactive la modification par l'utilisateur
    // on va créer les items, on utilise 2 boucles car on parcourt un tableau 2 dimensions

    for(unsigned int ligne = 0; ligne < haut; ++ligne) {
        // fixe les dimensions des lignes et des colonnes
        etats1D->setColumnWidth(ligne, taille);
        for(unsigned int colonne = 0; colonne < larg; ++colonne) {
            etats1D->setRowHeight(colonne, taille);
            etats1D->setItem(ligne, colonne, new QTableWidgetItem("0"));
            etats1D->item(ligne, colonne)->setBackgroundColor("white");
            etats1D->item(ligne, colonne)->setTextColor("white");
        }
    }

    simulation = new QPushButton("Simulation d'un pas", this);
    connect(simulation, SIGNAL(clicked()), this, SLOT(launchSimulation()));

    simulationPas = new QPushButton("Simulation pas de temps", this);
    connect(simulationPas, SIGNAL(clicked()), this, SLOT(launchSimulationEtape()));

    pasl = new QLabel("Pas (secondes)", this); // Texte au dessus de la barre de nombres défilables
    pas = new QDoubleSpinBox(this); // On créé une barre de nombres défilables
    pas->setRange(0,2); // Modification des limites
    pas->setValue(0.2);
    pas->setSingleStep(0.2);
    pasc = new QVBoxLayout; // Alignement de num et numl verticalement
    pasc->addWidget(pasl);
    pasc->addWidget(pas);

    timer = new QTimer;
    connect(timer, SIGNAL(timeout()), this, SLOT(launchSimulation()));

    arretSimulation = new QPushButton("Stop", this);
    resetSimulation = new QPushButton("Reset", this);
    connect(arretSimulation, SIGNAL(clicked()), this, SLOT(arret()));
    connect(resetSimulation, SIGNAL(clicked()), this, SLOT(reset()));

    simulationPasLayout = new QHBoxLayout; // Alignement horizontal
    simulationPasLayout->addLayout(pasc);
    simulationPasLayout->addWidget(simulationPas);
    simulationPasLayout->addWidget(arretSimulation);

    //Générateur d'états
    generateur = new QComboBox(this);
    generateur->addItem("Génération aléatoire");
    generateur->addItem("Génération symétrique");
    generer = new QPushButton("Générer Etat ", this);
    generateurL = new QHBoxLayout;
    generateurL->addWidget(generateur);
    generateurL->addWidget(generer);
    connect(generer, SIGNAL(clicked()), this, SLOT(genererEtat()));

    // Charger et Sauver Automate
    nomAutomate = new QLineEdit(this);
    nomAutomate->setPlaceholderText("Saisir le nom de l'automate");
    sauverAutomate = new QPushButton("Enregistrer automate", this);
    chargerAutomate = new QPushButton("Charger un automate", this);
    validerAutomate = new QPushButton("Valider le chargement", this);
    chargementBox = new QComboBox(this);

    connect(sauverAutomate, SIGNAL(clicked()), this, SLOT(saveAutomate()));
    connect(validerAutomate, SIGNAL(clicked()), this, SLOT(validateAutomate()));
    connect(chargerAutomate, SIGNAL(clicked()), this, SLOT(chargeAutomate()));

    sauvCharg = new QHBoxLayout;
    sauvCharg->addWidget(sauverAutomate);
    sauvCharg->addWidget(chargerAutomate);
    sauvCharg->addWidget(chargementBox);
    sauvCharg->addWidget(validerAutomate);
    chargementBox->hide();
    validerAutomate->hide();


    couche = new QVBoxLayout;
    couche->addLayout(dimensions);
    couche->addLayout(numeroc);
    couche->addWidget(depart);
    couche->addLayout(generateurL);
    couche->addWidget(nomAutomate);
    couche->addLayout(sauvCharg);
    couche->addWidget(etats1D);
    couche->addWidget(simulation);
    couche->addLayout(simulationPasLayout);
    couche->addWidget(resetSimulation);

    setLayout(couche);

}

void AutoCell1D::cellActivation(const QModelIndex& index) {
    if (depart->item(0, index.column())->text() == "0") {
        depart->item(0, index.column())->setText("1");
        depart->item(0, index.column())->setBackgroundColor("black");
        depart->item(0, index.column())->setTextColor("black");
    } else {
        depart->item(0, index.column())->setText("0");
        depart->item(0, index.column())->setBackgroundColor("white");
        depart->item(0, index.column())->setTextColor("white");
    }
}

void AutoCell1D::synchronizeNumToNumBit(int i) {
    std::string numBit = NumToNumBit(i);
    for(unsigned int counter = 0; counter < 8; ++counter)
        numeroBit[counter]->setText(QString(numBit[counter]));
}

void AutoCell1D::synchronizeNumBitToNum(const QString& s) {
    if (s == "")
        return;

    std::string numBit = "";
    for(unsigned int counter = 0; counter < 8; ++counter)
        numBit += numeroBit[counter]->text().toStdString();

    num->setValue(NumBitToNum(numBit));
}

void AutoCell1D::launchSimulation() {

    Etat1D e(larg);
    if (step == 0) { // Première étape, on génère l'état par rapport à l'état de départ
        for(unsigned int counter = 0; counter < larg; ++counter) {
            if(depart->item(0, counter)->text() == "1")
                e.setCellule(counter, true);
        }
    }
    else{ // On a passé la première étape, on génère l'état à partir de l'état précédent
        for(unsigned int counter = 0; counter < larg; ++counter) {
            if(etats1D->item((step-1)%haut, counter)->text() == "1")
                e.setCellule(counter, true);
        }
    }

    const Automate1D& a = AutomateManager::getAutomateManager().getAutomate1D(num->value());
    Simulateur1D sim(a, e, larg);
    sim.next();
    const Etat1D& etat = sim.dernier();
   // affichage du dernier état généré
    for(unsigned int colonne = 0; colonne < larg; ++colonne) {
        if (etat.getCellule(colonne) == true) {
            etats1D->item(step%haut, colonne)->setText("1");
            etats1D->item(step%haut, colonne)->setBackgroundColor("black");
            etats1D->item(step%haut, colonne)->setTextColor("black");
        } else {
            etats1D->item(step%haut, colonne)->setText("0");
            etats1D->item(step%haut, colonne)->setBackgroundColor("white");
            etats1D->item(step%haut, colonne)->setTextColor("white");
        }
    }
    step++;
}

void AutoCell1D::launchSimulationEtape() {

    timer->start(1000*pas->value());

}

void AutoCell1D::setSizeTable(){

    larg = largeur->value();
    haut = hauteur->value();
    etats1D->clear();
    depart->clear();
    depart->setColumnCount(larg);
    etats1D->setRowCount(haut);
    etats1D->setColumnCount(larg);
    depart->setRowHeight(0, taille);
    step = 0;
    if(timer->isActive())
        timer->stop();

    for (unsigned int col = 0; col < larg; ++col){
        depart->setColumnWidth(col, taille);
        depart->setItem(0, col, new QTableWidgetItem("0"));
        depart->item(0, col)->setBackgroundColor("white");
        depart->item(0, col)->setTextColor("white");
    }

    for(unsigned int ligne = 0; ligne < haut; ++ligne) {
        // fixe les dimensions des lignes et des colonnes
        etats1D->setRowHeight(ligne, taille);
        for(unsigned int colonne = 0; colonne < larg; ++colonne) {
            etats1D->setColumnWidth(colonne, taille);
            etats1D->setItem(ligne, colonne, new QTableWidgetItem("0"));
            etats1D->item(ligne, colonne)->setBackgroundColor("white");
            etats1D->item(ligne, colonne)->setTextColor("white");
        }
    }
}

void AutoCell1D::arret(){
    if(timer->isActive())
        timer->stop();
}

void AutoCell1D::reset(){

    if(timer->isActive())
        timer->stop();
    step = 0;
    for(unsigned int counter = 0; counter < larg; ++counter) {
        depart->setItem(0, counter, new QTableWidgetItem("0"));
        depart->item(0, counter)->setBackgroundColor("white");
        depart->item(0, counter)->setTextColor("white");
    }

    for(unsigned int ligne = 0; ligne < haut; ++ligne) {
        for(unsigned int colonne = 0; colonne < larg; ++colonne) {
            etats1D->setItem(ligne, colonne, new QTableWidgetItem("0"));
            etats1D->item(ligne, colonne)->setBackgroundColor("white");
            etats1D->item(ligne, colonne)->setTextColor("white");
        }
    }

}

void AutoCell1D::genererEtat() {

    if(timer->isActive())
        timer->stop();

    Etat1D e (larg);
    Generateur g;

    if (generateur->currentText() == "Génération aléatoire")
        g.genererEtatAleatoire(e);
    else if (generateur->currentText()== "Génération symétrique")
        g.genererEtatSymetrique(e);

    for(unsigned int colonne = 0; colonne < larg; ++colonne) {
        if (e.getCellule(colonne) == 1) {
            depart->item(0, colonne)->setText("1");
            depart->item(0, colonne)->setBackgroundColor("black");
            depart->item(0, colonne)->setTextColor("black");
        } else {
            depart->item(0, colonne)->setText("0");
            depart->item(0, colonne)->setBackgroundColor("white");
            depart->item(0, colonne)->setTextColor("white");
        }
    }
}



void AutoCell1D::saveAutomate(){

    QDomDocument document;
    QFile file("automate1D.xml");

    if(!file.open(QIODevice::ReadWrite | QIODevice::Text)) {
        QMessageBox::information(this, "erreur","Failed to open file");
        file.close();
        return;
    }
    QString qs; int y, j;
    if (!document.setContent(&file, false, &qs, &y, &j)) {
        std::cout << "error: " << qs.toStdString() << " " << y << " " << j << std::endl;
        QMessageBox::information(this, "erreur", "Failed to load document");
        file.close();
        return;
    }
    file.close();

    if (nomAutomate->text() == ""){
        QMessageBox::warning(this, "Attention Enregistrement", "Veuillez saisir un nom pour votre automate.");
        return ;
     }

    // Recuperer le root
    QDomElement autom = document.documentElement();

    // Detecter la presence de doublons
    QDomNode noeud = autom.firstChild();
    while(!noeud.isNull()){
        autom = noeud.toElement();
        if(autom.tagName() == nomAutomate->text()) {
            QMessageBox::warning(this, "Attention Enregistrement", "Un automate du meme nom est deja enregistre.");
            return ;
        }
        noeud = noeud.nextSibling();
    }

    // Recuperation de la racine unique
    QDomElement root = document.firstChildElement();

    //Creation d'un element automate
    QDomElement automate = document.createElement(nomAutomate->text());
    automate.setAttribute("Numero", num->value());


    // Creation d'un element Etat
    QDomElement elemEtat = document.createElement("Etat");
    elemEtat.setAttribute("Hauteur", haut);
    elemEtat.setAttribute("Largeur", larg);

    // Création des éléments cellule

    QTableWidgetItem *item;
    QString value;
       for(unsigned int colonne = 0; colonne < larg; ++colonne) {
           QDomElement elemCellule = document.createElement("Cellule");
           item = depart->item(0, colonne);
           value = item->text();
           if (value != "0") {
               elemCellule.setAttribute("Colonne", colonne);
               elemCellule.setAttribute("Valeur", value);
               elemEtat.appendChild(elemCellule);
            }
        }

    automate.appendChild(elemEtat);
    root.appendChild(automate);

    // Ecriture des changements dans le fichier
    if(!file.open(QIODevice::WriteOnly)) {
        QMessageBox::critical(this, "Erreur", "Impossible d'ouvrir le document JDV.xml.");
        return;
    } else {
        QTextStream stream(&file);
        stream<<document.toString();
        file.close();
    }

    QMessageBox::information(this, "Information","Enregistrement reussi.");

}


void AutoCell1D::chargeAutomate(){

    chargerAutomate->hide();
    validerAutomate->show();
    chargementBox->clear();
    chargementBox->show();

    QDomDocument document;
    QFile file ("automate1D.xml");

    if(!file.open(QIODevice::ReadOnly | QIODevice::Text	)) {
        QMessageBox::information(this, "erreur","Failed to open file");
        file.close();
        return;
    }
    QString qs; int y, j;
    if (!document.setContent(&file, false, &qs, &y, &j)) {
        std::cout << "error: " << qs.toStdString() << " " << y << " " << j << std::endl;
        QMessageBox::information(this, "erreur", "Failed to load document");
        file.close();
        return;
    }
    file.close();

    //Listage des automates disponibles dans le fichier
    QDomElement autom = document.documentElement();
    QDomNode noeud = autom.firstChild();
    while(!noeud.isNull()){
        autom = noeud.toElement();
        chargementBox->addItem(autom.tagName());
        noeud = noeud.nextSibling();
    }


}

void AutoCell1D::validateAutomate(){

    chargerAutomate->show();
    validerAutomate->hide();
    chargementBox->hide();

    QDomDocument document;
    QFile file ("automate1D.xml");

    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QMessageBox::information(this, "erreur","Failed to open file");
        file.close();
        return;
    }
    if(!document.setContent(&file)){
        QMessageBox::information(this, "erreur", "Failed to load document");
        file.close();
        return;
    }
    file.close();

    //Recherche de l'automate voulu
    QString nomAuto = chargementBox->currentText();
    QDomElement autom = document.documentElement();
    QDomNode noeud = autom.firstChild();
    while(!noeud.isNull() && autom.tagName()!=nomAuto) {
        autom = noeud.toElement();
        noeud = noeud.nextSibling();
    }

    //Recuperation des attributs de l'automate, de sa configuration
    num->setValue(autom.attribute("Numero").toInt());

    //Recuperer l'etat de l'automate
    QDomElement etat = autom.firstChildElement();
    hauteur->setValue(etat.attribute("Hauteur").toInt());
    largeur->setValue(etat.attribute("Largeur").toInt());
    setSizeTable();

    //Recuperer les cellules
    etat = etat.firstChildElement();
    while(!etat.isNull()) {
        int col = etat.attribute("Colonne").toInt();
        int val = etat.attribute("Valeur").toInt();
        if (val == 1) {
            depart->item(0, col)->setText("1");
            depart->item(0, col)->setBackgroundColor("black");
            depart->item(0, col)->setTextColor("black");
        } else {
            depart->item(0, col)->setText("0");
            depart->item(0, col)->setBackgroundColor("white");
            depart->item(0, col)->setTextColor("white");
        }
        etat=etat.nextSiblingElement();
    }

}
