#ifndef AUTOCELL_H
#define AUTOCELL_H

#include <QComboBox>
#include <QCoreApplication>
#include <QIntValidator>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QSpinBox>
#include <QString>
#include <QTableWidget>
#include <QThread>
#include <QTimer>
#include <QToolBar>
#include <QtXml>
#include <QWidget>

#include "automateManager.h"
/*!
* \file autocell1D.h
* \brief Documentation de notre interface pour l'automate une dimension 
* \author {CHENG Aurelie, DIONE Abdoul Aziz, HUONG-PARRAIN Anathan }
* \date {Printemps 2018}
*/




/*!
* \class AutoCell1D
* \brief Classe qui cree l'interface de l'automate une dimension 
*/

class AutoCell1D : public QWidget{
Q_OBJECT

    // Etat de depart
    QSpinBox* num; // numéro
    QLineEdit* numeroBit[8]; // un QLineEdit par bit
    QLabel* numl;
    QLabel* numeroBitl[8];
    QVBoxLayout* numc;
    QVBoxLayout* bitc[8];
    QHBoxLayout* numeroc;
    QIntValidator* zeroOneValidator;
    // Question 2
    QTableWidget* depart;
    QVBoxLayout* couche;

    // Table d'etats et dimensions
    QTableWidget* etats1D;
    QLabel* largeurL;
    QSpinBox* largeur;
    QLabel* hauteurL;
    QSpinBox* hauteur;
    QPushButton* validDimensions;
    QHBoxLayout* dimensions;
    unsigned int larg, haut;
    unsigned int taille = 25;
    unsigned int step = 0;

    // Simulation
    QTimer* timer;
    QLabel* pasl;
    QDoubleSpinBox* pas;
    QVBoxLayout* pasc;
    QPushButton* simulation;
    QPushButton* simulationPas;
    QHBoxLayout* simulationPasLayout;
    QPushButton* arretSimulation;
    QPushButton* resetSimulation;

    // Generateur d'Etat
    QPushButton* generer;
    QComboBox* generateur;
    QHBoxLayout* generateurL;

    // Sauvegarder et charger en XML
    QLineEdit* nomAutomate;
    QPushButton* sauverAutomate;
    QComboBox* chargementBox;
    QPushButton* chargerAutomate;
    QPushButton* validerAutomate;
    QHBoxLayout* sauvCharg;

public:
    explicit AutoCell1D(QWidget* parent = nullptr);

private slots:
    // Synchroniser les entrées
	/*!
	*\brief Actualise le numero binaire quand l'utilisateur entre un numero entier pour la regle 
	*/
    void synchronizeNumToNumBit(int i);
	/*!
	*\brief Actualise le numero entier quand l'utilisateur entre un numero binaire pour la regle 
	*/
    void synchronizeNumBitToNum(const QString& s);
    // Etats
	/*!
	*\brief Le double clique sur une cellule permet de la rendre vivante si elle etait morte et vice versa
	*/
    void cellActivation(const QModelIndex& index);
	/*!
	*\brief Change les dimensions de la grille
	*/
    void setSizeTable();
	/*!
	*\brief Le clique sur le bouton "Generer Etat" permet de remplir les cellules de l'etat de depart selon le mode choisi
	*/
    void genererEtat();
    //Simulation
	/*!
	*\brief Permet de lancer la simulation selon un pas de temps configurable
	*/
    void launchSimulation();
	/*!
	*\brief La methode permet de lancer la simulation en mode pas a pas : l'utilisateur declenche manuellement le passage
	* de l'etat a l'instant t a l'instant t+1
	*/
    void launchSimulationEtape();
	/*!
	*\brief Suspend le timer, la simulation en mode lecture simple est en pause
	*/
    void arret();
	/*!
	*\brief Efface la grille d'etat remise a l'etat initial avant simulation
	*/
    void reset();
    // Xml
	/*!
	*\brief Enregistre un automate dans un fichier XML
	*/
    void saveAutomate();
	/*!
	*\brief Charge un automate choisi dans un menu deroulant depuis un fichier XML
	*/
    void chargeAutomate();
	/*!
	*\brief Affiche dans un menu deroulant les differents automates enregistres dans le fichier
	*/
    void validateAutomate();
};

#endif // AUTOCELL_H
