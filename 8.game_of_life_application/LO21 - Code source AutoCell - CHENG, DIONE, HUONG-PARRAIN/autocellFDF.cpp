#include "autocellFDF.h"

AutoCellFDF::AutoCellFDF(QWidget *parent) : QWidget(parent) {

    setWindowTitle("Automate Feu de Fôret");

    //Definir la taille de l'etat
    largeurL = new QLabel("Largeur : ", this);
    largeur = new QSpinBox(this);
    largeur->setRange(5,100);
    largeur->setValue(50);
    hauteurL = new QLabel("Hauteur : ", this);
    hauteur = new QSpinBox(this);
    hauteur->setRange(5,100);
    hauteur->setValue(50);
    validDimensions = new QPushButton("Changer dimensions", this);
    connect(validDimensions, SIGNAL(clicked()), this, SLOT(setSizeTable()));
    // Layout des dimensions
    dimensions = new QHBoxLayout;
    dimensions->addWidget(largeurL);
    dimensions->addWidget(largeur);
    dimensions->addWidget(hauteurL);
    dimensions->addWidget(hauteur);
    dimensions->addWidget(validDimensions);


    //Generateur d'etats
    prob = new QLabel("Probabilité");
    probabilite = new QDoubleSpinBox;
    probabilite->setRange(0,1); // Modification des limites
    probabilite->setValue(0.55);
    probabilite->setSingleStep(0.01);
    probL = new QVBoxLayout;
    probL->addWidget(prob);
    probL->addWidget(probabilite);
    generer = new QPushButton("Generer Etat ", this);
    generateurL = new QHBoxLayout;
    generateurL->addLayout(probL);
    generateurL->addWidget(generer);
    connect(generer, SIGNAL(clicked()), this, SLOT(genererEtat()));


    //Taille de l'etat deux dimensions
    larg = largeur->value();
    haut = hauteur->value();
    etats = new QTableWidget(haut, larg, this);
    etats->horizontalHeader()->setVisible(false);
    etats->verticalHeader()->setVisible(false);
    etats->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    etats->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

    for(unsigned int ligne = 0; ligne < haut; ++ligne) {
        // fixe les dimensions des lignes et des colonnes
        etats->setRowHeight(ligne, taille);
        for(unsigned int colonne = 0; colonne < larg; ++colonne) {
            etats->setColumnWidth(colonne, taille);
            etats->setItem(ligne, colonne, new QTableWidgetItem("0"));
            etats->item(ligne, colonne)->setBackgroundColor("white");
            etats->item(ligne, colonne)->setTextColor("white");
        }
    }
    connect(etats, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(cellActivation(QModelIndex)));


    // Simulation
    simulationClik = new QPushButton("Next", this);
    simulationEtape = new QPushButton("Run", this);
    arretSimulation = new QPushButton("Stop", this);
    resetSimulation = new QPushButton("Reset", this);
    timer = new QTimer;

    connect(simulationClik, SIGNAL(clicked()), this, SLOT(launchSimulation()));
    connect(simulationEtape, SIGNAL(clicked()), this, SLOT(launchSimulationEtape()));
    connect(arretSimulation, SIGNAL(clicked()), this, SLOT(arret()));
    connect(resetSimulation, SIGNAL(clicked()), this, SLOT(reset()));
    connect(timer, SIGNAL(timeout()), this, SLOT(launchSimulation()));


    // Charger et Sauver Automate
    nomAutomate = new QLineEdit(this);
    nomAutomate->setPlaceholderText("Saisir le nom de l'automate");
    sauverAutomate = new QPushButton("Enregistrer automate", this);
    chargerAutomate = new QPushButton("Charger un automate", this);
    validerAutomate = new QPushButton("Valider le chargement", this);
    chargementBox = new QComboBox(this);

    connect(sauverAutomate, SIGNAL(clicked()), this, SLOT(saveAutomate()));
    connect(validerAutomate, SIGNAL(clicked()), this, SLOT(validateAutomate()));
    connect(chargerAutomate, SIGNAL(clicked()), this, SLOT(chargeAutomate()));

    sauvCharg = new QHBoxLayout;
    sauvCharg->addWidget(sauverAutomate);
    sauvCharg->addWidget(chargerAutomate);
    sauvCharg->addWidget(chargementBox);
    sauvCharg->addWidget(validerAutomate);
    chargementBox->hide();
    validerAutomate->hide();


    //Layout
    pasl = new QLabel("Pas (secondes)", this); // Texte au dessus de la barre de nombres defilables
    pas = new QDoubleSpinBox(this); // On cree une barre de nombres defilables
    pas->setRange(0, 2); // Modification des limites
    pas->setValue(0.2);
    pas->setSingleStep(0.2);
    pasc = new QVBoxLayout; // Alignement de num et numl verticalement
    pasc->addWidget(pasl);
    pasc->addWidget(pas);

    simulationPas = new QHBoxLayout; // Alignement horizontal
    simulationPas->addLayout(pasc);
    simulationPas->addWidget(simulationEtape);
    simulationPas->addWidget(arretSimulation);

    couche = new QVBoxLayout;
    couche->addLayout(dimensions);
    couche->addLayout(generateurL);
    couche->addWidget(nomAutomate);
    couche->addLayout(sauvCharg);

    couche->addWidget(etats);
    couche->addWidget(simulationClik);
    couche->addLayout(simulationPas);
    couche->addWidget(resetSimulation);

    setLayout(couche);

}

void AutoCellFDF::cellActivation(const QModelIndex& index) {

    if (etats->item(index.row(), index.column())->text() == "0") {
        etats->item(index.row(), index.column())->setText("1");
        etats->item(index.row(), index.column())->setBackgroundColor("green");
        etats->item(index.row(), index.column())->setTextColor("green");
    } else {
        etats->item(index.row(), index.column())->setText("O");
        etats->item(index.row(), index.column())->setBackgroundColor("white");
        etats->item(index.row(), index.column())->setTextColor("white");
    }

}

void AutoCellFDF::arret(){
    if (timer->isActive())
        timer->stop();
}

void AutoCellFDF::launchSimulation(){

    Etat2D e(larg, haut, 4);

    for(unsigned int ligne = 0; ligne < haut; ++ligne) {
         for(unsigned int colonne = 0; colonne < larg; ++colonne) {
             int val = etats->item(ligne, colonne)->text().toInt();
             e.setCellule(ligne, colonne, val);
         }
    }

    const AutomateFDF& a = AutomateManager::getAutomateManager().getAutomateFDF(probabilite->value());
    Simulateur2D sim(a, e);
    sim.next();

    const Etat2D& etat = sim.dernier();

    for (unsigned int ligne = 0; ligne < haut; ++ligne){
        for(unsigned int colonne = 0; colonne < larg; ++colonne) {
            if (etat.getCellule(ligne, colonne) == 1) {
                etats->item(ligne, colonne)->setText("1");
                etats->item(ligne, colonne)->setBackgroundColor("green");
                etats->item(ligne, colonne)->setTextColor("green");
            }
            else if (etat.getCellule(ligne, colonne) == 2) {
                etats->item(ligne, colonne)->setText("2");
                etats->item(ligne, colonne)->setBackgroundColor("red");
                etats->item(ligne, colonne)->setTextColor("red");
            }
            else if (etat.getCellule(ligne, colonne) == 3) {
                etats->item(ligne, colonne)->setText("3");
                etats->item(ligne, colonne)->setBackgroundColor("grey");
                etats->item(ligne, colonne)->setTextColor("grey");
            }
            else {
                etats->item(ligne, colonne)->setText("0");
                etats->item(ligne, colonne)->setBackgroundColor("white");
                etats->item(ligne, colonne)->setTextColor("white");
            }
        }
    }

}

void AutoCellFDF::reset() {

    if (timer->isActive())
        timer->stop();

    for (unsigned int ligne = 0; ligne < haut; ++ligne){
        for(unsigned int colonne = 0; colonne < larg; ++colonne) {
            if (etats->item(ligne, colonne)->text() != "0") {
                etats->item(ligne, colonne)->setText("0");
                etats->item(ligne, colonne)->setBackgroundColor("white");
                etats->item(ligne, colonne)->setTextColor("white");
            }
        }
    }

}

void AutoCellFDF::launchSimulationEtape() {
    timer->start(1000*pas->value());
}

void AutoCellFDF::setSizeTable() {
    if (timer->isActive())
        timer->stop();

    larg = largeur->value();
    haut = hauteur->value();
    etats->clear();
    etats->setRowCount(haut);
    etats->setColumnCount(larg);

    for(unsigned int ligne = 0; ligne < haut; ++ligne) {
        // fixe les dimensions des lignes et des colonnes
        etats->setRowHeight(ligne, taille);
        for(unsigned int colonne = 0; colonne < larg; ++colonne) {
            etats->setColumnWidth(colonne, taille);
            etats->setItem(ligne, colonne, new QTableWidgetItem("0"));
            etats->item(ligne, colonne)->setBackgroundColor("white");
            etats->item(ligne, colonne)->setTextColor("white");
        }
    }

}

void AutoCellFDF::genererEtat() {

    if(timer->isActive())
        timer->stop();

    Etat2D e (larg, haut, 4);
    Generateur g;

    g.genererEtatFDF(e, probabilite->value());

    int value = 0;
    for(unsigned int ligne = 0; ligne < haut; ++ligne){
        for(unsigned int colonne = 0; colonne < larg; ++colonne) {
            value = e.getCellule(ligne, colonne);
            if (value == 1) {
                etats->item(ligne, colonne)->setText("1");
                etats->item(ligne, colonne)->setBackgroundColor("green");
                etats->item(ligne, colonne)->setTextColor("green");
            } else if (value == 2) {
                etats->item(ligne, colonne)->setText("2");
                etats->item(ligne, colonne)->setBackgroundColor("red");
                etats->item(ligne, colonne)->setTextColor("red");
            } else if (value == 0) {
                etats->item(ligne, colonne)->setText("0");
                etats->item(ligne, colonne)->setBackgroundColor("white");
                etats->item(ligne, colonne)->setTextColor("white");
            }
        }
    }

}

void AutoCellFDF::saveAutomate(){

    QDomDocument document;
    QFile file("automateFDF.xml");

    if(!file.open(QIODevice::ReadWrite | QIODevice::Text)) {
        QMessageBox::information(this, "erreur","Failed to open file");
        file.close();
        return;
    }
    QString qs; int y, j;
    if (!document.setContent(&file, false, &qs, &y, &j)) {
        std::cout << "error: " << qs.toStdString() << " " << y << " " << j << std::endl;
        QMessageBox::information(this, "erreur", "Failed to load document");
        file.close();
        return;
    }
    file.close();

    if (nomAutomate->text() == ""){
        QMessageBox::warning(this, "Attention Enregistrement", "Veuillez saisir un nom pour votre automate.");
        return ;
     }

    // Recuperer le root
    QDomElement autom = document.documentElement();

    // Detecter la presence de doublons
    QDomNode noeud = autom.firstChild();
    while(!noeud.isNull()){
        autom = noeud.toElement();
        if(autom.tagName() == nomAutomate->text()) {
            QMessageBox::warning(this, "Attention Enregistrement", "Un automate du meme nom est deja enregistre.");
            return ;
        }
        noeud = noeud.nextSibling();
    }

    // Recuperation de la racine unique
    QDomElement root = document.firstChildElement();

    //Creation d'un element automate
    QDomElement automate = document.createElement(nomAutomate->text());
    automate.setAttribute("Probabilite", probabilite->value());

    // Creation d'un element Etat
    QDomElement elemEtat = document.createElement("Etat");
    elemEtat.setAttribute("Hauteur", haut);
    elemEtat.setAttribute("Largeur", larg);

    // Creation des elements cellule
    QTableWidgetItem *item;
    int value;
    for(unsigned int ligne = 0; ligne < haut; ++ligne){
        for(unsigned int colonne = 0; colonne < larg; ++colonne) {
            QDomElement elemCellule = document.createElement("Cellule");
            item = etats->item(ligne,colonne);
            value = item->text().toInt();
            if (value != 0) {
                elemCellule.setAttribute("Colonne", colonne);
                elemCellule.setAttribute("Ligne", ligne);
                elemCellule.setAttribute("Valeur", value);
                elemEtat.appendChild(elemCellule);
            }
        }
    }

    automate.appendChild(elemEtat);
    root.appendChild(automate);

    // Ecriture des changements dans le fichier
    if(!file.open(QIODevice::WriteOnly)) {
        QMessageBox::critical(this, "Erreur", "Impossible d'ouvrir le document JDV.xml.");
        return;
    } else {
        QTextStream stream(&file);
        stream<<document.toString();
        file.close();
    }

    QMessageBox::information(this, "Information","Enregistrement reussi.");

}


void AutoCellFDF::chargeAutomate(){

    chargerAutomate->hide();
    validerAutomate->show();
    chargementBox->clear();
    chargementBox->show();
    if(timer->isActive())
        timer->stop();

    QDomDocument document;
    QFile file ("automateFDF.xml");

    if(!file.open(QIODevice::ReadOnly | QIODevice::Text	)) {
        QMessageBox::information(this, "erreur","Failed to open file");
        file.close();
        return;
    }
    QString qs; int y, j;
    if (!document.setContent(&file, false, &qs, &y, &j)) {
        std::cout << "error: " << qs.toStdString() << " " << y << " " << j << std::endl;
        QMessageBox::information(this, "erreur", "Failed to load document");
        file.close();
        return;
    }
    file.close();

    //Listage des automates disponibles dans le fichier
    QDomElement autom = document.documentElement();
    QDomNode noeud = autom.firstChild();
    while(!noeud.isNull()){
        autom = noeud.toElement();
        chargementBox->addItem(autom.tagName());
        noeud = noeud.nextSibling();
    }

}

void AutoCellFDF::validateAutomate(){

    chargerAutomate->show();
    validerAutomate->hide();
    chargementBox->hide();

    QDomDocument document;
    QFile file ("automateFDF.xml");

    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QMessageBox::information(this, "erreur","Failed to open file");
        file.close();
        return;
    }
    if(!document.setContent(&file)){
        QMessageBox::information(this, "erreur", "Failed to load document");
        file.close();
        return;
    }
    file.close();

    //Recherche de l'automate voulu
    QString nomAuto = chargementBox->currentText();
    QDomElement autom = document.documentElement();
    QDomNode noeud = autom.firstChild();
    while(!noeud.isNull() && autom.tagName()!=nomAuto) {
        autom = noeud.toElement();
        noeud = noeud.nextSibling();
    }

    //Recuperation des attributs de l'automate, de sa configuration
    probabilite->setValue(autom.attribute("Probabilite").toFloat());

    //Recuperer l'etat de l'automate
    QDomElement etat = autom.firstChildElement();
    hauteur->setValue(etat.attribute("Hauteur").toInt());
    largeur->setValue(etat.attribute("Largeur").toInt());
    setSizeTable();

    //Recuperer les cellules
    etat = etat.firstChildElement();
    while(!etat.isNull()) {
        int lig = etat.attribute("Ligne").toInt();
        int col = etat.attribute("Colonne").toInt();
        int val = etat.attribute("Valeur").toInt();
        if (val == 1) {
            etats->item(lig, col)->setText("1");
            etats->item(lig, col)->setBackgroundColor("green");
            etats->item(lig, col)->setTextColor("green");
        } else if (val == 2) {
            etats->item(lig, col)->setText("2");
            etats->item(lig, col)->setBackgroundColor("red");
            etats->item(lig, col)->setTextColor("red");
        } else if (val == 3) {
            etats->item(lig, col)->setText("3");
            etats->item(lig, col)->setBackgroundColor("grey");
            etats->item(lig, col)->setTextColor("grey");
        } else {
            etats->item(lig, col)->setText("0");
            etats->item(lig, col)->setBackgroundColor("white");
            etats->item(lig, col)->setTextColor("white");
        }
        etat=etat.nextSiblingElement();
    }

}
