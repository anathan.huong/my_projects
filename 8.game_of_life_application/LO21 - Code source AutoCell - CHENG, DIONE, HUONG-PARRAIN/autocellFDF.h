#ifndef AUTOCELLFDF_H
#define AUTOCELLFDF_H


#include "autocellJDV.h"
/*!
* \file autocellFDF.h
* \brief Documentation de notre interface pour l'automate Feu de Foret
* \author {CHENG Aurelie, DIONE Abdoul Aziz, HUONG-PARRAIN Anathan }
* \date {Printemps 2018}
*/




/*!
* \class AutoCellFDF
* \brief Classe qui cree l'interface de l'automate2D Feu de Foret
*/
class AutoCellFDF : public QWidget {
	Q_OBJECT


	// Taille de l'�tat
	QLabel* largeurL;
	QLabel* hauteurL;
	QSpinBox* largeur;
	QSpinBox* hauteur;
	QPushButton* validDimensions;
	QHBoxLayout* dimensions;

	// Generateur, propbabilite -> densite de la f�ret
	QLabel* prob;
	QDoubleSpinBox* probabilite;
	QVBoxLayout* probL;
	QPushButton* generer;
	QHBoxLayout* generateurL;

	// Tableau etat
	QTableWidget* etats;
	unsigned int taille = 25;
	unsigned int larg, haut;

	// Simulation
	QDoubleSpinBox* pas;
	QLabel* pasl;
	QVBoxLayout* pasc;
	QPushButton* simulationEtape;
	QPushButton* simulationClik;
	QPushButton* arretSimulation;
	QPushButton* resetSimulation;
	QHBoxLayout* simulationPas;
	QTimer* timer;

	// Charger sauver
	QLineEdit* nomAutomate;
	QPushButton* sauverAutomate;
	QPushButton* chargerAutomate;
	QComboBox* chargementBox;
	QPushButton* validerAutomate;
	QHBoxLayout* sauvCharg;

	// Layout
	QVBoxLayout* couche;


public:
	explicit AutoCellFDF(QWidget* parent = nullptr);

	private slots:

	// Etat
	/*!
	*\brief Change les dimensions de la grille
	*/
	void setSizeTable();
	/*!
	*\brief Le clique sur le bouton "Generer Etat" permet de remplir les cellules selon le mode choisi
	*/
	void genererEtat();
	/*!
	*\brief Le double clique sur une cellule permet de la rendre vivante si elle etait morte et vice versa
	*/
	void cellActivation(const QModelIndex& index);
	// Simu
	/*!
	*\brief La methode permet de lancer la simulation en mode pas a pas : l'utilisateur declenche manuellement le passage
	* de l'etat a l'instant t a l'instant t+1
	*/
	void launchSimulationEtape();
	/*!
	*\brief Permet de lancer la simulation selon un pas de temps configurable
	*/
	void launchSimulation();
	/*!
	*\brief Efface la grille d'etat remise a l'etat initial avant simulation
	*/
	void reset();
	/*!
	*\brief Suspend le timer, la simulation en mode lecture simple est en pause
	*/
	void arret();

	// XML
	/*!
	*\brief Enregistre un automate dans un fichier XML
	*/
	void saveAutomate(); //ecrit dans XML
	/*!
	*\brief Charge un automate choisi dans un menu deroulant depuis un fichier XML
	*/
	void validateAutomate();
	/*!
	*\brief Affiche dans un menu deroulant les differents automates enregistres dans le fichier
	*/
	void chargeAutomate();

};


#endif // AUTOCELLFDF_H
