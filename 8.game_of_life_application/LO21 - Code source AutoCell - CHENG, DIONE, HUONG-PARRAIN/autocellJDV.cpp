#include "autocellJDV.h"

AutoCellJDV::AutoCellJDV(QWidget *parent) : QWidget(parent) {

    setWindowTitle("Automate 2 dimensions Jeu De La Vie");

    // numMin
    numMinAl = new QLabel("Num Min Alive", this); // Texte au dessus de la barre de nombres defilables
    numMinA = new QSpinBox(this); // On cree une barre de nombres defilables
    numMinA->setRange(0, 9); // Modification des limites
    numMinA->setValue(2);
    numMinAc = new QVBoxLayout; // Alignement de num et numl verticalement
    numMinAc->addWidget(numMinAl);
    numMinAc->addWidget(numMinA);

    numMinDl = new QLabel("Num Min Dead", this); // Texte au dessus de la barre de nombres defilables
    numMinD = new QSpinBox(this); // On cree une barre de nombres defilables
    numMinD->setRange(0, 9); // Modification des limites
    numMinD->setValue(2);
    numMinDc = new QVBoxLayout; // Alignement de num et numl verticalement
    numMinDc->addWidget(numMinDl);
    numMinDc->addWidget(numMinD);

    // numMax

    numMaxAl = new QLabel("Num Max Alive", this); // Texte au dessus de la barre de nombres defilables
    numMaxA = new QSpinBox(this); // On cree une barre de nombres defilables
    numMaxA->setRange(0, 9); // Modification des limites
    numMaxA->setValue(5);
    numMaxAc = new QVBoxLayout; // Alignement de num et numl verticalement
    numMaxAc->addWidget(numMaxAl);
    numMaxAc->addWidget(numMaxA);

    numMaxDl = new QLabel("Num Max Dead", this); // Texte au dessus de la barre de nombres defilables
    numMaxD = new QSpinBox(this); // On cree une barre de nombres defilables
    numMaxD->setRange(0, 9); // Modification des limites
    numMaxD->setValue(5);
    numMaxDc = new QVBoxLayout; // Alignement de num et numl verticalement
    numMaxDc->addWidget(numMaxDl);
    numMaxDc->addWidget(numMaxD);

    // numeros
    numerosA = new QHBoxLayout; // Alignement horizontal
    numerosA->addLayout(numMinAc);
    numerosA->addLayout(numMaxAc);

    numerosD = new QHBoxLayout; // Alignement horizontal
    numerosD->addLayout(numMinDc);
    numerosD->addLayout(numMaxDc);


    //Definir la taille de l'etat
    largeurL = new QLabel("Largeur : ", this);
    largeur = new QSpinBox(this);
    largeur->setRange(5,100);
    largeur->setValue(50);
    hauteurL = new QLabel("Hauteur : ", this);
    hauteur = new QSpinBox(this);
    hauteur->setRange(5,100);
    hauteur->setValue(50);
    validDimensions = new QPushButton("Changer dimensions", this);
    connect(validDimensions, SIGNAL(clicked()), this, SLOT(setSizeTable()));

    dimensions = new QHBoxLayout;
    dimensions->addWidget(largeurL);
    dimensions->addWidget(largeur);
    dimensions->addWidget(hauteurL);
    dimensions->addWidget(hauteur);
    dimensions->addWidget(validDimensions);


    //Generateur d'etats
    generateur = new QComboBox(this);
    generateur->addItem("Generation aleatoire");
    generateur->addItem("Generation symetrique");
    generer = new QPushButton("Generer Etat ", this);
    generateurL = new QHBoxLayout;
    generateurL->addWidget(generateur);
    generateurL->addWidget(generer);
    connect(generer, SIGNAL(clicked()), this, SLOT(genererEtat()));


    //Taille de l'etat deux dimensions
    larg = largeur->value();
    haut = hauteur->value();
    etats = new QTableWidget(haut, larg, this);

    etats->horizontalHeader()->setVisible(false);
    etats->verticalHeader()->setVisible(false);
    etats->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    etats->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

    for(unsigned int ligne = 0; ligne < haut; ++ligne) {
        // fixe les dimensions des lignes et des colonnes
        etats->setRowHeight(ligne, taille);
        for(unsigned int colonne = 0; colonne < larg; ++colonne) {
            etats->setColumnWidth(colonne, taille);
            etats->setItem(ligne, colonne, new QTableWidgetItem("0"));
            etats->item(ligne, colonne)->setBackgroundColor("white");
            etats->item(ligne, colonne)->setTextColor("white");
        }
    }
    connect(etats, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(cellActivation(QModelIndex)));

    // Simulation
    simulationClik = new QPushButton("Next", this);
    simulationEtape = new QPushButton("Run", this);
    arretSimulation = new QPushButton("Stop", this);
    resetSimulation = new QPushButton("Reset", this);
    timer = new QTimer;

    connect(simulationClik, SIGNAL(clicked()), this, SLOT(launchSimulation()));
    connect(simulationEtape, SIGNAL(clicked()), this, SLOT(launchSimulationEtape()));
    connect(arretSimulation, SIGNAL(clicked()), this, SLOT(arret()));
    connect(resetSimulation, SIGNAL(clicked()), this, SLOT(reset()));
    connect(timer, SIGNAL(timeout()), this, SLOT(launchSimulation()));

    // Layout Simulation
    pasl = new QLabel("Pas (secondes)", this); // Texte au dessus de la barre de nombres defilables
    pas = new QDoubleSpinBox(this); // On cree une barre de nombres defilables
    pas->setRange(0, 2); // Modification des limites
    pas->setValue(0.2);
    pas->setSingleStep(0.2);
    pasc = new QVBoxLayout; // Alignement de num et numl verticalement
    pasc->addWidget(pasl);
    pasc->addWidget(pas);

    simulationPas = new QHBoxLayout; // Alignement horizontal
    simulationPas->addLayout(pasc);
    simulationPas->addWidget(simulationEtape);
    simulationPas->addWidget(arretSimulation);


    // Charger et Sauver Automate
    nomAutomate = new QLineEdit(this);
    nomAutomate->setPlaceholderText("Saisir le nom de l'automate");
    sauverAutomate = new QPushButton("Enregistrer automate", this);
    chargerAutomate = new QPushButton("Charger un automate", this);
    validerAutomate = new QPushButton("Valider le chargement", this);
    chargementBox = new QComboBox(this);


    connect(sauverAutomate, SIGNAL(clicked()), this, SLOT(saveAutomate()));
    connect(validerAutomate, SIGNAL(clicked()), this, SLOT(validateAutomate()));
    connect(chargerAutomate, SIGNAL(clicked()), this, SLOT(chargeAutomate()));

    sauvCharg = new QHBoxLayout;
    sauvCharg->addWidget(sauverAutomate);
    sauvCharg->addWidget(chargerAutomate);
    sauvCharg->addWidget(chargementBox);
    sauvCharg->addWidget(validerAutomate);
    chargementBox->hide();
    validerAutomate->hide();


    //setLayout
    couche = new QVBoxLayout;
    couche->addLayout(numerosA);
    couche->addLayout(numerosD);
    couche->addLayout(dimensions);
    couche->addLayout(generateurL);
    couche->addWidget(nomAutomate);
    couche->addLayout(sauvCharg);

    couche->addWidget(etats);
    couche->addWidget(simulationClik);
    couche->addLayout(simulationPas);
    couche->addWidget(resetSimulation);

    setLayout(couche);

}

void AutoCellJDV::cellActivation(const QModelIndex& index) {
    if (etats->item(index.row(), index.column())->text() == "0") {
        etats->item(index.row(), index.column())->setText("1");
        etats->item(index.row(), index.column())->setBackgroundColor("black");
        etats->item(index.row(), index.column())->setTextColor("black");
    } else {
        etats->item(index.row(), index.column())->setText("0");
        etats->item(index.row(), index.column())->setBackgroundColor("white");
        etats->item(index.row(), index.column())->setTextColor("white");
    }
}

void AutoCellJDV::arret(){
    if(timer->isActive())
        timer->stop();
}

void AutoCellJDV::launchSimulation() {

    larg = largeur->value();
    haut = hauteur->value();
    Etat2D e(larg, haut);

    for(unsigned int ligne = 0; ligne < haut; ++ligne) {
         for(unsigned int colonne = 0; colonne < larg; ++colonne) {
             int val = etats->item(ligne, colonne)->text().toInt();
             e.setCellule(ligne, colonne, val);
         }
    }

    if (numMinA->value()>numMaxA->value() || numMinD->value()>numMaxD->value()) {
        QMessageBox::warning(this, "Erreur sur les regles",  "Arguments incorrects !");
        if(timer->isActive())
            timer->stop();
        return;
    }

    const AutomateJDV& a = AutomateManager::getAutomateManager().getAutomateJDV(numMinA->value(),numMaxA->value(),numMinD->value(),numMaxD->value());
    Simulateur2D sim(a, e);
    sim.next();
    const Etat2D& etat = sim.dernier();

    for (unsigned int ligne = 0; ligne < haut; ++ligne){
        for(unsigned int colonne = 0; colonne < larg; ++colonne) {
            if (etat.getCellule(ligne, colonne) == 1) {
                etats->item(ligne, colonne)->setText("1");
                etats->item(ligne, colonne)->setBackgroundColor("black");
                etats->item(ligne, colonne)->setTextColor("black");
            } else {
                etats->item(ligne, colonne)->setText("0");
                etats->item(ligne, colonne)->setBackgroundColor("white");
                etats->item(ligne, colonne)->setTextColor("white");
            }
        }
    }

}

void AutoCellJDV::reset() {
    if(timer->isActive())
        timer->stop();
    // Faire une fonction clear pour factoriser et faire la verification si une cellule est dej? blanche on fait rien
    for (unsigned int ligne = 0; ligne < haut; ++ligne){
        for(unsigned int colonne = 0; colonne < larg; ++colonne) {
            etats->item(ligne, colonne)->setText("0");
            etats->item(ligne, colonne)->setBackgroundColor("white");
            etats->item(ligne, colonne)->setTextColor("white");
        }
    }
}

void AutoCellJDV::launchSimulationEtape() {
    timer->start(1000*pas->value());
}

void AutoCellJDV::setSizeTable() {
    larg = largeur->value();
    haut = hauteur->value();
    etats->clear();
    etats->setRowCount(haut);
    etats->setColumnCount(larg);
    if(timer->isActive())
        timer->stop();

    for(unsigned int ligne = 0; ligne < haut; ++ligne) {
        // fixe les dimensions des lignes et des colonnes
        etats->setRowHeight(ligne, taille);
        for(unsigned int colonne = 0; colonne < larg; ++colonne) {
            etats->setColumnWidth(colonne, taille);
            etats->setItem(ligne, colonne, new QTableWidgetItem("0"));
            etats->item(ligne, colonne)->setBackgroundColor("white");
            etats->item(ligne, colonne)->setTextColor("white");
        }
    }
}

void AutoCellJDV::genererEtat() {

    if (timer->isActive())
        timer->stop();

    Etat2D e (larg, haut);
    Generateur g;

    if (generateur->currentText() == "Generation aleatoire")
        g.genererEtatAleatoire(e);
    else if (generateur->currentText()== "Generation symetrique")
        g.genererEtatSymetrique(e);

    for(unsigned int ligne = 0; ligne < haut; ++ligne){
        for(unsigned int colonne = 0; colonne < larg; ++colonne) {
            if (e.getCellule(ligne, colonne) == 1) {
                etats->item(ligne, colonne)->setText("1");
                etats->item(ligne, colonne)->setBackgroundColor("black");
                etats->item(ligne, colonne)->setTextColor("black");
            } else {
                etats->item(ligne, colonne)->setText("0");
                etats->item(ligne, colonne)->setBackgroundColor("white");
                etats->item(ligne, colonne)->setTextColor("white");
            }
        }
    }

}

void AutoCellJDV::saveAutomate(){

    QDomDocument document;
    QFile file("automateJDV.xml");

    if(!file.open(QIODevice::ReadWrite | QIODevice::Text)) {
        QMessageBox::information(this, "erreur","Failed to open file");
        file.close();
        return;
    }
    QString qs; int y, j;
    if (!document.setContent(&file, false, &qs, &y, &j)) {
        std::cout << "error: " << qs.toStdString() << " " << y << " " << j << std::endl;
        QMessageBox::information(this, "erreur", "Failed to load document");
        file.close();
        return;
    }
    file.close();

    if (nomAutomate->text() == ""){
        QMessageBox::warning(this, "Attention Enregistrement", "Veuillez saisir un nom pour votre automate.");
        return ;
     }

    // Recuperer le root
    QDomElement autom = document.documentElement();

    // Detecter la presence de doublons
    QDomNode noeud = autom.firstChild();
    while(!noeud.isNull()){
        autom = noeud.toElement();
        if(autom.tagName() == nomAutomate->text()) {
            QMessageBox::warning(this, "Attention Enregistrement", "Un automate du meme nom est deja enregistre.");
            return ;
        }
        noeud = noeud.nextSibling();
    }

    // Recuperation de la racine unique
    QDomElement root = document.firstChildElement();

    //Creation d'un element automate
    QDomElement automate = document.createElement(nomAutomate->text());
    automate.setAttribute("NumMinAlive", numMinA->value());
    automate.setAttribute("NumMaxAlive", numMaxA->value());
    automate.setAttribute("NumMinDead", numMinD->value());
    automate.setAttribute("NumMaxDead", numMaxD->value());

    // Creation d'un element Etat
    QDomElement elemEtat = document.createElement("Etat");
    elemEtat.setAttribute("Hauteur", haut);
    elemEtat.setAttribute("Largeur", larg);

    // Creation des elements cellule
    QTableWidgetItem *item;
    QString value;
    for(unsigned int ligne = 0; ligne < haut; ++ligne){
        for(unsigned int colonne = 0; colonne < larg; ++colonne) {
            QDomElement elemCellule = document.createElement("Cellule");
            item = etats->item(ligne,colonne);
            value = item->text();
            if (value == "1") {
                elemCellule.setAttribute("Colonne", colonne);
                elemCellule.setAttribute("Ligne", ligne);
                elemCellule.setAttribute("Valeur", value);
                elemEtat.appendChild(elemCellule);
            }
        }
    }

    automate.appendChild(elemEtat);
    root.appendChild(automate);

    // Ecriture des changements dans le fichier
    if(!file.open(QIODevice::WriteOnly)) {
        QMessageBox::critical(this, "Erreur", "Impossible d'ouvrir le document automateJDV.xml.");
        return;
    } else {
        QTextStream stream(&file);
        stream<<document.toString();
        file.close();
    }

    QMessageBox::information(this, "Information","Enregistrement reussi.");

}


void AutoCellJDV::chargeAutomate(){

    chargerAutomate->hide();
    validerAutomate->show();
    chargementBox->clear();
    chargementBox->show();
    if(timer->isActive())
        timer->stop();

    QDomDocument document;
    QFile file ("automateJDV.xml");

    if(!file.open(QIODevice::ReadOnly | QIODevice::Text	)) {
        QMessageBox::information(this, "erreur","Failed to open file");
        file.close();
        return;
    }
    QString qs; int y, j;
    if (!document.setContent(&file, false, &qs, &y, &j)) {
        std::cout << "error: " << qs.toStdString() << " " << y << " " << j << std::endl;
        QMessageBox::information(this, "erreur", "Failed to load document");
        file.close();
        return;
    }
    file.close();

    //Listage des automates disponibles dans le fichier
    QDomElement autom = document.documentElement();
    QDomNode noeud = autom.firstChild();
    while(!noeud.isNull()){
        autom = noeud.toElement();
        chargementBox->addItem(autom.tagName());
        noeud = noeud.nextSibling();
    }


}

void AutoCellJDV::validateAutomate(){

    chargerAutomate->show();
    validerAutomate->hide();
    chargementBox->hide();

    QDomDocument document;
    QFile file ("automateJDV.xml");

    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QMessageBox::information(this, "erreur","Failed to open file");
        file.close();
        return;
    }
    if(!document.setContent(&file)){
        QMessageBox::information(this, "erreur", "Failed to load document");
        file.close();
        return;
    }
    file.close();

    //Recherche de l'automate voulu
    QString nomAuto = chargementBox->currentText();
    QDomElement autom = document.documentElement();
    QDomNode noeud = autom.firstChild();
    while(!noeud.isNull() && autom.tagName()!=nomAuto) {
        autom = noeud.toElement();
        noeud = noeud.nextSibling();
    }

    //Recuperation des attributs de l'automate, de sa configuration
    numMinA->setValue(autom.attribute("NumMinAlive").toInt());
    numMaxA->setValue(autom.attribute("NumMaxAlive").toInt());
    numMinD->setValue(autom.attribute("NumMinDead").toInt());
    numMaxD->setValue(autom.attribute("NumMaxDead").toInt());

    //Recuperer l'etat de l'automate
    QDomElement etat = autom.firstChildElement();
    hauteur->setValue(etat.attribute("Hauteur").toInt());
    largeur->setValue(etat.attribute("Largeur").toInt());
    setSizeTable();

    //Recuperer les cellules
    etat = etat.firstChildElement();
    while(!etat.isNull()) {
        int lig = etat.attribute("Ligne").toInt();
        int col = etat.attribute("Colonne").toInt();
        int val = etat.attribute("Valeur").toInt();
        if (val == 1) {
            etats->item(lig, col)->setText("1");
            etats->item(lig, col)->setBackgroundColor("black");
            etats->item(lig, col)->setTextColor("black");
        } else {
            etats->item(lig, col)->setText("0");
            etats->item(lig, col)->setBackgroundColor("white");
            etats->item(lig, col)->setTextColor("white");
        }
        etat=etat.nextSiblingElement();
    }

}
