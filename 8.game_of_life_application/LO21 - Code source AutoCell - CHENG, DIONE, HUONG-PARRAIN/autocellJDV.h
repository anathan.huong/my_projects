#ifndef AUTOCELLJDV_H
#define AUTOCELLJDV_H

#include "autocell1D.h"

/*!
* \file autocellJDV.h
* \brief Documentation de notre interface pour l'automate Jeu de la Vie
* \author {CHENG Aurelie, DIONE Abdoul Aziz, HUONG-PARRAIN Anathan }
* \date {Printemps 2018}
*/




/*!
* \class AutoCellJDV
* \brief Classe qui cree l'interface de l'automate2D jeu de la vie 
*/
class AutoCellJDV : public QWidget{ // vérifier que l'on utilise tout
Q_OBJECT

    // Informations sur l'automate
    QSpinBox* numMinA; // numéro
    QLabel* numMinAl;
    QVBoxLayout* numMinAc;
    QSpinBox* numMaxA; // numéro
    QLabel* numMaxAl;
    QVBoxLayout* numMaxAc;
    QHBoxLayout* numerosA;
    QSpinBox* numMinD; // numéro
    QLabel* numMinDl;
    QVBoxLayout* numMinDc;
    QSpinBox* numMaxD; // numéro
    QLabel* numMaxDl;
    QVBoxLayout* numMaxDc;
    QHBoxLayout* numerosD;

    // Taille du tableau d'etats
    QLabel* largeurL;
    QSpinBox* largeur;
    QLabel* hauteurL;
    QSpinBox* hauteur;
    QPushButton* validDimensions;
    QHBoxLayout* dimensions;
    QTableWidget* etats;
    unsigned int taille = 20;
    unsigned int larg, haut;

    // Generateur d'etat
    QComboBox* generateur;
    QComboBox* chargementBox;
    QPushButton* generer;
    QHBoxLayout* generateurL;

    // Simulation
    QTimer* timer;
    QDoubleSpinBox* pas; // numéro
    QLabel* pasl;
    QHBoxLayout* simulationPas;
    QPushButton* simulationEtape;
    QPushButton* simulationClik;
    QPushButton* arretSimulation;
    QPushButton* resetSimulation;
    QVBoxLayout* pasc;

    //Sauvegarder et charger avec XML
    QLineEdit* nomAutomate;//pour le fichier xml
    QPushButton* sauverAutomate;
    QPushButton* chargerAutomate;
    QPushButton* validerAutomate;
    QHBoxLayout* sauvCharg;

    // Layout
    QVBoxLayout* couche;

public:
    explicit AutoCellJDV(QWidget* parent = nullptr);

private slots:
    // Etats
	/*!
	*\brief Le double clique sur une cellule permet de la rendre vivante si elle etait morte et vice versa 
	*/
    void cellActivation(const QModelIndex& index);
	/*!
	*\brief Change les dimensions de la grille 
	*/
    void setSizeTable();
	/*!
	*\brief Le clique sur le bouton "Generer Etat" permet de remplir les cellules selon le mode choisi
	*/
    void genererEtat();
    //Simulation
	/*!
	*\brief Permet de lancer la simulation selon un pas de temps configurable 
	*/
    void launchSimulation();
	/*!
	*\brief La methode permet de lancer la simulation en mode pas a pas : l'utilisateur declenche manuellement le passage
	* de l'etat a l'instant t a l'instant t+1 
	*/
    void launchSimulationEtape();
	/*!
	*\brief Efface la grille d'etat remise a l'etat initial avant simulation 
	*/
    void reset();
	/*!
	*\brief Suspend le timer, la simulation en mode lecture simple est en pause 
	*/
    void arret();
    //SauvCharg
	/*!
	*\brief Enregistre un automate dans un fichier XML
	*/
    void saveAutomate(); 
	/*!
	*\brief Charge un automate choisi dans un menu deroulant depuis un fichier XML
	*/
    void validateAutomate(); 
	/*!
	*\brief Affiche dans un menu deroulant les differents automates enregistres dans le fichier 
	*/
    void chargeAutomate();

};

#endif // AUTOCELLJDV_H
