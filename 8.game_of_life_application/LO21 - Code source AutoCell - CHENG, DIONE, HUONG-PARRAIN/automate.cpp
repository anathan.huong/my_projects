#include "automate.h"

using namespace std;

//Automate 1D
short unsigned int NumBitToNum(const std::string& num) {
    if (num.size() != 8) throw AutomateException("Numero d'automate indefini");
    int puissance = 1;
    short unsigned int numero = 0;
    for (int i = 7; i >= 0; i--) {
        if (num[i] == '1') numero += puissance;
        else if (num[i] != '0') throw AutomateException("Numero d'automate indefini");
        puissance *= 2;
    }
    return numero;
}

std::string NumToNumBit(short unsigned int num) {
    std::string numeroBit;
    if (num > 256) throw AutomateException("Numero d'automate indefini");
    unsigned short int p = 128;
    int i = 7;
    while (i >= 0) {
        if (num >= p) {
            numeroBit.push_back('1');
            num -= p;
        }
        else { numeroBit.push_back('0'); }
        i--;
        p = p / 2;
    }
    return numeroBit;
}

Automate1D::Automate1D(unsigned short int num) : Automate(1), numero(num), numeroBit(NumToNumBit(num)) {}
Automate1D::Automate1D(const std::string& num) : Automate(1), numero(NumBitToNum(num)), numeroBit(num) {}

void Automate1D::appliquerTransition(const Etat1D& dep, Etat1D& dest) const {
    if (dep.getTaille() != dest.getTaille()) dest = dep;
    for (unsigned int i = 0; i < dep.getTaille(); i++) {
        unsigned short int conf=0;
        if (i > 0) conf+=dep.getCellule(i-1)*4;
        conf+=dep.getCellule(i)*2;
        if (i < dep.getTaille()-1) conf+=dep.getCellule(i+1);
        dest.setCellule(i, numeroBit[7-conf]-'0');
    }
}

std::ostream& operator<<(std::ostream& f, const Automate1D& A) {
    f << "Automate 1D " << A.getNumero() << " : " << A.getNumeroBit() << "\n";
    return f;
}



//!Automate JDV
AutomateJDV::AutomateJDV (unsigned int nbMinA, unsigned int nbMaxA, unsigned int nbMinD, unsigned int nbMaxD) {
    if (nbMinA>nbMaxA || nbMinD>nbMaxD) throw AutomateException("Arguments incorrects !");
    nbMinAlive = nbMinA;
    nbMaxAlive = nbMaxA;
    nbMinDead = nbMinD;
    nbMaxDead = nbMaxD;
} // Constructeur

unsigned int AutomateJDV::voisinage(const Etat2D& etat, unsigned int i, unsigned int j) const{
    // Fonction pour trouver le nombre de voisins
    unsigned int nbVoisins, nbL, nbC;
    nbVoisins = 0;
    nbL = etat.getHauteur();
    nbC = etat.getLargeur();

    if (etat.getCellule((i?i-1:nbL-1), (j?j-1:nbC-1)))  //Nord-Ouest
        nbVoisins++;
    if (etat.getCellule((i?i-1:nbL-1), j%nbC))  //Nord
        nbVoisins++;
    if (etat.getCellule((i?i-1:nbL-1), (j+1)%nbC))  //Nord-Est
        nbVoisins++;
    if (etat.getCellule(i%nbL, (j?j-1:nbC-1)))  //Ouest
        nbVoisins++;
    if (etat.getCellule(i%nbL, (j+1)%nbC))  //Est
        nbVoisins++;
    if (etat.getCellule((i+1)%nbL, (j?j-1:nbC-1)))  //Sud-Ouest
        nbVoisins++;
    if (etat.getCellule((i+1)%nbL, j%nbC))  //Sud
        nbVoisins++;
    if (etat.getCellule((i+1)%nbL, (j+1)%nbC))  //Sud-Est
        nbVoisins++;

    return nbVoisins;
}

void AutomateJDV::appliquerTransition(const Etat2D& etatDep, Etat2D& etatDest) const {
    if (etatDep.getLargeur()!=etatDest.getLargeur() || etatDep.getHauteur()!=etatDest.getHauteur())
        etatDest = etatDep;

    unsigned int voisinageJDV = 0;
    for (unsigned int i = 0; i < etatDep.getHauteur(); i++){
        for (unsigned int j = 0; j< etatDep.getLargeur(); j++){
            voisinageJDV = voisinage(etatDep,i,j);
            if (etatDep.getCellule(i,j)) {
                if ((voisinageJDV >= nbMinAlive) && (voisinageJDV <= nbMaxAlive))
                    etatDest.setCellule(i,j,1);
                else
                    etatDest.setCellule(i,j,0);
            } else {
                if ((voisinageJDV >= nbMinDead) && (voisinageJDV <= nbMaxDead))
                    etatDest.setCellule(i,j,1);
                else
                    etatDest.setCellule(i,j,0);
            }
        }
    }
}

std::ostream& operator<<(std::ostream& f, const AutomateJDV& A) {
    f << "Automate JDV <" << A.getNbMinAlive() << " " << A.getNbMaxAlive() << ">\n";
    return f;
}



//!Automate FDF
AutomateFDF::AutomateFDF (float p) {
    if (p<0 || p>1) throw AutomateException("Valeur de probabilit� incorrect !");
    probabilite = p;
} // Constructeur

unsigned int AutomateFDF::voisinage(const Etat2D& etat, unsigned int i, unsigned int j) const{
    // Fonction pour trouver le nombre de voisins vivants
    unsigned int nbVoisins, nbL, nbC;
    nbVoisins = 0;
    nbL = etat.getHauteur();
    nbC = etat.getLargeur();

    //Une cellule prend feu � l'�tat suivant s'ila un voisin en feu (etat=2)
    if (etat.getCellule((i)  %nbL, (j-1)%nbC) == 2)  //Ouest
        nbVoisins++;
    if (etat.getCellule((i)  %nbL, (j+1)%nbC) == 2)  //Est
        nbVoisins++;
    if (etat.getCellule((i-1)%nbL, (j)  %nbC) == 2)  //Nord
        nbVoisins++;
    if (etat.getCellule((i+1)%nbL, (j)  %nbC) == 2)  //Sud
        nbVoisins++;

    return nbVoisins;
}

void AutomateFDF::appliquerTransition(const Etat2D& etatDep, Etat2D& etatDest) const {
    etatDest = etatDep;
    int etat = 0;
    for (unsigned int i = 0; i < etatDep.getHauteur(); i++){
        for (unsigned int j = 0; j< etatDep.getLargeur(); j++){
            etat = etatDep.getCellule(i,j);
            if (etat == 1 && voisinage(etatDep,i,j) > 0)
                etatDest.setCellule(i,j,2);
            if (etat > 1)
                etatDest.setCellule(i,j,(++etat)%etatDest.getNbEtats());
        }
    }
}

ostream& operator<<(ostream& f, const AutomateFDF& A) {
    f << "Automate FDF :" << A.getProbabilite() << "\n";
    return f;
}


