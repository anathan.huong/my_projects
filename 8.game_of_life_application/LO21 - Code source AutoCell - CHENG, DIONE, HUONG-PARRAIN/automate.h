#ifndef AUTOMATE_H_INCLUDED
#define AUTOMATE_H_INCLUDED
#include "etat.h"

/*!
 * \file automates.h
 * \brief Documentation des classes automates
 * \author {CHENG Aurelie, DIONE Abdoul Aziz et HUONG-PARRAIN Anathan}
 * \date {Printemps 2018}

 * Voici notre projet AutoCell realise dans le cadre de l'UV LO21
 *

*/


/*!
  * \class Automate
  * \brief Objet mere
  *
  * Notre automate est caracterise par un attribut dimension,
  * herite par ses classes filles Automate1D et Automate2D.
 */
class Automate {
 private:
	 /*!
	 *\brief Dimension de l'automate
	 */
    unsigned int dimension;
 protected:
	 /*!
	 * \fn Automate(unsigned int dim)
	 * \brief Constructeur initialisant un Automate avec une dimension
	 *
	 */
    Automate(unsigned int dim) : dimension(dim) {}
 public:
	 /*!
	 * \fn unsigned int getDimension() const
	 * \brief Accesseur a l'attribut dimension
	 */
    unsigned int getDimension() const { return dimension; }
};


/*!
* \class Automate1D
* \brief Objet qui herite de Automate
*/
class Automate1D : public Automate {
 private:
	 /*!
	 *\brief Numero de la regle d'evolution
	 */
	unsigned short int numero;
	/*!
	*\brief Numero binaire de la regle d'evolution
	*/
	std::string numeroBit;
	/*!
	*\brief Constructeur initialisant un automate avec le numero de regle
	*/
 	Automate1D(unsigned short int num);
	/*!
	*\fn Automate1D(unsigned short int num)
	*\brief Constructeur initialisant un automate avec le numero de regle sous forme binaire
	*/
    Automate1D(const std::string& num);
	/*!
	*\fn Automate1D(const std::string& num)
	*\brief Constructeur de recopie
	*/
	Automate1D(const Automate1D& a) = default;
	/*!
	*\fn Automate1D(const Automate1D& a) = default
	*\brief Operateur d'affectation
	*/
	Automate1D& operator=(const Automate1D& a) = default;
	/*!
	*\fn ~Automate1D()
	*\brief Destructeur
	*/
    ~Automate1D() = default;
	/*!
	*\brief Les objets Automate sont geres par un module appele AutomateManager qui est responsable de leur creation (et destruction)
	*et de leur sauvegarde
	*/
	friend class AutomateManager;
 public:
	 /*!
	 * \fn unsigned short int getNumero() const
	 * \brief Accesseur a numero
	 * \return Retourne un entier correspondant au numero de la regle d'evolution
	 */
	unsigned short int getNumero() const { return numero; }
	/*!
	* \fn const std::string& getNumeroBit() const
	* \brief Accesseur a numeroBit
	* \return Retourne une chaine de taille 8 ne contenant que des '0' et des '1'representant la regle d�evolution
	*/
	const std::string& getNumeroBit() const { return numeroBit; }
	/*!
	* \fn void appliquerTransition(const Etat1D& dep, Etat1D& dest) const
	* \brief La methode permet d'appliquer la regle d'evolution sur un etat designe par dep pour obtenir un etat qui sera designe par dest.
	* \param dep pour l'etat de depart sur lequel on va appliquer la transition
	* \param dest l'etat qu'on obtient apres transition
	*/
	void appliquerTransition(const Etat1D& dep, Etat1D& dest) const;
};

/*!
* \fn short unsigned int NumBitToNum(const std::string& num)
* \brief La methode convertit le numero de la regle d'evolution sous forme binaire en numero
* \param num representant le numero binaire
* \return Retourne un entier correspondant au numero de la regle d'evolution
*/
short unsigned int NumBitToNum(const std::string& num);
/*!
* \fn std::string NumToNumBit(short unsigned int num)
* \brief La methode permet de passer d'un numero a numeroBit
* \param num representant le numero de type entier
* \return Retourne une chaine de taille 8 ne contenant que des '0' et des '1'representant la regle d�evolution
*/
std::string NumToNumBit(short unsigned int num);


/*!
* \class Automate2D
* \brief Objet qui herite de Automate
* Objet mere de AutomateJDV
*/
class Automate2D : public Automate {
 protected:
	 /*!
	 *\fn Automate2D()
	 *\brief Constructeur initialisant la dimension a 2
	 */
    Automate2D() : Automate(2) {}
 public:
	 /*!
	 *\fn virtual unsigned int voisinage (const Etat2D& etat, unsigned int i, unsigned int j) const = 0
	 *\brief Methode abstraite pour calculer le nombre de voisin d'une cellule
	 * \param etat Etat de la cellule dont on veut connaitre son voisinage
	 * \param i indice de la ligne de la cellule consideree
	 * \param j indice de la colonne de la cellule consideree
	 * \return Retourne le nombre de voisins
	 */
    virtual unsigned int voisinage (const Etat2D& etat, unsigned int i, unsigned int j) const = 0;
	/*!
	*\fn virtual void appliquerTransition(const Etat2D& etatDep, Etat2D& etatDest) const = 0
	* \brief La methode abstraite permet d'appliquer la regle d'evolution sur un etat designe par etatDep pour obtenir un etat qui sera designe par etatDest
	* \param etatDep pour l'etat de depart sur lequel on va appliquer la transition
	* \param etatDest l'etat qu'on obtient apres transition
	*/
    virtual void appliquerTransition(const Etat2D& dep, Etat2D& dest) const = 0;
};


/**
* \class AutomateJDV
* \brief Objet qui herite de Automate2D
* Jeu de la vie
*/
class AutomateJDV : public Automate2D {
 private:
	 /*!
	 *\brief Nombre minimum de voisins pour qu'une cellule morte devienne vivante
	 */
	unsigned int nbMinDead;
	/*!
	*\brief Nombre maximun de voisins pour qu'une cellule morte devienne vivante
	*/
	unsigned int nbMaxDead;
	/*!
	*\brief Nombre minimum de voisins pour qu'une cellule vivante reste vivante
	*/
	unsigned int nbMinAlive;
	/*!
	*\brief Nombre maximum de voisins pour qu'une cellule vivante reste vivante
	*/
	unsigned int nbMaxAlive;
	/*!
	*\fn AutomateJDV(unsigned int nbMinA, unsigned int nbMaxA, unsigned int nbMinD, unsigned int nbMaxD)
	*\brief Constructeur initialisant un AutomateJDV avec le nombre minimum/maximum de voisins pour la naissance d'une cellule
	* et avec le nombre minimum/maximum de voisins pour la survie d'une cellule
	*/
    AutomateJDV(unsigned int nbMinA, unsigned int nbMaxA, unsigned int nbMinD, unsigned int nbMaxD);
	/*!
	*\fn AutomateJDV(const AutomateJDV& a) = default
	*\brief Constructeur de recopie
	*/
	AutomateJDV(const AutomateJDV& a) = default;
	/*!
	*\fn AutomateJDV& operator=(const AutomateJDV& a) = default
	*\brief Operateur de recopie
	*/
	AutomateJDV& operator=(const AutomateJDV& a) = default;
	/*!
	*\brief Destructeur
	*/
    virtual ~AutomateJDV() = default;
	/*!
	*\brief Les objets AutomateJDV sont geres par un module appele AutomateManager qui est responsable de leur creation (et destruction)
	*et de leur sauvegarde
	*/
    friend class AutomateManager;
 public:
	 /*!
	 *\fn unsigned int getNbMinAlive() const
	 *\brief Accesseur a l'attribut nbMinAlive
	 */
    unsigned int getNbMinAlive() const { return nbMinAlive; }
	/*!
	*\fn unsigned int getNbMaxAlive() const
	*\brief Accesseur a l'attribut nbMaxAlive
	*/
    unsigned int getNbMaxAlive() const { return nbMaxAlive; }
	/*!
	*\fn unsigned int getNbMinDead() const
	*\brief Accesseur a l'attribut nbMinDead
	*/
	unsigned int getNbMinDead() const { return nbMinDead; }
	/*!
	*\fn unsigned int getNbMaxDead() const
	*\brief Accesseur a l'attribut nbMaxDead
	*/
	unsigned int getNbMaxDead() const { return nbMaxDead; }
	/*!
	*\fn virtual unsigned int voisinage (const Etat2D& etat, unsigned int i, unsigned int j) const
	*\brief Methode virtuelle (dont AutomateFDF va heriter) pour calculer le nombre de voisin d'une cellule
	* \param etat Etat de la cellule dont on veut connaitre son voisinage
	* \param i indice de la ligne de la cellule consideree
	* \param j indice de la colonne de la cellule consideree
	* \return Retourne le nombre de voisins d'une cellule
	*/
    virtual unsigned int voisinage (const Etat2D& etat, unsigned int i, unsigned int j) const;
	/*!
	*\fn virtual void appliquerTransition(const Etat2D& etatDep, Etat2D& etatDest) const
	* \brief La methode permet d'appliquer la regle d'evolution sur un etat designe par etatDep pour obtenir un etat qui sera designe par etatDest
	* \param etatDep pour l'etat de depart sur lequel on va appliquer la transition
	* \param etatDest l'etat qu'on obtient apres transition
	*/
    virtual void appliquerTransition(const Etat2D& etatDep, Etat2D& etatDest) const;
};


/**
* \class AutomateFDF
* \brief Objet qui herite de Automate2D
* Automate Feu de Foret : automate 2D avec 4 etats
*/
class AutomateFDF : public Automate2D {
 private:
	 /*!
	 *\brief Probabilite pour qu'une cellule soit un arbre ou vide
	 */
    float probabilite;
	/*!
	*\fn  AutomateFDF(float p = 0.5)
	*\brief Constructeur initialisant un AutomateFDF avec une probabilite 0.5 par defaut
	*/
    AutomateFDF(float p = 0.5);
	/*!
	*\fn AutomateFDF(const AutomateFDF& a) = default
	*\brief Constructeur de recopie
	*/
    AutomateFDF(const AutomateFDF& a) = default;
	/*!
	*\fn AutomateFDF& operator=(const AutomateFDF& a) = default
	*\brief Operateur d'affectation
	*/
	AutomateFDF& operator=(const AutomateFDF& a) = default;
	/*!
	*\fn virtual ~AutomateFDF() = default
	*\brief Destructeur
	*/
    virtual ~AutomateFDF() = default;
	/*!
	*\brief Les objets AutomateFDF sont geres par un module appele AutomateManager qui est responsable de leur creation (et destruction)
	*et de leur sauvegarde
	*/
    friend class AutomateManager;
 public:
	 /*!
	 *\fn float getProbabilite() const
	 *\brief Accesseur a l'attribut probabilite
	 */
    float getProbabilite() const { return probabilite; }
	/*!
	*\fn virtual unsigned int voisinage (const Etat2D& etat, unsigned int i, unsigned int j) const
	*\brief Methode virtuelle pour calculer le nombre de voisin d'une cellule
	* \param etat Etat de la cellule dont on veut connaitre son voisinage
	* \param i indice de la ligne de la cellule consideree
	* \param j indice de la colonne de la cellule consideree
	* \return Retourne le nombre de voisins
	*/
    virtual unsigned int voisinage (const Etat2D& etat, unsigned int i, unsigned int j) const;
	/*!
	* \fn void appliquerTransition(const Etat1D& dep, Etat1D& dest) const
	* \brief La methode permet d'appliquer la regle d'evolution sur un etat designe par etatDep pour obtenir un etat qui sera designe par etatDest
	* \param etatDep pour l'etat de depart sur lequel on va appliquer la transition
	* \param etatDest l'etat qu'on obtient apres transition
	*/
    virtual void appliquerTransition(const Etat2D& etatDep, Etat2D& etatDest) const;
};
/*!
* \fn std::ostream& operator<<(std::ostream& f, const Automate1D& t)
* \brief Affiche un objet Automate1D sur un flux ostream
*/
std::ostream& operator<<(std::ostream& f, const Automate1D& t);
/*!
* \fn std::ostream& operator<<(std::ostream& f, const AutomateJDV& t)
* \brief Affiche un objet AutomateJDV sur un flux ostream
*/
std::ostream& operator<<(std::ostream& f, const AutomateJDV& t);
/*!
* \fn std::ostream& operator<<(std::ostream& f, const AutomateFDF& t)
* \brief Affiche un objet AutomateFDF sur un flux ostream
*/
std::ostream& operator<<(std::ostream& f, const AutomateFDF& t);


#endif // AUTOMATE_H_INCLUDED
