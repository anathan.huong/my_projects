#include "automateManager.h"

AutomateManager::AutomateManager () {
	for (unsigned int i = 0; i < 256; i++) automates1D[i] = nullptr;
	automateJDV = nullptr;
	automateFDF = nullptr;
}

AutomateManager::~AutomateManager() {
	for (unsigned int i = 0; i < 256; i++) delete automates1D[i];
    delete automateFDF;
    delete automateJDV;
}

AutomateManager::Handler AutomateManager::handler = Handler();

AutomateManager& AutomateManager::getAutomateManager() {
	if (!handler.instance) handler.instance = new AutomateManager;
	return *handler.instance;
}

void AutomateManager::libererAutomateManager() {
	delete handler.instance;
	handler.instance = nullptr;
}

const Automate1D& AutomateManager::getAutomate1D(unsigned short int num) {
	if (!automates1D[num]) automates1D[num] = new Automate1D(num);
	return *automates1D[num];
}

const Automate1D& AutomateManager::getAutomate1D(const std::string& numBit) {
	return getAutomate1D(NumBitToNum(numBit));
}

const AutomateJDV& AutomateManager::getAutomateJDV (unsigned int nbMinA, unsigned int nbMaxA, unsigned int nbMinD, unsigned int nbMaxD) {
    delete automateJDV;
    automateJDV = new AutomateJDV(nbMinA,nbMaxA,nbMinD,nbMaxD);
    return *automateJDV;
}

const AutomateFDF& AutomateManager::getAutomateFDF (float p) {
    delete automateFDF;
    automateFDF = new AutomateFDF(p);
    return *automateFDF;
}


// G�n�rateur d'etats pour les automates deux dimensions

void Generateur::genererEtatAleatoire(Etat2D& E){
    srand(time(nullptr));
    int h = E.getHauteur(), l = E.getLargeur();
    for(int i=0 ; i < h ; i++) {
        for(int j=0 ; j < l ; j++)
            E.setCellule(i,j, rand()%2);
    }
}

void Generateur::genererEtatSymetrique(Etat2D& E){
    srand(time(nullptr));
    int valeur = 0, h = E.getHauteur(), l = E.getLargeur() ;
    for(int i = 0 ; i < h ; i++) {
        for(int j = 0 ; j <= l/2+1 ; j++) {
            valeur = rand()%2;
            E.setCellule(i,j, valeur);
            E.setCellule(i,l-j-1,valeur);
        }
    }
}

void Generateur::genererEtatFDF(Etat2D& E, float p) {
    srand(time(nullptr));
    int valeur = 0, h = E.getHauteur(), l = E.getLargeur() , prob = p*100;

    for(int i=0; i < h ; i++) {
        for(int j=0 ; j < l ; j++) {
            valeur = rand()%100;
            if (valeur < prob)
                E.setCellule(i,j,1);
            else
                E.setCellule(i,j,0);
        }
    }

    // Mettre une cellule au hasard en feu � l'initialisation
    int col = rand()%l , lig = rand()%h;
    E.setCellule(lig,col,2);

}

void Generateur::genererEtatAleatoire(Etat1D& E) {
    srand(time(nullptr));
    int l = E.getTaille();
    for(int i=0 ; i < l ; i++)
        E.setCellule(i, rand()%2);
}

void Generateur::genererEtatSymetrique(Etat1D& E) {
    srand(time(nullptr));
    int valeur = 0, l = E.getTaille() ;
    for(int i = 0 ; i < l/2+1 ; i++) {
            valeur = rand()%2;
            E.setCellule(i, valeur);
            E.setCellule(l-i-1,valeur);
    }
}
