#ifndef AUTOMATEMANAGER_H_INCLUDED
#define AUTOMATEMANAGER_H_INCLUDED


#include <random>
#include <time.h>
#include "simulateur.h"
/*!
* \file automateManager.h
* \brief Documentation de la classe AutomateManager et de Generateur
* \author {CHENG Aurelie, DIONE Abdoul Aziz, HUONG-PARRAIN Anathan }
* \date {Printemps 2018}
*/




/*!
* \class AutomateManager
* \brief Classe qui est responsable de la cr�ation (et destruction) et de la sauvegarde de l'ensemble des objets Automate.
* Le Design Pattern Singleton a ete applique a cette classe car AutomateManager est unique dans le systeme
*/
class AutomateManager {
private:
	/*!
	*\brief Tableau pouvant contenir jusqu'a 256 objets Automate1D
	*/
	Automate1D* automates1D[256];
	/*!
	*\brief Tableau allou� dynamiquement contenant des objets AutomateJDV
	*/
	AutomateJDV* automateJDV;
	/*!
	*\brief Tableau allou� dynamiquement contenant des objets AutomateJDV
	*/
	AutomateFDF* automateFDF;
	/*!
	*\brief Constructeur
	*/
	AutomateManager();
	/*!
	*\brief Destructeur
	*/
	~AutomateManager();
	/*!
	*\brief Constructeur de recopie
	*/
	AutomateManager(const AutomateManager& a) = delete;
	/*!
	*\brief Operateur d'affectation
	*/
	AutomateManager& operator=(const AutomateManager& a) = delete;
	/*!
	* \struct Handler
	* \brief Le Design Pattern Singleton a ete applique a cette classe car AutomateManager est unique dans le systeme
	* Cette structure permet de creer une seule instance de AutomateManager
	*/
	struct Handler {
		AutomateManager* instance;
		Handler() :instance(nullptr) {}
		~Handler() { delete instance; }
	};
	/*!
	* \struct Handler
	* \brief Static pour que la variable n'existe qu'en un seul exemplaire
	*/
	static Handler handler;
public:
	/*!
	*\brief La methode permet d'acceder (eventuellement de creer) un Automate1D
	*\param num Numero entier de la regle d'evolution
	*/
	const Automate1D& getAutomate1D(unsigned short int num);
	/*!
	*\brief La methode permet d'acceder (eventuellement de creer) un Automate1D
	*\param num Numero binaire de la regle d'evolution
	*/
	const Automate1D& getAutomate1D(const std::string& num);
	/*!
	*\brief La methode permet d'acceder (eventuellement de creer) un AutomateJDV
	* dont les nombres minimum/maximum de voisins pour la naissance et pour la survie d'une cellule sont transmis en argument
	*/
	const AutomateJDV& getAutomateJDV(unsigned int nbMinA, unsigned int nbMaxA, unsigned int nbMinD, unsigned int nbMaxD);
	/*!
	*\brief La methode permet d'acceder (eventuellement de creer) un AutomateFDF dont la probabilite est transmis en argument
	*/
	const AutomateFDF& getAutomateFDF(float p = 0.5);

	/*!
	*\brief La methode permet d'acceder a un AutomateManager et eventuellement de le creer s'il n'existe pas
	*/
	static AutomateManager& getAutomateManager();
	/*!
	*\brief La methode permet de detruire
	*/
	static void libererAutomateManager();
};

/*!
* \class Generateur
* \brief Classe qui remplit les cellules de l'etat de depart selon differents modes possibles (aleatoire, symetrique)
*/
class Generateur {
public:
	void genererEtatAleatoire(Etat2D& E);
	void genererEtatSymetrique(Etat2D& E);
	void genererEtatAleatoire(Etat1D& E);
	void genererEtatSymetrique(Etat1D& E);
	void genererEtatFDF(Etat2D& E, const float p);
};


#endif // AUTOMATEMANAGER_H_INCLUDED
