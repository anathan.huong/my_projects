#include "etat.h"
#include "automate.h"

using namespace std;

//impl�mentation des constructeurs
Etat1D::Etat1D(unsigned int t, unsigned int nbEtats) : Etat(nbEtats), taille(t) {
    valeur = new int[taille];      //tableau allou� dynamiquement
	for (unsigned int i = 0; i < taille; i++)//il faut initialis� toutes les valeurs par FALSE
		valeur[i] = 0;
}

Etat1D::Etat1D(const Etat1D& e) : Etat(e.nbEtatsParCellule) {
	taille = e.taille;
	valeur = new int[taille];
	for (unsigned int i = 0; i < taille; i++)
		valeur[i] = e.valeur[i];
}

Etat1D& Etat1D::operator = (const Etat1D& e) {
    if (this == &e) return *this; //on v�rifie qu'on agit pas sur la mm adresse
    if (taille != e.taille) {
        //on desalloue
        delete[] valeur;
        nbEtatsParCellule = e.nbEtatsParCellule;
        taille = e.taille;
        valeur = new int[taille];
    }
    for(unsigned int i = 0; i < taille; i++)
        valeur[i] = e.valeur[i];
    return *this;
}

//destructeur
Etat1D::~Etat1D() {
	delete[] valeur;//on a un tableau
}

//m�thodes
int Etat1D::getCellule(unsigned int numero) const {
    if (numero >= taille) throw AutomateException("Out of range 1D");
    return valeur[numero];
}

void Etat1D::setCellule(unsigned int numero, unsigned int val) {
    if (val>=nbEtatsParCellule) throw AutomateException("Etat non defini");
    if (numero >= taille) throw AutomateException("Out of range 1D");
	valeur[numero] = val;
}

// ETAT 2D
Etat2D::Etat2D(unsigned int lar, unsigned int haut, unsigned int nbEtat)
    : Etat(nbEtat), largeur(lar), hauteur(haut) {
    cellule = new int*[hauteur];
    for(unsigned int i=0 ; i<hauteur; i++) {
        cellule[i] = new int[largeur];
        for(unsigned int j=0 ; j<largeur ; j++)
            cellule[i][j] = 0;
    }
}

Etat2D::Etat2D(const Etat2D& dep) : Etat(dep.nbEtatsParCellule), largeur(dep.largeur), hauteur(dep.hauteur) {
    nbEtatsParCellule = dep.nbEtatsParCellule;
    cellule = new int*[hauteur];
    for(unsigned int i=0 ; i<hauteur ; i++) {
        cellule[i] = new int[largeur];
        for(unsigned int j=0 ; j<largeur ; j++)
            cellule[i][j] = dep.cellule[i][j];
    }
}

Etat2D& Etat2D::operator=(const Etat2D& dep) {
    if (this == &dep) return *this;   //ne pas se recopier soi-m�me

    //Suppression de l'ancien tableau puis cr�ation d'un nouveau
    for(unsigned int i=0 ; i<hauteur ; i++)
        delete cellule[i];
    delete[] cellule;

    largeur = dep.largeur; hauteur = dep.hauteur;
    nbEtatsParCellule = dep.nbEtatsParCellule;
    cellule = new int*[hauteur];
    for(unsigned int i=0 ; i<hauteur ; i++) {
        cellule[i] = new int[largeur];
        for(unsigned int j=0 ; j<largeur ; j++)
            cellule[i][j] = dep.cellule[i][j];
    }
    return *this;
}

Etat2D::~Etat2D() {
    for(unsigned int i=0 ; i<hauteur ; i++)
        delete cellule[i];
    delete[] cellule;
}

int Etat2D::getCellule(unsigned int lig, unsigned int col) const {
    if (lig>=hauteur || col>=largeur) throw AutomateException("Out of range 1D");
    return cellule[lig][col];
}

void Etat2D::setCellule(unsigned int lig, unsigned int col, unsigned int val) {
    if (val>=nbEtatsParCellule) throw AutomateException("Etat non defini");
    if (lig>=hauteur|| col>=largeur) throw AutomateException("Out of range 1D");
    cellule[lig][col] = val;
}

ostream& operator<<(ostream& f, const Etat1D& e) {
	for (unsigned int i = 0; i < e.getTaille(); i++)
        f << e.getCellule(i);
    return f;
}

ostream& operator<< (ostream& f, const Etat2D& e) {
    unsigned int larg = e.getLargeur();
    unsigned int haut = e.getHauteur();
    for (unsigned int i=0 ; i<haut ; i++) {
        for (unsigned int j=0 ; j<larg ; j++)
            f << e.getCellule(i,j);
        f << endl;
    }
    return f;
}
