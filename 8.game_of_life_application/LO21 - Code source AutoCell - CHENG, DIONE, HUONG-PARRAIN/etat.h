#ifndef ETAT_H_INCLUDED
#define ETAT_H_INCLUDED

#include <iostream>
#include <string>

/*!
* \file etat.h
* \brief Documentation des classes �tats
* \author {CHENG Aurelie, DIONE Abdoul Aziz, HUONG-PARRAIN Anathan }
* \date {Printemps 2018}
*/




/*!
* \class AutomateException
* \brief Classe permettant de lever des exceptions liees a un Automate.
* Lorsqu une exception est levee, on recupere une chaine de caracteres contenant l'information.
*/
class AutomateException {
private:
	/*!
	*\brief Chaine de caractere renvoyant l'information d'erreur
	*/
	std::string info;
public:

	/*!
	*  \brief Constructeur
	*
	*  Constructeur de la classe AutomateException
	*
	*  \param message On construit notre automate avec la chaine de caractere recuperee
	*/
	AutomateException(const std::string& message) :info(message) {}
	/*!
	*  \brief Retourne la chaine de caracetere correspondant a l'exception relevee et contenu dans l'attribut info de l'AutomateException
	*/
	std::string getInfo() const { return info; }
};

/*!
* \class Etat
*  \brief Classe mere Etat dont les classes filles sont Etat1D et Etat2D
* Le code a ete factorisee de telle sorte qu'elle contienne le nombre d'etats possibles qu'une cellule peut prendre
*/
class Etat {
protected:
	/*!
	*\brief Nombre d'etat par cellule
	*/
	unsigned int nbEtatsParCellule;
public:
	/*!
	*  \fn Etat(unsigned int nbEtats=2) : nbEtatsParCellule(nbEtats)
	*  \brief Constructeur de la classe Etat
	*  \param nbEtats On construit un etat avec un attribut nbEtats. La valeur par defaut est 2 car il s'agit du cas le plus frequent.
	*  \return Retourne un etat
	*/
	Etat(unsigned int nbEtats = 2) : nbEtatsParCellule(nbEtats) {}
	/*!
	*  \brief Accesseur du nombre d'etats
	*
	*  \return Retourne un attribut protege de la classe Etat
	*/
	unsigned int getNbEtats() const { return nbEtatsParCellule; }
};

/*!
* \class Etat1D
*  \brief Classe fille de Etat.
* Correspond a un etat pour les automates une dimension
*/
class Etat1D : public Etat {
private:
	/*!
	*\brief Taille representant le nombre de cellule impliquees dans la grille
	*/
	unsigned int taille;
	/*!
	*\brief Valeur pointe sur un tableau alloue dynamiquement de taille
	* Taille et contenant des valeurs de type int.
	* Chaque valeur de ce tableau correspond a l'etat d'une cellule
	*/
	int* valeur;
public:
	/*!
	*  \fn explicit Etat1D() : Etat(2), taille(0), valeur(nullptr) {}
	*  \brief Constructeur par defaut
	*/
	explicit Etat1D() : Etat(2), taille(0), valeur(nullptr) {}  //initialis� � 0 par d�faut
	/*!
	*  \brief Constructeur
	*/
	explicit Etat1D(unsigned int t, unsigned int nbEtats = 2);
	/*!
	*  \brief Constructeur de recopie
	*/
	explicit Etat1D(const Etat1D& e);
	/*!
	*  \brief Operateur d'affectation
	*/
	Etat1D& operator = (const Etat1D& e);
	/*!
	*  \brief Detructeur
	*/
	~Etat1D();
	/*!
	*  \brief Accesseur a l'attribut taille
	*/
	unsigned int getTaille() const { return taille; }  //par d�faut elle est inline dans le .h
	/*!
	*  \brief Accesseur pour connaitre l'etat d'une cellule
	*/
	int getCellule(unsigned int numero) const;
	/*!
	*  \brief La methode permet de modifier l'etat de la cellule numero avec la valeur val
	*/
	void setCellule(unsigned int numero, unsigned int val);
};


/*!
* \class Etat2D
*  \brief Classe fille de Etat.
* Correspond a un etat pour les automates deux dimensions
*/
class Etat2D : public Etat {
private:
	/*!
	*\brief Nombre de cellule sur une ligne
	*/
	unsigned int largeur;
	/*!
	*\brief Nombre de cellule sur une colonne
	*/
	unsigned int hauteur;
	/*!
	*\brief cellule pointe sur un tableau alloue dynamiquement de taille
	* largeur x hauteur et contenant des valeurs de type int.
	* Chaque valeur de ce tableau correspond a l'etat d'une cellule
	*/
	int** cellule;
public:
	/*!
	*\brief Constructeur par defaut
	*/
	Etat2D() : Etat(2), largeur(0), hauteur(0), cellule(nullptr) {}
	/*!
	*\brief Constructeur
	*/
	explicit Etat2D(unsigned int lar, unsigned int haut, unsigned int nbEtats = 2);
	/*!
	*\brief Constructeur de recopie
	*/
	explicit Etat2D(const Etat2D& dep);
	/*!
	*\brief Operateur d'affectation
	*/
	Etat2D& operator=(const Etat2D& dep);
	/*!
	*\brief Destructeur
	*/
	~Etat2D();
	/*!
	*\brief Accesseur de largeur
	*/
	unsigned int getLargeur() const { return largeur; }
	/*!
	*\brief Accesseur d'hauteur
	*/
	unsigned int getHauteur() const { return hauteur; }
	/*!
	*\brief Accesseur pour connaitre l'etat d'une cellule
	\param lig indice de la ligne de la cellule consideree
	\param col indice de la colonne de la cellule consideree
	*/
	int getCellule(unsigned int lig, unsigned int col) const;
	/*!
	*\brief La methode modifie l'etat d'une cellule avec la valeur val
	*/
	void setCellule(unsigned int lig, unsigned int col, unsigned int val);

};

/*!
* \brief Affiche un objet Etat1D sur un flux ostream
*/
std::ostream& operator<<(std::ostream& f, const Etat1D& e);
/*!
* \brief Affiche un objet Etat2D sur un flux ostream
*/
std::ostream& operator<<(std::ostream& f, const Etat2D& e);

#endif // ETAT_H_INCLUDED
