#include "mainwindow.h"


MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent) {

    setWindowTitle("Fenêtre principale... Le monde des automates !");

    zoneCentrale = new QMdiArea;

    // Menu déroulant
    QMenu *menuAutomate = menuBar()->addMenu("&Automates");


    // Interface Automate1D

    QAction *ActionLancementAutomate1D = new QAction("&Lancement Automate 1D", this);

    menuAutomate->addAction(ActionLancementAutomate1D);

    connect(ActionLancementAutomate1D, SIGNAL(triggered()), this, SLOT(LancementAutomate1D()));



    QAction *ActionLancementAutomateJDV = new QAction("&Lancement Automate JDV", this);

    menuAutomate->addAction(ActionLancementAutomateJDV);

    connect(ActionLancementAutomateJDV, SIGNAL(triggered()), this, SLOT(LancementAutomateJDV()));



    // Interface Automate FDF

    QAction *ActionLancementAutomateFDF = new QAction("&Lancement Automate FDF", this);

    menuAutomate->addAction(ActionLancementAutomateFDF);

    connect(ActionLancementAutomateFDF, SIGNAL(triggered()), this, SLOT(LancementAutomateFDF()));



    // Présentation fenêtre principale
    QFont police("calibri");
    police.setPointSize (22);

    bienvenue =new QLabel("Nous vous souhaitons la bienvenue sur notre application ! ");
    bienvenue->setFont(police);

    couche = new QVBoxLayout;

    couche->addWidget(bienvenue);

    zoneCentrale->setLayout(couche);

    setCentralWidget(zoneCentrale);

    this->showMaximized();

    sousFenetre1 = new QMdiSubWindow;
    sousFenetre2 = new QMdiSubWindow;
    sousFenetre3 = new QMdiSubWindow;

}



void MainWindow::LancementAutomate1D() {

   bienvenue->setVisible(false);
   FenetreAutomate1D = new AutoCell1D;
   sousFenetre1->setWidget(FenetreAutomate1D);
   zoneCentrale->addSubWindow(sousFenetre1);
   sousFenetre1->show();
   sousFenetre1->showMaximized();

}



void MainWindow::LancementAutomateJDV() {

    bienvenue->setVisible(false);
   FenetreAutomateJDV = new AutoCellJDV;
   sousFenetre2->setWidget(FenetreAutomateJDV);
   zoneCentrale->addSubWindow(sousFenetre2);
   sousFenetre2->show();
   sousFenetre2->showMaximized();

}



void MainWindow::LancementAutomateFDF() {

   bienvenue->setVisible(false);
   FenetreAutomateFDF = new AutoCellFDF;
   sousFenetre3->setWidget(FenetreAutomateFDF);
   zoneCentrale->addSubWindow(sousFenetre3);
   sousFenetre3->show();
   sousFenetre3->showMaximized();

}

