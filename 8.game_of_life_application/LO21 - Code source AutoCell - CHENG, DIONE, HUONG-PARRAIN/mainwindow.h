#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include <QMainWindow>
#include <QMdiSubWindow>
#include <QMdiArea>
#include <QMenuBar>
#include <QFormLayout>
#include <QTextEdit>

#include "autocellFDF.h"
/*!
* \file mainwindow.h
* \brief Documentation de notre interface pour la fenetre principale qui gere les sous-fenetres correspondant aux differents automates 
* \author {CHENG Aurelie, DIONE Abdoul Aziz, HUONG-PARRAIN Anathan }
* \date {Printemps 2018}
*/




/*!
* \class MainWindow
* \brief Classe qui cree l'interface de la fenetre principale qui gere les sous-fenetres correspondant aux differents automates 
*/


class MainWindow : public QMainWindow {
    Q_OBJECT

    //QPushButton* BoutonAutomate2D;
    QMdiArea* zoneCentrale;

    QLabel* bienvenue;

    QVBoxLayout* couche;


    AutoCell1D* FenetreAutomate1D;

    QMdiSubWindow* sousFenetre1;

    AutoCellJDV* FenetreAutomateJDV;

    QMdiSubWindow* sousFenetre2;

    AutoCellFDF* FenetreAutomateFDF;

    QMdiSubWindow* sousFenetre3;


public:
    explicit MainWindow(QWidget *parent = 0);

private slots :
	/*!
	*\brief Affiche l'interface de l'automate une dimension dans une sous fenetre issue de la fenetre principale
	*/
    void LancementAutomate1D();
	/*!
	*\brief Affiche l'interface de l'automate deux dimensions Jeu de la Vie dans une sous fenetre issue de la fenetre principale
	*/
    void LancementAutomateJDV();
	/*!
	*\brief Affiche l'interface de l'automate deux dimensions Feu de Foret dans une sous fenetre issue de la fenetre principale
	*/
    void LancementAutomateFDF();

};

#endif // MAINWINDOW_H
