QT += widgets
QT += xml

QMAKE_CXXFLAGS = -std=c++11
QMAKE_LFLAGS = -std=c++11

HEADERS += \
    mainwindow.h \
    autocell1D.h \
    autocellJDV.h \
    autocellFDF.h \
    automate.h \
    automateManager.h \
    etat.h \
    simulateur.h

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    autocell1D.cpp \
    autocellJDV.cpp \
    autocellFDF.cpp \
    automate.cpp \
    automateManager.cpp \
    etat.cpp \
    simulateur.cpp
