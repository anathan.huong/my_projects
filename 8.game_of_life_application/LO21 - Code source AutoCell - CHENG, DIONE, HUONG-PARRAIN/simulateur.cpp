#include "simulateur.h"

Simulateur1D::Simulateur1D(const Automate1D& a, unsigned int buffer):
    automate1D(a), nbMaxEtats(buffer) {
    etats1D = new Etat1D*[nbMaxEtats];
    for (unsigned int i = 0; i < nbMaxEtats; i++)
        etats1D[i] = nullptr;
}

Simulateur1D::Simulateur1D(const Automate1D& a, const Etat1D& dep, unsigned int buffer):
	automate1D(a), nbMaxEtats(buffer) {
	etats1D = new Etat1D*[nbMaxEtats];
	for (unsigned int i = 0; i < nbMaxEtats; i++)
        etats1D[i] = nullptr;
	setEtatDepart(dep);
}

void Simulateur1D::setEtatDepart(const Etat1D& e) {
	depart = &e;
	reset();
}

Simulateur1D::~Simulateur1D() {
	for (unsigned int i = 0; i < nbMaxEtats; i++)
        delete etats1D[i];
	delete[] etats1D;
}

void Simulateur1D::build(unsigned int cellule) {
	if (cellule >= nbMaxEtats) throw AutomateException("erreur taille buffer");
	if (etats1D[cellule] == nullptr)
        etats1D[cellule] = new Etat1D;
}

void Simulateur1D::next() {
	if (depart == nullptr) throw AutomateException("etat depart indefini");
	rang++;
	build(rang%nbMaxEtats);
	automate1D.appliquerTransition(*etats1D[(rang - 1) % nbMaxEtats], *etats1D[rang%nbMaxEtats]);
}

void Simulateur1D::run(unsigned int nb_steps) {
	for (unsigned int i = 0; i < nb_steps; i++)
        next();
}

void Simulateur1D::reset() {
	if (depart==nullptr) throw AutomateException("etat depart indefini");
	build(0); *etats1D[0] = *depart;
	rang = 0;
}


// Simulateur2D
Simulateur2D::Simulateur2D(const Automate2D& a, unsigned int buffer):
    automate2D(a), nbMaxEtats(buffer) {
    etats2D = new Etat2D*[nbMaxEtats];
    for (unsigned int i = 0; i < nbMaxEtats; i++)
        etats2D[i] = nullptr;
}

Simulateur2D::Simulateur2D(const Automate2D& a, const Etat2D& dep, unsigned int buffer):
	automate2D(a), nbMaxEtats(buffer) {
	etats2D = new Etat2D*[nbMaxEtats];
	for (unsigned int i = 0; i < nbMaxEtats; i++)
        etats2D[i] = nullptr;
	setEtatDepart(dep);
}

void Simulateur2D::setEtatDepart(const Etat2D& e) {
	depart = &e;
	reset();
}

Simulateur2D::~Simulateur2D() {
	for (unsigned int i = 0; i < nbMaxEtats; i++)
        delete etats2D[i];
	delete[] etats2D;
}

void Simulateur2D::build(unsigned int cellule) {
	if (cellule >= nbMaxEtats) throw AutomateException("erreur taille buffer");
	if (etats2D[cellule] == nullptr)
        etats2D[cellule] = new Etat2D;
}

void Simulateur2D::next() {
	if (depart == nullptr) throw AutomateException("etat depart indefini");
	rang++;
	build(rang%nbMaxEtats);
	automate2D.appliquerTransition(*etats2D[(rang - 1) % nbMaxEtats], *etats2D[rang%nbMaxEtats]);
}

void Simulateur2D::run(unsigned int nb_steps) {
	for (unsigned int i = 0; i < nb_steps; i++)
        next();
}

void Simulateur2D::reset() {
	if (depart==nullptr) throw AutomateException("etat depart indefini");
	build(0); *etats2D[0] = *depart;
	rang = 0;
}
