#ifndef SIMULATEUR_H_INCLUDED
#define SIMULATEUR_H_INCLUDED
#include "automate.h"

/*!
* \file simulateur.h
* \brief Documentation des simulateurs 
* \author {CHENG Aurelie, DIONE Abdoul Aziz et HUONG-PARRAIN Anathan}
* \date {Printemps 2018}
*/


/*!
* \class Simulateur1D
* \brief Classe qui permet de faire le lien entre la classe Etat1D et un Automate1D. 
* Il permettra d�appliquer un objet Automate1D sur plusieurs g�n�rations � partir d�un objet Etat1D repr�sentant une grille de d�part
*/
class Simulateur1D {
	/*!
	*\brief Objet Automate1D
	*/
	const Automate1D& automate1D;
	/*!
	*\brief Tableau alloue dynamiquement contenant des valeurs de type
	* Etat1D*. Un pointeur du tableau contient eventuellement l'adresse d'un etat alloue dynamiquement
	*/
	Etat1D** etats1D = nullptr;
	/*!
	*\brief Pointe sur un eventuel etat de depart (s'il a ete donne)
	*/
	const Etat1D* depart = nullptr;
	/*!
	*\brief Taille du tableau etats1D
	*/
	unsigned int nbMaxEtats;
	/*!
	*\brief Indice de l'etat en cours 
	*/
	unsigned int rang = 0;
	/*!
	*\brief La methode alloue un nouvel objet si Etat1D jamais il n'existe pas encore (pour un premier parcours du tableau)
	*/
	void build(unsigned int c);
	/*!
	*\brief Constructeur de recopie 
	*/
	Simulateur1D(const Simulateur1D& s) = delete;
	/*!
	*\brief Operateur d'affectation 
	*/
	Simulateur1D& operator=(const Simulateur1D& s) = delete;
public:
	/*!
	*\brief Constructeur initialisant un objet Simulateur1D en utilisant
	* un buffer de taille 10 (par defaut)
	*/
	Simulateur1D(const Automate1D& a, unsigned int buffer = 10);
	/*!
	*\brief Constructeur fournit l'automate, la taille du buffer et de preciser
	* l'etat de depart 
	*/
	Simulateur1D(const Automate1D& a, const Etat1D& dep, unsigned int buffer = 10);
	/*!
	*\brief Modifie l'etat de depart 
	*/
	void setEtatDepart(const Etat1D& e);
	/*!
	*\brief Genere les n prochains etats
	*/
	void run(unsigned int nbSteps); 
	/*!
	*\brief Genere le prochain etat
	*/
	void next(); 
	/*!
	*\brief Revient a l'etat de depart 
	*/
	void reset();
	/*!
	*\brief Renvoie le dernier etat qui a ete genere
	*/
	const Etat1D& dernier() const { return *etats1D[rang%nbMaxEtats]; }
	/*!
	*\brief Renvoie le rang du dernier etat cree
	*/
	unsigned int getRangDernier() const { return rang; }
	/*!
	*\brief Destructeur
	*/
	~Simulateur1D();

	/*!
	* \class iterator
	* \brief Classe qui permet de parcourir les derniers etats generes 
	* tout en respectant le principe d'encapsulation 
	*/
	// Iterateur 1D
	class iterator {
		friend class Simulateur1D;
		Simulateur1D* sim = nullptr;
		int i = 0;
		iterator(Simulateur1D* s) :sim(s), i(s->rang) {}
		iterator(Simulateur1D* s, int dep) :sim(s), i(dep) {}
	public:
		iterator() {}
		iterator& operator++() {
			i--;
			if (i == -1 && sim->rang >= sim->nbMaxEtats) i = sim->nbMaxEtats - 1;
			return *this;
		}
		Etat1D& operator*() const { return *sim->etats1D[i%sim->nbMaxEtats]; }
		bool operator!=(iterator it) const { return sim != it.sim || i != it.i; }
	};

	iterator begin() { return iterator(this); }
	iterator end() { if (rang < nbMaxEtats) return iterator(this, -1); else return iterator(this, rang - nbMaxEtats); }
	/*!
	* \class const_iterator
	* \brief Classe qui permet de parcourir les objets Etat1D (constants)
	* tout en respectant le principe d'encapsulation 
	*/
	class const_iterator {
		friend class Simulateur1D;
		const Simulateur1D* sim = nullptr;
		int i = 0;
		const_iterator(const Simulateur1D* s) :sim(s), i(s->rang) {}
		const_iterator(const Simulateur1D* s, int dep) :sim(s), i(dep) {}
	public:
		const_iterator() {}
		const_iterator& operator++() {
			i--;
			if (i == -1 && sim->rang >= sim->nbMaxEtats) i = sim->nbMaxEtats - 1;
			return *this;
		}
		const Etat1D& operator*() const { return *sim->etats1D[i%sim->nbMaxEtats]; }
		bool operator!=(const_iterator it) const { return sim != it.sim || i != it.i; }
	};

	const_iterator begin() const { return const_iterator(this); }
	const_iterator end() const { if (rang < nbMaxEtats) return const_iterator(this, -1); else return const_iterator(this, rang - nbMaxEtats); }
	const_iterator cbegin() const { return const_iterator(this); }
	const_iterator cend() const { if (rang < nbMaxEtats) return const_iterator(this, -1); else return const_iterator(this, rang - nbMaxEtats); }

};

/*!
* \class Simulateur2D
* \brief Classe qui permet de faire le lien entre la classe Etat2D et un Automate2D.
* Il permettra d�appliquer un objet Automate2D sur plusieurs g�n�rations � partir d�un objet Etat2D repr�sentant une grille de d�part
*/
class Simulateur2D {
	/*!
	*\brief Objet Automate2D
	*/
	const Automate2D& automate2D;
	/*!
	*\brief Tableau alloue dynamiquement contenant des valeurs de type
	* Etat2D*. Un pointeur du tableau contient eventuellement l'adresse d'un etat alloue dynamiquement
	*/
	Etat2D** etats2D = nullptr;
	/*!
	*\brief Pointe sur un eventuel etat de depart (s'il a ete donne)
	*/
	const Etat2D* depart = nullptr;
	/*!
	*\brief Taille du tableau etats2D
	*/
	unsigned int nbMaxEtats;
	/*!
	*\brief Indice de l'etat en cours
	*/
	unsigned int rang = 0;
	/*!
	*\brief La methode alloue un nouvel objet Etat2D si jamais il n'existe pas encore (pour un premier parcours du tableau)
	*/
	void build(unsigned int c);
	/*!
	*\brief Constructeur de recopie 
	*/
	Simulateur2D(const Simulateur2D& s) = delete;
	/*!
	*\brief Operateur d'affectation 
	*/
	Simulateur2D& operator=(const Simulateur2D& s) = delete;
public:
	/*!
	*\brief Constructeur initialisant un objet Simulateur2D en utilisant
	* un buffer de taille 10 (par defaut)
	*/
	Simulateur2D(const Automate2D& a, unsigned int buffer = 10);
	/*!
	*\brief Constructeur fournit l'automate, la taille du buffer et de preciser
	* l'etat de depart
	*/
	Simulateur2D(const Automate2D& a, const Etat2D& dep, unsigned int buffer = 10);
	/*!
	*\brief Modifie l'etat de depart
	*/
	void setEtatDepart(const Etat2D& e);
	/*!
	*\brief Genere les n prochains etats
	*/
	void run(unsigned int nbSteps); 
	/*!
	*\brief Genere le prochain etat
	*/
	void next(); 
	/*!
	*\brief Revient a l'etat de depart 
	*/
	void reset(); 
	/*!
	*\brief Renvoie le dernier etat qui a ete genere
	*/
	const Etat2D& dernier() const { return *etats2D[rang%nbMaxEtats]; }
	/*!
	*\brief Renvoie le rang du dernier etat cree
	*/
	unsigned int getRangDernier() const { return rang; }
	/*!
	*\brief Destructeur 
	*/
	~Simulateur2D();

	/*!
	* \class iterator
	* \brief Classe qui permet de parcourir les derniers etats generes
	* tout en respectant le principe d'encapsulation
	*/
	// Iterateur 2D
	class iterator {
		friend class Simulateur2D;
		Simulateur2D* sim = nullptr;
		int i = 0;
		iterator(Simulateur2D* s) :sim(s), i(s->rang) {}
		iterator(Simulateur2D* s, int dep) :sim(s), i(dep) {}
	public:
		iterator() {}
		iterator& operator++() {
			i--;
			if (i == -1 && sim->rang >= sim->nbMaxEtats) i = sim->nbMaxEtats - 1;
			return *this;
		}
		Etat2D& operator*() const {
			return *sim->etats2D[i%sim->nbMaxEtats];
		}
		bool operator!=(iterator it) const { return sim != it.sim || i != it.i; }
	};

	iterator begin() { return iterator(this); }
	iterator end() { if (rang < nbMaxEtats) return iterator(this, -1); else return iterator(this, rang - nbMaxEtats); }
	/*!
	* \class const_iterator
	* \brief Classe qui permet de parcourir les objets Etat2D (constants)
	* tout en respectant le principe d'encapsulation
	*/
	class const_iterator {
		friend class Simulateur2D;
		const Simulateur2D* sim = nullptr;
		int i = 0;
		const_iterator(const Simulateur2D* s) :sim(s), i(s->rang) {}
		const_iterator(const Simulateur2D* s, int dep) :sim(s), i(dep) {}
	public:
		const_iterator() {}
		const_iterator& operator++() {
			i--;
			if (i == -1 && sim->rang >= sim->nbMaxEtats) i = sim->nbMaxEtats - 1;
			return *this;
		}
		const Etat2D& operator*() const {
			return *sim->etats2D[i%sim->nbMaxEtats];
		}
		bool operator!=(const_iterator it) const { return sim != it.sim || i != it.i; }
	};

	const_iterator begin() const { return const_iterator(this); }
	const_iterator end() const { if (rang < nbMaxEtats) return const_iterator(this, -1); else return const_iterator(this, rang - nbMaxEtats); }
	const_iterator cbegin() const { return const_iterator(this); }
	const_iterator cend() const { if (rang < nbMaxEtats) return const_iterator(this, -1); else return const_iterator(this, rang - nbMaxEtats); }

};



#endif // SIMULATEUR_H_INCLUDED
