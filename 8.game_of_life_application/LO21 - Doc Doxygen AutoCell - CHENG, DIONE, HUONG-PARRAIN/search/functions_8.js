var searchData=
[
  ['setcellule',['setCellule',['../class_etat1_d.html#a149eb63f199190da9464c485d4a9f099',1,'Etat1D::setCellule()'],['../class_etat2_d.html#aaf9391d7268af90a653632344cd3cc17',1,'Etat2D::setCellule()']]],
  ['setetatdepart',['setEtatDepart',['../class_simulateur1_d.html#a6acef36a0b627000967f8558dcc2f657',1,'Simulateur1D::setEtatDepart()'],['../class_simulateur2_d.html#a3df2ecfad9e9b7c119826f9aa61fe875',1,'Simulateur2D::setEtatDepart()']]],
  ['simulateur1d',['Simulateur1D',['../class_simulateur1_d.html#ac3653af154ccc0f8e0c301be523a552e',1,'Simulateur1D::Simulateur1D(const Automate1D &amp;a, unsigned int buffer=10)'],['../class_simulateur1_d.html#ad3f921042d27b7e8d53406ae2d1740b4',1,'Simulateur1D::Simulateur1D(const Automate1D &amp;a, const Etat1D &amp;dep, unsigned int buffer=10)']]],
  ['simulateur2d',['Simulateur2D',['../class_simulateur2_d.html#a17f90f7f330701d7f63b4ba0e09051ed',1,'Simulateur2D::Simulateur2D(const Automate2D &amp;a, unsigned int buffer=10)'],['../class_simulateur2_d.html#a48a6fd4c1372568c872c6b1acd3534fe',1,'Simulateur2D::Simulateur2D(const Automate2D &amp;a, const Etat2D &amp;dep, unsigned int buffer=10)']]]
];
