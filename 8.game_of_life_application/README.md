*Project carried out as a trinomial in spring 2018*

# Game of life application: AutoCell

This project was carried out as part of my engineering training in spring 2018 (LO21 teaching).
This project was carried out in trinomial over several weeks.
The goal was to code a C++ / Qt simulation application of the **game of life** (AutoCell).
We also leave the possibility of choosing from a known variant of the game of life: 1D, 2D and "Forest Fires".
It is also possible to save and load states of the game of life in XML files. 
(See the video below for more details.)


A live test of our application has been carried out and can be viewed via the Youtube link below:
- https://youtu.be/72DJOmVvR_M

For more details regarding the game of life, some links are given below:
- https://fr.wikipedia.org/wiki/Jeu_de_la_vie
- http://ressources.univ-lemans.fr/AccesLibre/UM/Pedago/physique/02/recre/conway.html