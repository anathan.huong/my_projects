<!DOCTYPE html>

<html>
<head>
	<meta charset="utf-8">
	<title>Liste_Configuration_Recherchées</title>
	<LINK rel="stylesheet" HREF="miseenforme.css" type="text/css">
</head>

<body>
	<h1>Les configurations recherchées : </h1>

<?php

//Déclaration des variables
$id_cfg = $_POST["id_cfg"];
$montant_cfg = $_POST["montant_cfg"];
//$jachat = $_POST["jourachat"];
//$machat = $_POST["moisachat"];
//$aachat = $_POST["anneeachat"];
if ($_POST["anneeachat"] != 0)
{
	$date_achat = $_POST["anneeachat"].'-'.$_POST["moisachat"].'-'.$_POST["jourachat"];
}
$fourniss = $_POST["fourniss"];
$unite_gestion = $_POST["unite_gestion"];
$fk_cdm = $_POST["fk_cdm"];
if ($_POST["annee_udg"] != 0)
{
	$date_udg = $_POST["annee_udg"].'-'.$_POST["mois_udg"].'-'.$_POST["jour_udg"];
}


//insertion dans Configuration
$connect = pg_connect("host=tuxa.sme.utc dbname=dbnf17p036 user=nf17p036 password=yeN4oPJH") or die ('Error connecting to postgreSQL');
//$query = "insert into Configuration (date_achat, montant_cfg) VALUES ('$date_achat','$montant_cfg');";


$query = "select * from configuration";
$id_where = False;

if (!empty($id_cfg))
{
	if ($id_where)
	{
		$query = $query . " and id_cfg = '$id_cfg'";
	}
	else
	{
		$query = $query . " where id_cfg = '$id_cfg'";
		$id_where = True;
	}
}

if (!empty($montant_cfg))
{
	if ($id_where)
	{
		$query = $query . " and montant_cfg = '$montant_cfg'";
	}
	else
	{
		$query = $query . " where montant_cfg = '$montant_cfg'";
		$id_where = True;
	}

}

if (!empty($date_achat))
{
	if ($id_where)
	{
		$query = $query . " and date_achat = '$date_achat'";
	}
	else
	{
		$query = $query . " where date_achat = '$date_achat'";
		$id_where = True;
	}

}

if (!empty($fourniss))
{
	if ($id_where)
	{
		$query = $query . " and fk_fourniss = '$fourniss'";
	}
	else
	{
		$query = $query . " where fk_fourniss = '$fourniss'";
		$id_where = True;
	}

}

if (!empty($unite_gestion))
{
	if ($id_where)
	{
		$query = $query . " and fk_unite_gestion = '$unite_gestion'";
	}
	else
	{
		$query = $query . " where fk_unite_gestion = '$unite_gestion'";
		$id_where = True;
	}

}

if (!empty($fk_cdm))
{
	if ($id_where)
	{
		$query = $query . " and fk_cdm = '$fk_cdm'";
	}
	else
	{
		$query = $query . " where fk_cdm = '$fk_cdm'";
		$id_where = True;
	}

}

if (!empty($date_udg))
{
	if ($id_where)
	{
		$query = $query . " and date_affectation_udg = '$date_udg'";
	}
	else
	{
		$query = $query . " where date_affectation_udg = '$date_udg'";
		$id_where = True;
	}

}

//echo "<br>$query<br>";

$result = pg_query($connect, $query);
echo "<table border='1'>";
echo "<th>Id</th>";
echo "<th>date d'achat</th>";
echo "<th>montant</th>";
echo "<th>Id fournisseur</th>";
echo "<th>unite de gestion</th>";
echo "<th>date d'affectation/th>";
echo "<th>id contrat de maintenance</th>";


while ($row = pg_fetch_array($result))
{
	echo "<tr>";
	echo "<td>$row[0]</td>";
	echo "<td>$row[1]</td>";
	echo "<td>$row[2]</td>";
	echo "<td>$row[3]</td>";
	echo "<td>$row[4]</td>";
	echo "<td>$row[6]</td>";
	echo "<td>$row[5]</td>";
	echo "</tr>";
	if (empty($array_id_cfg))
	{
		$array_id_cfg = $row[0];
	}
	else
	{
		$array_id_cfg = $array_id_cfg . "," .$row[0];
	}
}

echo "</table><br>";

$array_id_cfg_temp = split(',', $array_id_cfg); //dans $array_id_cfg_temp, c'est une phrase comme 7,2. Donc, on utilise split pour faire cette phrase devenir un array[7,2].
foreach($array_id_cfg_temp as $value) //c'est pour parcourir chaque valeur dans array. Pour chaque configuration, on affiche un tableau de équipement correspondante.
{
	echo "<h1>Les équipements correspondantes de configuration" . $value ."</h1>";
	echo "<table>";
	echo "<th>numéro de série</th>";
	echo "<th>nature</th>";
	echo "<th>type</th>";
	echo "<th>id de la configuration</th>";

	$query2 = "select * from equipement where fk_config = '$value' order by id_eqp";
	$result2 = pg_query($connect, $query2);
	while ($row2 = pg_fetch_array($result2))
	{
		echo "<tr>";
		echo "<td>$row2[0]</td>";
		echo "<td>$row2[1]</td>";
		echo "<td>$row2[2]</td>";
		echo "<td>$row2[3]</td>";
		echo "</tr>";
	}

	echo "</table><br>";

}

pg_close($connect);

?>
<br>
<a href="Page_Accueil.html"> Retour vers la page d'accueil
</body>
</html>
