<!DOCTYPE html>

<html>
<head>
	<meta charset="utf-8">
	<title>Recherche équipements</title>
	<LINK rel="stylesheet" HREF="miseenforme.css" type="text/css">
</head>

<body>
	<h1>Relation entre les pannes et les différents attributs possibles</h1>


<?php

$relation = $_POST["relation"];
$connect = pg_connect("host=tuxa.sme.utc dbname=dbnf17p036 user=nf17p036 password=yeN4oPJH") or die ('Error connecting to postgreSQL');

if ($relation == 'equipement')
{
	$query = "SELECT equipement.nature, statut, COUNT(equipement.nature) AS TOTAL
	FROM ticket_intervention, equipement
	WHERE equipement.id_eqp = ticket_intervention.fk_equipement GROUP BY equipement.nature, statut order by statut;";
	$result = pg_query($connect, $query);

	echo " <br>Les relations entre équipements et les pannes sont : <br><br>";
	echo "<table>";
	echo "<th>Nature</th>";
	echo "<th>Statut</th>";
	echo "<th>Nombre de ticket d'intervention avec le statut correspondant</th>";

	while ($row = pg_fetch_array($result))
	{
		echo "<tr>";
		echo "<td>$row[0]</td>";
		echo "<td>$row[1]</td>";
		echo "<td>$row[2]</td>";
		echo "</tr>";
	}
	echo "</table><br>";

}
	if ($relation == 'fournisseur')
	{
		$query = "SELECT fournisseur.nom, statut, COUNT (fournisseur.nom) AS TOTAL
		FROM ticket_intervention, equipement, configuration, fournisseur
		WHERE equipement.id_eqp = ticket_intervention.fk_equipement
		AND equipement.fk_config = configuration.id_cfg
		AND configuration.fk_fourniss = fournisseur.id_frn
		GROUP BY fournisseur.nom, statut ORDER BY statut;";
		$result = pg_query($connect, $query);

		echo " <br>Les relations entre les fournisseurs et les pannes sont : <br><br>";
		echo "<table>";
		echo "<th>Nom du fournisseur</th>";
		echo "<th>Statut</th>";
		echo "<th>Nombre ticket total du fournisseur</th>";

		while ($row = pg_fetch_array($result))
		{
			echo "<tr>";
			echo "<td>$row[0]</td>";
			echo "<td>$row[1]</td>";
			echo "<td>$row[2]</td>";
			echo "</tr>";
		}
		echo "</table><br>";


	}
	pg_close($connect);

?>
<br>
<a href="Page_Accueil.html"> Retour vers la page d'accueil
</body>
</html>
