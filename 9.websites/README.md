*Paired project carried out at the beginning of my studies at the UTC - 1st and 2nd year at the University of Technology of Compiègne*.
The Driving School website has been done alone whereas the IT Park Management website has been done in a group of 4 people.

# Presentation

These are sites that were coded at the start of my studies at UTC. The objective was to put into practice the use of an Apache server and the classic programming languages: HTML / CSS / PHP / SQL.
These are therefore university projects to create basic websites.
The structure is the same for both sites: a front-end in HTML / CSS allows the user to enter information which will be saved in an SQL database (use of PHP / MySQL).
Since then, I have been able to greatly improve my skills in terms of documentation, comments, rigor, security and UI / UX design: mainly during my two 6-month engineering internships (after these projects).

# Driving School website folder

Management site for driving school sessions (creation of sessions, registration / unsubscription of students, display of statistics, display of sessions, etc.)

# IT Park Management website folder

Management site of a computer park (addition / removal of equipment, supplier, location, contracts, possibility of searching for equipment, end of warranty, etc.). The SQL queries were built in a more complex way than the Driving School site.