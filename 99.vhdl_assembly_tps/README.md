*Paired project carried out in Autumn 2018 (at UTC, course code: MI01)*

As part of the chosen course: MI01, engineering training, I studied low-level programming with VHDL and 8086 assembler languages.
In addition to the courses, projects have been carried out, making it possible to apply the codes seen in class to deepen them.
In each file is a practical session (2 hours per week), including: the description of the practical work, the source code and a report explaining our implementations.

- **VHDL**: TP2 and TP3. The practical work does not follow one another, the difficulty increases with the number.

- **Assembler**: TP4, TP5, TP6-7 and TP8. The practical work follows one another and we build, little by little, a code allowing the recognition of the contours of an image in order to be able to change the background. Once the code was finished, a benchmark with local software (C) was carried out to compare the performance of the functions.

# TP 2

Combinatorial VHDL. Small basic programs to train in VHDL.

# TP 3

Sequential VHDL, time counting.

# TP 4

Getting started with the assembler development environment and first programs.

# TP 5

MI01 TP5 - Image processing - Part 1. Convert to grayscale.

# TP 6-7

MI01 TP6 / 7 - Image processing - Part two. Detection of the edges of an image.

# TP 8

MI01 TP6 / 7 - Image Processing - Part Three. Vectorization.