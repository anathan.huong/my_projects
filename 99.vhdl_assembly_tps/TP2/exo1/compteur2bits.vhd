----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:28:06 10/06/2017 
-- Design Name: 
-- Module Name:    compteur2bits - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity compteur2bits is
	PORT(PB_0, PB_1 : IN BIT ; LED_10 : OUT INTEGER RANGE 0 TO 3);
end compteur2bits;

architecture Behavioral of compteur2bits is

begin
	process(PB_0)
	VARIABLE compteur : INTEGER RANGE 0 TO 3;
	begin
	if(PB_1 = '1') then
		compteur := 0;
		LED_10<=compteur;
	else
		if (PB_0'event and PB_0 = '1') then
			if (compteur=3) then
				compteur :=0;
				LED_10<=compteur;
			else
			compteur := compteur + 1;
			LED_10<=compteur;
			end if;
		end if;
	end if ;
	end process;
end Behavioral;

