/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "//nassme.utc/~mi01a043/TP2/exo1/compteur2bits.vhd";



static void work_a_2176036037_3212880686_p_0(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    int t6;
    char *t7;
    char *t8;
    char *t9;
    unsigned char t10;
    unsigned char t11;
    unsigned char t12;
    int t13;

LAB0:    xsi_set_current_line(42, ng0);
    t1 = (t0 + 1192U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)1);
    if (t4 != 0)
        goto LAB2;

LAB4:    xsi_set_current_line(46, ng0);
    t1 = (t0 + 992U);
    t4 = xsi_signal_has_event(t1);
    if (t4 == 1)
        goto LAB8;

LAB9:    t3 = (unsigned char)0;

LAB10:    if (t3 != 0)
        goto LAB5;

LAB7:
LAB6:
LAB3:    t1 = (t0 + 2952);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(43, ng0);
    t1 = (t0 + 1648U);
    t5 = *((char **)t1);
    t1 = (t5 + 0);
    *((int *)t1) = 0;
    xsi_set_current_line(44, ng0);
    t1 = (t0 + 1648U);
    t2 = *((char **)t1);
    t6 = *((int *)t2);
    t1 = (t0 + 3032);
    t5 = (t1 + 56U);
    t7 = *((char **)t5);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    *((int *)t9) = t6;
    xsi_driver_first_trans_fast_port(t1);
    goto LAB3;

LAB5:    xsi_set_current_line(47, ng0);
    t2 = (t0 + 1648U);
    t7 = *((char **)t2);
    t6 = *((int *)t7);
    t12 = (t6 == 3);
    if (t12 != 0)
        goto LAB11;

LAB13:    xsi_set_current_line(51, ng0);
    t1 = (t0 + 1648U);
    t2 = *((char **)t1);
    t6 = *((int *)t2);
    t13 = (t6 + 1);
    t1 = (t0 + 1648U);
    t5 = *((char **)t1);
    t1 = (t5 + 0);
    *((int *)t1) = t13;
    xsi_set_current_line(52, ng0);
    t1 = (t0 + 1648U);
    t2 = *((char **)t1);
    t6 = *((int *)t2);
    t1 = (t0 + 3032);
    t5 = (t1 + 56U);
    t7 = *((char **)t5);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    *((int *)t9) = t6;
    xsi_driver_first_trans_fast_port(t1);

LAB12:    goto LAB6;

LAB8:    t2 = (t0 + 1032U);
    t5 = *((char **)t2);
    t10 = *((unsigned char *)t5);
    t11 = (t10 == (unsigned char)1);
    t3 = t11;
    goto LAB10;

LAB11:    xsi_set_current_line(48, ng0);
    t2 = (t0 + 1648U);
    t8 = *((char **)t2);
    t2 = (t8 + 0);
    *((int *)t2) = 0;
    xsi_set_current_line(49, ng0);
    t1 = (t0 + 1648U);
    t2 = *((char **)t1);
    t6 = *((int *)t2);
    t1 = (t0 + 3032);
    t5 = (t1 + 56U);
    t7 = *((char **)t5);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    *((int *)t9) = t6;
    xsi_driver_first_trans_fast_port(t1);
    goto LAB12;

}


extern void work_a_2176036037_3212880686_init()
{
	static char *pe[] = {(void *)work_a_2176036037_3212880686_p_0};
	xsi_register_didat("work_a_2176036037_3212880686", "isim/compteur2bits_isim_beh.exe.sim/work/a_2176036037_3212880686.didat");
	xsi_register_executes(pe);
}
