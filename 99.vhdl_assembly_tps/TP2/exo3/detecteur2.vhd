----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:27:28 10/06/2017 
-- Design Name: 
-- Module Name:    detecteur1 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity detecteur2 is
	PORT(SW_0, PB_0 : IN BIT; LED_0 : OUT BIT; LED_7654 : OUT INTEGER RANGE 0 TO 15);
end detecteur2;

architecture Behavioral of detecteur2 is
begin
	PROCESS(PB_0)
	VARIABLE etat : INTEGER RANGE 0 TO 5;
	BEGIN
		IF(PB_0'EVENT AND PB_0='1') THEN
			CASE etat IS
				WHEN 0 => IF(SW_0='1') THEN
								etat:= 1;
								LED_7654<=1;
								LED_0<='0';
							  END IF;
				
				WHEN 1 => IF(SW_0='1') THEN
								etat:= 2;
								LED_7654<=2;
								LED_0<='0';
							  END IF;
				
				WHEN 2 => IF(SW_0='0') THEN
								etat:= 3;
								LED_7654<=3;
								LED_0<='0';
							  END IF;
				
				WHEN 3 => IF(SW_0='1') THEN
								etat:= 4;
								LED_7654<=4;
								LED_0<='0';
							  END IF;
				
				WHEN 4 => IF(SW_0='0') THEN
								etat:= 5;
								LED_7654<=5;
								LED_0<='1';
							  END IF;
				WHEN OTHERS => etat:= 0;
									LED_7654<=0;
									LED_0<='0';

			END CASE;								
		END IF;
	END PROCESS;
end Behavioral;