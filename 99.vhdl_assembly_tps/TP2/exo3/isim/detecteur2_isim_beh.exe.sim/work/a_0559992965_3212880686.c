/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "//nassme.utc/~mi01a043/TP2/exo3/detecteur2.vhd";



static void work_a_0559992965_3212880686_p_0(char *t0)
{
    unsigned char t1;
    char *t2;
    unsigned char t3;
    char *t4;
    char *t5;
    unsigned char t6;
    unsigned char t7;
    char *t8;
    int t9;
    char *t10;
    unsigned char t11;
    unsigned char t12;
    char *t13;

LAB0:    xsi_set_current_line(41, ng0);
    t2 = (t0 + 1152U);
    t3 = xsi_signal_has_event(t2);
    if (t3 == 1)
        goto LAB5;

LAB6:    t1 = (unsigned char)0;

LAB7:    if (t1 != 0)
        goto LAB2;

LAB4:
LAB3:    t2 = (t0 + 3112);
    *((int *)t2) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(42, ng0);
    t4 = (t0 + 1808U);
    t8 = *((char **)t4);
    t9 = *((int *)t8);
    if (t9 == 0)
        goto LAB9;

LAB15:    if (t9 == 1)
        goto LAB10;

LAB16:    if (t9 == 2)
        goto LAB11;

LAB17:    if (t9 == 3)
        goto LAB12;

LAB18:    if (t9 == 4)
        goto LAB13;

LAB19:
LAB14:    xsi_set_current_line(72, ng0);
    t2 = (t0 + 1808U);
    t4 = *((char **)t2);
    t2 = (t4 + 0);
    *((int *)t2) = 0;
    xsi_set_current_line(73, ng0);
    t2 = (t0 + 3192);
    t4 = (t2 + 56U);
    t5 = *((char **)t4);
    t8 = (t5 + 56U);
    t10 = *((char **)t8);
    *((int *)t10) = 0;
    xsi_driver_first_trans_fast_port(t2);
    xsi_set_current_line(74, ng0);
    t2 = (t0 + 3256);
    t4 = (t2 + 56U);
    t5 = *((char **)t4);
    t8 = (t5 + 56U);
    t10 = *((char **)t8);
    *((unsigned char *)t10) = (unsigned char)0;
    xsi_driver_first_trans_fast_port(t2);

LAB8:    goto LAB3;

LAB5:    t4 = (t0 + 1192U);
    t5 = *((char **)t4);
    t6 = *((unsigned char *)t5);
    t7 = (t6 == (unsigned char)1);
    t1 = t7;
    goto LAB7;

LAB9:    xsi_set_current_line(43, ng0);
    t4 = (t0 + 1032U);
    t10 = *((char **)t4);
    t11 = *((unsigned char *)t10);
    t12 = (t11 == (unsigned char)1);
    if (t12 != 0)
        goto LAB21;

LAB23:
LAB22:    goto LAB8;

LAB10:    xsi_set_current_line(49, ng0);
    t2 = (t0 + 1032U);
    t4 = *((char **)t2);
    t1 = *((unsigned char *)t4);
    t3 = (t1 == (unsigned char)1);
    if (t3 != 0)
        goto LAB24;

LAB26:
LAB25:    goto LAB8;

LAB11:    xsi_set_current_line(55, ng0);
    t2 = (t0 + 1032U);
    t4 = *((char **)t2);
    t1 = *((unsigned char *)t4);
    t3 = (t1 == (unsigned char)0);
    if (t3 != 0)
        goto LAB27;

LAB29:
LAB28:    goto LAB8;

LAB12:    xsi_set_current_line(61, ng0);
    t2 = (t0 + 1032U);
    t4 = *((char **)t2);
    t1 = *((unsigned char *)t4);
    t3 = (t1 == (unsigned char)1);
    if (t3 != 0)
        goto LAB30;

LAB32:
LAB31:    goto LAB8;

LAB13:    xsi_set_current_line(67, ng0);
    t2 = (t0 + 1032U);
    t4 = *((char **)t2);
    t1 = *((unsigned char *)t4);
    t3 = (t1 == (unsigned char)0);
    if (t3 != 0)
        goto LAB33;

LAB35:
LAB34:    goto LAB8;

LAB20:;
LAB21:    xsi_set_current_line(44, ng0);
    t4 = (t0 + 1808U);
    t13 = *((char **)t4);
    t4 = (t13 + 0);
    *((int *)t4) = 1;
    xsi_set_current_line(45, ng0);
    t2 = (t0 + 3192);
    t4 = (t2 + 56U);
    t5 = *((char **)t4);
    t8 = (t5 + 56U);
    t10 = *((char **)t8);
    *((int *)t10) = 1;
    xsi_driver_first_trans_fast_port(t2);
    xsi_set_current_line(46, ng0);
    t2 = (t0 + 3256);
    t4 = (t2 + 56U);
    t5 = *((char **)t4);
    t8 = (t5 + 56U);
    t10 = *((char **)t8);
    *((unsigned char *)t10) = (unsigned char)0;
    xsi_driver_first_trans_fast_port(t2);
    goto LAB22;

LAB24:    xsi_set_current_line(50, ng0);
    t2 = (t0 + 1808U);
    t5 = *((char **)t2);
    t2 = (t5 + 0);
    *((int *)t2) = 2;
    xsi_set_current_line(51, ng0);
    t2 = (t0 + 3192);
    t4 = (t2 + 56U);
    t5 = *((char **)t4);
    t8 = (t5 + 56U);
    t10 = *((char **)t8);
    *((int *)t10) = 2;
    xsi_driver_first_trans_fast_port(t2);
    xsi_set_current_line(52, ng0);
    t2 = (t0 + 3256);
    t4 = (t2 + 56U);
    t5 = *((char **)t4);
    t8 = (t5 + 56U);
    t10 = *((char **)t8);
    *((unsigned char *)t10) = (unsigned char)0;
    xsi_driver_first_trans_fast_port(t2);
    goto LAB25;

LAB27:    xsi_set_current_line(56, ng0);
    t2 = (t0 + 1808U);
    t5 = *((char **)t2);
    t2 = (t5 + 0);
    *((int *)t2) = 3;
    xsi_set_current_line(57, ng0);
    t2 = (t0 + 3192);
    t4 = (t2 + 56U);
    t5 = *((char **)t4);
    t8 = (t5 + 56U);
    t10 = *((char **)t8);
    *((int *)t10) = 3;
    xsi_driver_first_trans_fast_port(t2);
    xsi_set_current_line(58, ng0);
    t2 = (t0 + 3256);
    t4 = (t2 + 56U);
    t5 = *((char **)t4);
    t8 = (t5 + 56U);
    t10 = *((char **)t8);
    *((unsigned char *)t10) = (unsigned char)0;
    xsi_driver_first_trans_fast_port(t2);
    goto LAB28;

LAB30:    xsi_set_current_line(62, ng0);
    t2 = (t0 + 1808U);
    t5 = *((char **)t2);
    t2 = (t5 + 0);
    *((int *)t2) = 4;
    xsi_set_current_line(63, ng0);
    t2 = (t0 + 3192);
    t4 = (t2 + 56U);
    t5 = *((char **)t4);
    t8 = (t5 + 56U);
    t10 = *((char **)t8);
    *((int *)t10) = 4;
    xsi_driver_first_trans_fast_port(t2);
    xsi_set_current_line(64, ng0);
    t2 = (t0 + 3256);
    t4 = (t2 + 56U);
    t5 = *((char **)t4);
    t8 = (t5 + 56U);
    t10 = *((char **)t8);
    *((unsigned char *)t10) = (unsigned char)0;
    xsi_driver_first_trans_fast_port(t2);
    goto LAB31;

LAB33:    xsi_set_current_line(68, ng0);
    t2 = (t0 + 1808U);
    t5 = *((char **)t2);
    t2 = (t5 + 0);
    *((int *)t2) = 5;
    xsi_set_current_line(69, ng0);
    t2 = (t0 + 3192);
    t4 = (t2 + 56U);
    t5 = *((char **)t4);
    t8 = (t5 + 56U);
    t10 = *((char **)t8);
    *((int *)t10) = 5;
    xsi_driver_first_trans_fast_port(t2);
    xsi_set_current_line(70, ng0);
    t2 = (t0 + 3256);
    t4 = (t2 + 56U);
    t5 = *((char **)t4);
    t8 = (t5 + 56U);
    t10 = *((char **)t8);
    *((unsigned char *)t10) = (unsigned char)1;
    xsi_driver_first_trans_fast_port(t2);
    goto LAB34;

}


extern void work_a_0559992965_3212880686_init()
{
	static char *pe[] = {(void *)work_a_0559992965_3212880686_p_0};
	xsi_register_didat("work_a_0559992965_3212880686", "isim/detecteur2_isim_beh.exe.sim/work/a_0559992965_3212880686.didat");
	xsi_register_executes(pe);
}
