----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:49:36 10/13/2017 
-- Design Name: 
-- Module Name:    circulation - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity circulation is
	PORT(PB_0, PB_1, PB_2, Clk100MHz : IN BIT;
	LED_3210, LED_7654 : OUT BIT_VECTOR (0 TO 3));end circulation;


architecture Behavioral of circulation is
 alias reset is PB_0; -- alias pour le signal de r�initialisation
 signal clk_out : bit := '0'; -- signal d'horloge apr�s division
 
 
-- -- Constante de division, ici pour une sortie � 1Hz.
 constant clock_divisor : integer := 100000000;
begin

-- Diviseur de fr�quence : divise la fr�quence du signal Clk100MHz par clock_div.
clock_divider : process(Clk100Mhz, reset)
variable c : integer range 0 to clock_divisor - 1 := 0;
begin
if reset = '1' then
c := 0;
clk_out <= '0';
elsif Clk100MHz'event and Clk100MHz = '1' then
if c < (clock_divisor - 1) / 2 then
c := c + 1;
clk_out <= '0';
elsif c = (clock_divisor - 1) then
c := 0;
clk_out <= '0';
else
c := c + 1;
clk_out <= '1';
end if;
end if;
end process;

process(clk_out)
variable	etat : INTEGER RANGE 0 TO 3 ;
variable V1, O1, R1, V2, O2, R2 : BIT;
variable t : INTEGER RANGE 0 to 255; -- on augmente la taille de t
begin
	IF reset='1' THEN
		t := 0 ;
		etat := 0 ;
		R1 := '1'; V2 := '1'; O1 := '0'; V1 := '0'; R2 := '0'; O2 := '0';
		LED_3210(3)<=R1;
		LED_3210(2)<=O1;
		LED_3210(1)<=V1;
		LED_3210(0)<='0';
		
		LED_7654(3)<=R2;
		LED_7654(2)<=O2;
		LED_7654(1)<=V2;
		LED_7654(0)<='0';
	ELSIF (clk_out 'EVENT AND clk_out = '1') THEN
		CASE etat is 
			
			WHEN 0 => R1 := '1'; V2 := '1'; O1 := '0'; V1 := '0'; R2 := '0'; O2 := '0';
				t := t + 1;
				LED_3210(3)<=R1;
				LED_3210(2)<=O1;
				LED_3210(1)<=V1;
				LED_3210(0)<='0';
				
				LED_7654(3)<=R2;
				LED_7654(2)<=O2;
				LED_7654(1)<=V2;
				LED_7654(0)<='0';
				IF (PB_1='1' AND t >= 8) THEN --on attend qu'une voiture passe sur l'axe 1
					etat := 1;
					t:=8;
				END IF;
			
			WHEN 1 => R1 := '1'; V2 := '0'; O1 := '0'; V1 := '0'; R2 := '0'; O2 := '1';
				t := t + 1;
				LED_3210(3)<=R1;
				LED_3210(2)<=O1;
				LED_3210(1)<=V1;
				LED_3210(0)<='0';
				
				LED_7654(3)<=R2;
				LED_7654(2)<=O2;
				LED_7654(1)<=V2;
				LED_7654(0)<='0';
				IF (t = 10) THEN 
					etat := 2;
				END IF;
				  
			WHEN 2 => R1 := '0'; V2 := '0'; O1 := '0'; V1 := '1'; R2 := '1'; O2 := '0';
				t := t + 1;
				LED_3210(3)<=R1;
				LED_3210(2)<=O1;
				LED_3210(1)<=V1;
				LED_3210(0)<='0';
				
				LED_7654(3)<=R2;
				LED_7654(2)<=O2;
				LED_7654(1)<=V2;
				LED_7654(0)<='0';
				IF (PB_2='1' AND t >= 18) THEN  -- on attend qu'une voiture passe sur l'axe 2
					etat := 3;
					t:=2;
				END	IF;

			WHEN 3 => R1 := '0'; V2 := '0'; O1 := '1'; V1 := '0'; R2 := '1'; O2 := '0';
				t := t - 1;
				LED_3210(3)<=R1;
				LED_3210(2)<=O1;
				LED_3210(1)<=V1;
				LED_3210(0)<='0';
				
				LED_7654(3)<=R2;
				LED_7654(2)<=O2;
				LED_7654(1)<=V2;
				LED_7654(0)<='0';
				IF (t = 0) THEN 
					etat := 0;
				END IF;
		END CASE ;
	END IF;
end process;


end Behavioral;