; conversion.asm
;
; MI01 - TP Assembleur 1
;
; Affiche un nombre de 32 bits sous forme lisible

title conversion.asm

.686
.model 		flat, c

extern      putchar:near
extern      getchar:near

.data

nombre			dd		95c8ah				; Nombre � convertir
base			db		35				;base d'arriv�e
chiffres		db		"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
chaine			db		32 dup(?)			; Remplacer xx par la longueur maximale n de la cha�ne



.code

; Sous-programme main, automatiquement appel� par le code de
; d�marrage 'C'
public      main
main        proc

			push	ebx					; sauvegarde de la valeur du registre
			push	ecx

			xor		ebx,ebx				; ebx index initialis� � 0
			movzx	ecx,[base]				; on stocke le diviseur sur 32bits

			mov		eax,[nombre]
			cmp		eax,0				;on d�termine le signe du nombre
			jns		boucle

			mov		edx,'-'			
			push	ecx
			push	eax
			push	edx
			call	putchar					;on affiche le signe "-"
			add		esp,4
			pop		eax
			pop		ecx
			neg		eax				;on stocke l'oppos� de nombre
			
boucle:		
			xor		edx,edx
			div		ecx				; eax le quotient et edx le reste

			mov		dl,[chiffres+edx]
			mov		[chaine + ebx], dl
			inc		ebx
			cmp		eax,0
			jne		boucle



suivant:
			movzx	eax, byte ptr [chaine+ebx] 
			push	eax 
			call	putchar 
			add		esp , 4 
			dec		ebx
			cmp		ebx,-1
			jne		suivant 

			pop		ecx
			pop		ebx				; restaure l'�tat du registre

            call    getchar						; Attente de l'appui sur "Entr�e"

            ret								; Retour au code de d�marrage 'C'

main        endp

            end