; conversion.asm
;
; MI01 - TP Assembleur 1
;
; Affiche un nombre de 32 bits sous forme lisible

title conversion.asm

.686
.model 		flat, c

extern      putchar:near
extern      getchar:near

.data

nombre			dd		95c8ah						; Nombre � convertir
base			db		10							;base d'arriv�e
chaine			db		10 dup(?)					; Remplacer xx par la longueur maximale n de la cha�ne



.code

; Sous-programme main, automatiquement appel� par le code de
; d�marrage 'C'
public      main
main        proc

			push	ebx								; sauvegarde de la valeur du registre
			push	ecx

			xor		ebx,ebx							; ebx index initialis� � 0
			movzx	ecx,[base]						; on stocke le diviseur sur 32bits

			mov		eax,[nombre]
			
boucle:		
			xor		edx,edx
			div		ecx								; eax le quotient et edx le reste

			add		dl,'0'
			mov		[chaine + ebx], dl
			inc		ebx
			cmp		eax,0
			jne		boucle



suivant:
			movzx	eax, byte ptr [chaine+ebx] 
			push	eax 
			call	putchar 
			add		esp , 4 
			dec		ebx
			cmp		ebx,-1
			jne		suivant 

			pop		ecx
			pop		ebx								; restaure l'�tat du registre

            call    getchar							; Attente de l'appui sur "Entr�e"

            ret										; Retour au code de d�marrage 'C'

main        endp

            end