; IMAGE.ASM
;
; MI01 - TP Assembleur 2 � 5
;
; R�alise le traitement d'une image 32 bits.

.686
.MODEL FLAT, C

.DATA

.CODE

; **********************************************************************
; Sous-programme _process_image_asm
;
; R�alise le traitement d'une image 32 bits.
;
; Entr�es sur la pile : Largeur de l'image (entier 32 bits)
;           Hauteur de l'image (entier 32 bits)
;           Pointeur sur l'image source (d�pl. 32 bits)
;           Pointeur sur l'image tampon 1 (d�pl. 32 bits)
;           Pointeur sur l'image tampon 2 (d�pl. 32 bits)
;           Pointeur sur l'image finale (d�pl. 32 bits)
; **********************************************************************
PUBLIC      process_image_asm
process_image_asm   PROC NEAR       ; Point d'entr�e du sous programme

        push    ebp
        mov     ebp, esp

        push    ebx
        push    esi
        push    edi

        mov     ecx, [ebp + 8]		;largeur
        imul    ecx, [ebp + 12]		;ecx=index

        mov     esi, [ebp + 16]		;adresse du premier pixel de l'image source
        mov     edi, [ebp + 20]		;adresse du premier pixel de l'image tampon 1

boucle :
		cmp		ecx,1				;si index>=1, on entre dans la boucle de traitement de pixel
		jbe		fin

		dec		ecx
		mov		ebx,[esi+ecx*4]		;on recupere notre pixel
		mov		eax,ebx				;on le copie dans eax
		and		eax,000000FFh		;on r�cup�re la composante bleue
		shl		eax,8				;on la stocke dans ax sur 16 bits dont 8 apr�s la virgule
		mov		dx,001Dh			;on stocke le facteur 001D
		mul		dx					;ax:dx <-- ax*dx
		mov		al,dl			
		rol		ax,8				;resultat de 0.114*B
		push	ax					;on le sauvegarde dans la pile
		
		mov		eax,ebx				
		and		eax,0000FF00h		;on r�cup�re la composante verte du pixel dans ax (16 bits avec 8 bits apr�s la virgule)
		mov		dx,0096h			;on stocke le facteur 0096
		mul		dx					;ax:dx <-- ax*dx
		mov		al,dl
		rol		ax,8				;resultat de 0.587*V
		push	ax					;on le sauvegarde dans la pile

		mov		eax,ebx
		and		eax,00FF0000h		
		shr		eax,8				;on r�cup�re la composante rouge du pixel dans ax (16 bits avec 8 bits apr�s la virgule)
		mov		dx,004Ch			;on stocke dans dx le facteur 004C
		mul		dx					;ax:dx <-- ax*dx
		mov		al,dl
		rol		ax,8				;resultat de 0.299*R
		push	ax					;on le sauvegarde dans la pile

		xor		eax,eax				;on r�initialise eax � 0
		pop		ax					;ax <-- 0.299*R
		pop		bx					;bx <-- 0.587*V
		add		ax,bx				;ax <-- 0.299*R + 0.587*V
		pop		bx					;bx <-- 0.114*B
		add		ax,bx				;ax <-- 0.299*R + 0.587*B + 0.114*B = I
		shr		ax,8				;on stocke I dans le premier octet de donn�e (correspond � la couleur bleue du pixel)
		mov		[edi+ecx*4],eax		;on stocke le r�sultat dans l'image tampon

		jmp boucle
		
fin:
        pop     edi
        pop     esi
        pop     ebx

		leave
        ret                         ; Retour � la fonction MainWndProc

process_image_asm   ENDP
END