; IMAGE.ASM
;
; MI01 - TP Assembleur 2 � 5
;
; R�alise le traitement d'une image 32 bits.

.686
.MODEL FLAT, C

.DATA

.CODE

; **********************************************************************
; Sous-programme _process_image_asm
;
; R�alise le traitement d'une image 32 bits.
;
; Entr�es sur la pile : Largeur de l'image (entier 32 bits)
;           Hauteur de l'image (entier 32 bits)
;           Pointeur sur l'image source (d�pl. 32 bits)
;           Pointeur sur l'image tampon 1 (d�pl. 32 bits)
;           Pointeur sur l'image tampon 2 (d�pl. 32 bits)
;           Pointeur sur l'image finale (d�pl. 32 bits)
; **********************************************************************
PUBLIC      process_image_asm
process_image_asm   PROC NEAR       ; Point d'entr�e du sous programme

        push    ebp
        mov     ebp, esp

        push    ebx
        push    esi
        push    edi

        mov     ecx, [ebp + 8]		;largeur
        imul    ecx, [ebp + 12]		;ecx=index

        mov     esi, [ebp + 16]		;adresse du premier pixel de l'image source
        mov     edi, [ebp + 20]		;adresse du premier pixel de l'image tampon 1

boucle :
		cmp		ecx,1				;si index>=1, on entre dans la boucle de traitement de pixel
		jbe		etape2

		dec		ecx
		mov		ebx,[esi+ecx*4]		;on recupere notre pixel
		mov		eax,ebx				;on le copie dans eax
		and		eax,000000FFh		;on r�cup�re la composante bleue
		shl		eax,8				;on la stocke dans ax sur 16 bits dont 8 apr�s la virgule
		mov		dx,001Dh			;on stocke le facteur 001D
		mul		dx					;ax:dx <-- ax*dx
		mov		al,dl			
		rol		ax,8				;resultat de 0.114*B
		push	ax					;on le sauvegarde dans la pile
		
		mov		eax,ebx				
		and		eax,0000FF00h		;on r�cup�re la composante verte du pixel dans ax (16 bits avec 8 bits apr�s la virgule)
		mov		dx,0096h			;on stocke le facteur 0096
		mul		dx					;ax:dx <-- ax*dx
		mov		al,dl
		rol		ax,8				;resultat de 0.587*V
		push	ax					;on le sauvegarde dans la pile

		mov		eax,ebx
		and		eax,00FF0000h		
		shr		eax,8				;on r�cup�re la composante rouge du pixel dans ax (16 bits avec 8 bits apr�s la virgule)
		mov		dx,004Ch			;on stocke dans dx le facteur 004C
		mul		dx					;ax:dx <-- ax*dx
		mov		al,dl
		rol		ax,8				;resultat de 0.299*R
		push	ax					;on le sauvegarde dans la pile

		xor		eax,eax				;on r�initialise eax � 0
		pop		ax					;ax <-- 0.299*R
		pop		bx					;bx <-- 0.587*V
		add		ax,bx				;ax <-- 0.299*R + 0.587*V
		pop		bx					;bx <-- 0.114*B
		add		ax,bx				;ax <-- 0.299*R + 0.587*B + 0.114*B = I
		shr		ax,8				;on stocke I dans le premier octet de donn�e (correspond � la couleur bleue du pixel)
		mov		[edi+ecx*4],eax		;on stocke le r�sultat dans l'image tampon

		jmp boucle
		
;TP 6 : d�tection de contours
etape2 :
		mov     ecx, [ebp + 8]	;largeur en pixels
		lea		eax,[ecx*4]
        imul    ecx, [ebp + 12]	; ecx=largeur*hauteur en pixels*pixels
		imul	ecx,4
		
		mov		esi,edi			;esi prend le premier pixel de l'image source (tampon 1)
		add		esi,ecx
		sub		esi,eax
		sub		esi,eax			;esi = premier pixel de la derni�re ligne (ce n'est pas le premier pixel trait�, il faut encore soustraire 4 � esi)

		mov		edi,[ebp+24]
		add		edi,ecx
		sub		edi,eax			;edi = premier pixel de la derni�re ligne (ce n'est pas le premier pixel destination, il faut encore soustraire 4 � edi)

		mov     ecx, [ebp + 12]	;hauteur
		shl		ecx,16
		sub		ecx,00010000h

		push	ebp
		mov		ebp,[ebp + 8]		;on r�cup�re la largeur

lignes :
		sub		ecx,00010000h	;on d�cr�mente le num�ro de ligne
		test	ecx,0FFFF0000h
		jz		fin
		sub		esi,8 ; fin de ligne
		sub		edi,8
		mov		cx,bp
		sub		cx,2

colonnes :
		
Sx : 
		mov		eax,[esi]			;on r�cup�re "a11"
		neg		eax					;on traite le pixel et on l'ajoute � Sx
		add		eax,[esi+8]			;on r�cup�re "a13" et on l'ajoute � Sx
		mov		ebx,[esi+ebp*4]		;on r�cup�re "a21"
		imul	ebx,2				;on traite le pixel
		sub		eax,ebx				;on le soustrait � Sx
		mov		ebx,[esi+ebp*4+8]	;on r�cup�re "a23"
		imul	ebx,2				;on traite le pixel
		add		eax,ebx				;on l'ajoute � Sx
		sub		eax,[esi+ebp*8]		;on soustrait "a31" � Sx
		add		eax,[esi+ebp*8+8]	;on ajoute "a33" � Sx
		cmp		eax,0				;on applique la valeur absolue
		jge		Sy
		neg		eax

Sy :
		mov		ebx,[esi]			;on r�cup�re "a11" et on l'ajoute � Sy
		mov		edx,[esi+4]			;on r�cup�re "a21"
		imul	edx,2				;on traite le pixel
		add		ebx,edx				;on l'ajoute � Sy
		add		ebx,[esi+8]			;on r�cup�re "a13" et on l'ajoute � Sy
		mov		edx,[esi+ebp*8]		;on r�cup�re "a31"
		sub		ebx,edx				;on le soustrait � Sy
		mov		edx,[esi+ebp*8+4]	;on r�cup�re "a32"
		imul	edx,2				;on traite le pixel
		sub		ebx,edx				;on le soustrait � Sy
		mov		edx,[esi+ebp*8+8]	;on r�cup�re "a33"
		sub		ebx,edx				;on le soustrait � Sy
		cmp		ebx,0				;on applique la valeur absolue
		jge		calculG
		neg		ebx

		
calculG :
		xor		edx,edx
		mov		edx,eax
		add		edx,ebx
		cmp		edx,255
		jbe		calculG2
		mov		edx,255

calculG2:
		neg		edx
		add		edx, 255

		;copie de l'intensit� dans les trois couleurs R G B pour avoir des niveaux de gris :
		mov		dh,dl
		shl		edx,8
		mov		dl,dh
		mov		[edi],edx
		

		sub		esi,4
		sub		edi,4
		sub		cx,1
		test	cx,0FFFFh
		jnz		colonnes

		jmp		lignes
		
fin:
		pop		ebp
        pop     edi
        pop     esi
        pop     ebx

		leave
        ret                         ; Retour � la fonction MainWndProc

process_image_asm   ENDP
END