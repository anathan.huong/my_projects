; IMAGE_SIMD.ASM
;
; MI01 - TP Assembleur 2 � 5
;
; R�alise le traitement d'une image 32 bits.

.686
.XMM
.MODEL FLAT, C

.DATA
const	dq		0000004C0096001Dh,0000004C0096001Dh

.CODE

; **********************************************************************
; Sous-programme _process_image_simd
;
; R�alise le traitement d'une image 32 bits avec des instructions SIMD.
;
; Entr�es sur la pile : Largeur de l'image (entier 32 bits)
;           Hauteur de l'image (entier 32 bits)
;           Pointeur sur l'image source (d�pl. 32 bits)
;           Pointeur sur l'image tampon 1 (d�pl. 32 bits)
;           Pointeur sur l'image tampon 2 (d�pl. 32 bits)
;           Pointeur sur l'image finale (d�pl. 32 bits)
; **********************************************************************
PUBLIC      process_image_simd
process_image_simd   PROC NEAR       ; Point d'entr�e du sous programme

        push    ebp
        mov     ebp, esp

        push    ebx
        push    esi
        push    edi

        mov     ecx, [ebp + 8]
        imul    ecx, [ebp + 12]

        mov     esi, [ebp + 16]
        mov     edi, [ebp + 20]


		movdqa		xmm2,oword ptr [const]

boucle :
		cmp		ecx,2				;si index>=2, on entre dans la boucle de traitement de pixel
		jbe		fin

		sub		ecx,2
		
		movq	xmm0,qword ptr [esi+ecx*4]
		pxor		xmm1,xmm1
		punpcklbw	xmm1,xmm0
		pmulhuw		xmm1,xmm2
		phaddw		xmm1,xmm1
		phaddw		xmm1,xmm1
		pxor		xmm0,xmm0
		punpcklwd	xmm1,xmm0
		movq		qword ptr [edi+ecx*4],xmm1

		jmp boucle

fin:
        pop     edi
        pop     esi
        pop     ebx

        leave
        ret                         ; Retour � la fonction MainWndProc

process_image_simd   ENDP
END
