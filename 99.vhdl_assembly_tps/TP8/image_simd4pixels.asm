; IMAGE_SIMD.ASM
;
; MI01 - TP Assembleur 2 � 5
;
; R�alise le traitement d'une image 32 bits.

.686
.XMM
.MODEL FLAT, C

.DATA
const	dq		004C961D004C961Dh,004C961D004C961Dh

.CODE

; **********************************************************************
; Sous-programme _process_image_simd
;
; R�alise le traitement d'une image 32 bits avec des instructions SIMD.
;
; Entr�es sur la pile : Largeur de l'image (entier 32 bits)
;           Hauteur de l'image (entier 32 bits)
;           Pointeur sur l'image source (d�pl. 32 bits)
;           Pointeur sur l'image tampon 1 (d�pl. 32 bits)
;           Pointeur sur l'image tampon 2 (d�pl. 32 bits)
;           Pointeur sur l'image finale (d�pl. 32 bits)
; **********************************************************************
PUBLIC      process_image_simd
process_image_simd   PROC NEAR       ; Point d'entr�e du sous programme

        push    ebp
        mov     ebp, esp

        push    ebx
        push    esi
        push    edi

        mov     ecx, [ebp + 8]
        imul    ecx, [ebp + 12]

        mov     esi, [ebp + 16]
        mov     edi, [ebp + 20]


		movdqa		xmm2,oword ptr [const]

boucle :
		cmp		ecx,4				;si index>=4, on entre dans la boucle de traitement de pixel
		jbe		fin

		sub		ecx,4
		movdqa	xmm0,oword ptr [esi+ecx*4]
		pmaddubsw		xmm0,xmm2

		
		pxor		xmm1,xmm1
		pcmpgtw		xmm1,xmm0
		pxor		xmm0,xmm1
		psubb		xmm0,xmm1



		pxor			xmm1,xmm1
		phaddw		xmm0,xmm1
		punpcklwd	xmm0,xmm1

		psrlw		xmm0,8

		movdqa		oword ptr [edi+ecx*4],xmm0


		jmp boucle

fin:
        pop     edi
        pop     esi
        pop     ebx

        leave
        ret                         ; Retour � la fonction MainWndProc

process_image_simd   ENDP
END
