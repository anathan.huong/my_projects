# I) Presentation

Greetings! I am a Data Enthusiast and a two-time nominee for the "Viz of the Day". I am utilizing my fervor for data as the Head of Presales at MYDRAL, a renowned Tableau consulting firm. In addition to my professional role, I create captivating Tableau visualizations to assist others in making data-driven decisions and strive to inspire innovation through visually engaging data stories.

# II) Git goal

**This Git aims to present the projects that I carried out (alone or in a group), during my 5 years of study and after.**
These are either university projects or projects carried out alone, in my spare time.
In each file, a description of the project is given.
I also carried out other projects during my internships, with a direct impact on the customer top-line. 
I would be happy to discuss it in an interview.

# III) Coursework

It is important to clarify that the choice of subjects at the University of Technology of Compiègne is flexible.
Indeed, it is possible, respecting certain conditions, to choose our subjects in order to build a unique engineering course.
Therefore, I will describe below my coursework:

**The detailed coursework for my Engineering degree is:**

**First of all**, I have chosen at least one subject of mathematics for each semester.
Thus, I was able to develop continuously my logic and my scientific culture.
The main subjects of mathematics that I studied are:
- Continuity and differentiability of the functions of several real variables.
- Linear algebra and its applications.
- Probability calculation and statistical methods with hypothesis tests.
- Numerical analysis like iterative methods: linear and non-linear problems (Jacobi and Newton methods, etc.).

**From the third year**, my major is in the Computer Engineering field. Adapting to new, rapidly changing technologies is essential for any computer engineer.
The coursework is general and enables me to develop a versatile and general profile.
The main subjects taken are:
- UML, SQL, relational and non-relational database.
- System programming (C and Python).
- Object design and programming (C++).
- Linear / Nonlinear programming.
- Complexity of algorithms and used of graph-based tools to address combinatorial problems.

**From the fourth year**, I specialized in Data mining and decision-making. My passion for mathematics and statistics guided me in my choice of the course content.
The main subjects I have followed are:
- Data Mining: modern techniques for analyzing large data sets and main methods of supervised and unsupervised learning.
- Machine learning: mathematics and statistics to give computers the ability to "learn" from data. Practical application of the techniques using the R language.

**In addition to my fifth year**, I also followed the subjects of the Master AOS (Machine Learning and Optimization of
Complex Systems) from UTC - double degree, parallel Master's degree programme. Compared to my engineering training, the subjects are more theoretical and research oriented. Both degrees are complementary.
The main subjects I studied are:
- Main formalisms for uncertainty representation with emphasis on the theory of belief functions, which generalizes both probability theory and the set-membership approach.
- Neural Network and Deep Learning.
- Robust Optimization to deal with the presence of uncertain data in optimization problems.
- Modeling and optimization of discrete systems: combinatorial optimization, scheduling, theory of complexity, exact and heuristics methods.

**Finally, during my exchange semester at SEOULTECH (Seoul - South Korea)**, in my fourth year, my subjects were business-oriented. I was thus able to acquire a solid foundation on Business studies which I would now like to deepen.
The main subjects taken are:
- Strategic Technology Management: overview of technological innovation management with an emphasis on the integrative relationship of technology development with strategic planning, marketing, finance, and operations over the entire life cycle of technology.
- Fundamentals of Finance: Financial Statement Analysis, time value of money, valuing projects and firms, risk and return.
- Information Technology Project Management: methods and tools for the definition, planning, implementation, monitoring and evaluation for the management of large-scale IT projects.